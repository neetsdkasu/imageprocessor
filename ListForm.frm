VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmList 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "複数同時処理"
   ClientHeight    =   5385
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6630
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   359
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   442
   StartUpPosition =   3  'Windows の既定値
   Begin VB.CommandButton cmdClose 
      Caption         =   "閉じる"
      Height          =   300
      Left            =   5175
      TabIndex        =   15
      Top             =   4800
      Width           =   1350
   End
   Begin VB.TextBox txtFolder 
      Height          =   300
      Left            =   1365
      Locked          =   -1  'True
      TabIndex        =   13
      Text            =   "Text1"
      Top             =   4425
      Width           =   5175
   End
   Begin VB.CommandButton cmdFolder 
      Caption         =   "保存先の指定"
      Height          =   300
      Left            =   2325
      TabIndex        =   12
      Top             =   4800
      Width           =   1350
   End
   Begin MSComDlg.CommonDialog dlgList 
      Left            =   2415
      Top             =   5445
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "保存先のフォルダを開いて下さい。"
      FileName        =   "[Folder]"
      Filter          =   "フォルダ|folder"
      Flags           =   4
   End
   Begin MSComctlLib.ProgressBar prgList 
      Height          =   180
      Left            =   60
      TabIndex        =   11
      Top             =   5175
      Width           =   6450
      _ExtentX        =   11377
      _ExtentY        =   318
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.ListBox lstFile 
      Height          =   600
      Left            =   1185
      TabIndex        =   10
      Top             =   5460
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdShori 
      Caption         =   "実行"
      Height          =   300
      Left            =   3750
      TabIndex        =   9
      Top             =   4800
      Width           =   1335
   End
   Begin VB.ComboBox cboShori 
      Height          =   300
      Left            =   1155
      Style           =   2  'ﾄﾞﾛｯﾌﾟﾀﾞｳﾝ ﾘｽﾄ
      TabIndex        =   8
      Top             =   4065
      Width           =   5400
   End
   Begin VB.ComboBox cboTab 
      Height          =   300
      Left            =   75
      Style           =   2  'ﾄﾞﾛｯﾌﾟﾀﾞｳﾝ ﾘｽﾄ
      TabIndex        =   7
      Top             =   4050
      Width           =   945
   End
   Begin VB.ListBox lstPath 
      Height          =   600
      Left            =   105
      TabIndex        =   6
      Top             =   5475
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "削除"
      Height          =   300
      Left            =   5625
      TabIndex        =   5
      Top             =   3600
      Width           =   825
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "追加"
      Height          =   300
      Left            =   4650
      TabIndex        =   4
      Top             =   3600
      Width           =   825
   End
   Begin VB.ListBox lstList 
      Height          =   3480
      Left            =   4575
      MultiSelect     =   2  '拡張
      TabIndex        =   3
      Top             =   75
      Width           =   1950
   End
   Begin VB.FileListBox filList 
      Height          =   3870
      Left            =   2550
      MultiSelect     =   2  '拡張
      Pattern         =   "*.bmp;*.jpg;*.jpeg;*.gif;*.png"
      TabIndex        =   2
      Top             =   75
      Width           =   1950
   End
   Begin VB.DirListBox dirList 
      Height          =   3450
      Left            =   75
      TabIndex        =   1
      Top             =   450
      Width           =   2400
   End
   Begin VB.DriveListBox drvList 
      Height          =   300
      Left            =   75
      TabIndex        =   0
      Top             =   75
      Width           =   2400
   End
   Begin VB.Label lblFolder 
      AutoSize        =   -1  'True
      Caption         =   "保存先フォルダ"
      Height          =   180
      Left            =   90
      TabIndex        =   14
      Top             =   4485
      Width           =   1185
   End
End
Attribute VB_Name = "frmList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Function MakeFileName(lt As String, qq As Integer) As String
    Dim i As Integer
    Dim pp As Integer
    Dim lt2 As String
    
    If qq > 99 Then
        MakeFileName = lt
        Exit Function
    End If
    
    If qq > 0 Then
        lt2 = lt & "[" & Format$(qq) & "]"
    Else
        lt2 = lt
    End If
    
    pp = 0
    For i = 0 To lstFile.ListCount - 1
        If StrComp(lt2, lstFile.List(i)) = 0 Then
            pp = 1
            Exit For
        End If
    Next i
    
    If pp = 0 Then
        MakeFileName = lt2
    Else
        MakeFileName = MakeFileName(lt, qq + 1)
    End If

End Function


Private Sub cboTab_Click()
    Select Case cboTab.ListIndex
    Case 0
        cboShori.Clear
        cboShori.AddItem "拡大縮小"
        cboShori.AddItem "濃度調整"
        cboShori.AddItem "切り取り"
    Case 1
        cboShori.Clear
        cboShori.AddItem "信号抽出 Ｒ（赤）"
        cboShori.AddItem "信号抽出 Ｇ（緑）"
        cboShori.AddItem "信号抽出 Ｂ（青）"
        cboShori.AddItem "信号抽出 Ｙ（輝度）"
        cboShori.AddItem "信号抽出 Ｉ（色差）"
        cboShori.AddItem "信号抽出 Ｑ（色差）"
        cboShori.AddItem "信号抽出 Ｃｂ（色差）"
        cboShori.AddItem "信号抽出 Ｃｒ（色差）"
        cboShori.AddItem "２値化"
    Case 2
        cboShori.Clear
        cboShori.AddItem "平滑化（平均値法）"
        cboShori.AddItem "平滑化（メディアン法）"
        cboShori.AddItem "平滑化（MinMax法(MinMax)）"
        cboShori.AddItem "平滑化（MinMax法(MaxMin)）"
        cboShori.AddItem "エッジ（一次微分(一次元)）"
        cboShori.AddItem "エッジ（一次微分(二次元)）"
        cboShori.AddItem "エッジ（ソーベル微分）"
        cboShori.AddItem "エッジ（ラプラシアン(４近傍差分)）"
        cboShori.AddItem "エッジ（ラプラシアン(８近傍差分)）"
        cboShori.AddItem "エッジ（ラプラシアン(４近傍エッジ)）"
        cboShori.AddItem "エッジ（ラプラシアン(８近傍エッジ)）"
        cboShori.AddItem "エッジ（MinMax系 g1）"
        cboShori.AddItem "エッジ（MinMax系 g2）"
        cboShori.AddItem "エッジ（MinMax系 g3）"
        cboShori.AddItem "エッジ（MinMax系 g4）"
        cboShori.AddItem "エッジ（MinMax系 g5）"
    Case 3
        cboShori.Clear
        cboShori.AddItem "合成（正差）"
        cboShori.AddItem "合成（負差）"
        cboShori.AddItem "合成（全差）"
        cboShori.AddItem "合成（除算）"
        cboShori.AddItem "合成（最大）"
        cboShori.AddItem "合成（最小）"
        cboShori.AddItem "合成（平均）"
        cboShori.AddItem "合成（同値）"
        cboShori.AddItem "合成（ＯＲ）"
        cboShori.AddItem "合成（ＡＮＤ）"
        cboShori.AddItem "合成（ＸＯＲ）"
        cboShori.AddItem "モザイク化"
        cboShori.AddItem "色反転（全色）"
        cboShori.AddItem "色反転（赤）"
        cboShori.AddItem "色反転（緑）"
        cboShori.AddItem "色反転（青）"
        cboShori.AddItem "回転（上下）"
        cboShori.AddItem "回転（左右）"
    Case 4
        cboShori.Clear
        cboShori.AddItem "量子化（モノクロ）"
        cboShori.AddItem "濃度正規化（モノクロ）"
        cboShori.AddItem "雑音付加（モノクロ）"
        cboShori.AddItem "大津の方法による２値化"
        cboShori.AddItem "大津閾値濃度調整"
        cboShori.AddItem "中央値濃度調整"
    Case 5
        cboShori.Clear
        cboShori.AddItem "ヒストグラム描写"
        cboShori.AddItem "小頻度濃度除去"
        cboShori.AddItem "濃度詰め"
    End Select
    cboShori.ListIndex = 0
End Sub


Private Sub cmdAdd_Click()
    Dim pth As String, fn As String
    Dim i As Integer, j As Integer
    Dim pp As Integer
    
    pth = filList.Path
    
    If StrComp(Right$(pth, 1), "&") <> 0 Then
        pth = pth & "\"
    End If
    
    For i = 0 To filList.ListCount - 1
        If filList.Selected(i) Then
            fn = filList.List(i)
            pp = 0
            For j = 0 To lstPath.ListCount - 1
                If StrComp((pth & fn), lstPath.List(j)) = 0 Then
                    pp = pp + 1
                End If
            Next j
            If pp = 0 Then
                lstList.AddItem fn
                lstPath.AddItem pth & fn
            End If
        End If
    Next i
    
    
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdDelete_Click()
    Dim i As Integer
    
    For i = (lstList.ListCount - 1) To 0 Step -1
        If lstList.Selected(i) Then
            lstList.RemoveItem i
            lstPath.RemoveItem i
        End If
    Next i
    
End Sub

Private Sub cmdFolder_Click()
    On Error GoTo cmdFolder_Click_Error
    
    Dim fn As String
    Dim pp As Integer
    Dim qq As Integer
    
    dlgList.FileName = "[Folder]"
    
    dlgList.ShowOpen
    
    pp = Len(dlgList.FileTitle)
    qq = Len(dlgList.FileName)
    
    fn = Left$(dlgList.FileName, qq - pp)
    
    txtFolder.Text = fn
    
    Exit Sub
    
cmdFolder_Click_Error:
    MsgBox Error(Err), , "ERROR" & Str$(Err)
End Sub

Private Sub cmdShori_Click()
    
    On Error GoTo cmdShori_Click_Error
    
    Dim i As Integer, j As Integer
    Dim fn As String, lt As String
    Dim fld As String
    Dim iCboT As Integer, iCboS As Integer
    Dim iLstF As Integer
    
    If lstList.ListCount = 0 Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    frmList.prgList.Value = 0!
    
    fld = Format$(Date$, "yymmdd") & Format$(CLng(Timer), "00000")
    fld = txtFolder.Text & fld
    MkDir fld
    
   
    lstFile.Clear
    For i = 0 To lstList.ListCount - 1
        lt = lstList.List(i)
        For j = Len(lt) To 1 Step -1
            If StrComp(Mid$(lt, j, 1), ".") = 0 Then
                Exit For
            End If
        Next j
        If j > 0 Then
            lt = Left$(lt, j - 1)
            lstFile.AddItem MakeFileName(lt, 0)
        End If
    Next i
    
    iCboT = cboTab.ListIndex
    iCboS = cboShori.ListIndex
    
    iLstF = lstFile.ListCount
    
    frmList.Caption = "複数画像処理 (0/" & Format$(iLstF) & ")"
    
    For i = 0 To iLstF - 1
        
        fn = lstPath.List(i)
        frmLoad.picView.Picture = LoadPicture(fn)
        
        Select Case iCboT
        Case 0      'Ａ
            Select Case iCboS
            Case 0          '拡大縮小
                Call ChangeSize
            Case 1          '濃度調整
                Call ChangeNoudo
            Case 2          '切り取り
                Call pCutting
            End Select
        Case 1      'Ｂ
            Select Case iCboS
            Case 0, 1, 2    '信号抽出 赤、緑、青
                Call ExtrctRGB(iCboS)
            Case 3          '信号抽出 輝度 Y
                Call ExtrctRGBTrans(0.3!, 0.59!, 0.11!, 0!)
            Case 4          '信号抽出 色差 I
                Call ExtrctRGBTrans(0.6!, -0.28!, -0.32!, 0!)
            Case 5          '信号抽出 色差 Q
                Call ExtrctRGBTrans(0.21!, -0.52!, 0.31!, 0!)
            Case 6          '信号抽出 色差 Cb
                Call ExtrctRGBTrans(-0.17!, -0.33!, 0.5!, 128!)
            Case 7          '信号抽出 色差 Cr
                Call ExtrctRGBTrans(0.5!, -0.42!, -0.08!, 128!)
            Case 8          '２値化
                If frmMain.opt2chiAndOr(0) Then
                    Call Shori2chiAnd
                Else
                    Call Shori2chiOr
                End If
            End Select
        Case 2      'Ｃ
            Select Case iCboS
            Case 0          '平滑化 平均化法
                Call FltrHeikin
            Case 1          '平滑化 メディアン法
                Call FltrMedian
            Case 2 To 3     '平滑化　MinMax
                Call FltrMinMax(iCboS - 2)
            Case 4 To 7     'エッジ 一次微分(一、二)、ソーベル微分
                Call MskMtrx(iCboS - 4)
            Case 8 To 11    'エッジ ラプラシアン(4差、8差、4エ、8エ)
                Call MskLaplacian(iCboS - 8)
            Case 12         'MinMax系 g1
                Call MinMaxg1
            Case 13         'MinMax系 g2
                Call MinMaxg2
            Case 14         'MinMax系 g3
                Call MinMaxg3
            Case 15         'MinMax系 g4
                Call MinMaxg4
            Case 16         'MinMax系 g5
                Call MinMaxg5
            End Select
        Case 3      'Ｄ
            Select Case iCboS
            Case 0 To 10    '合成
                Call Gosei(iCboS)
            Case 11         'モザイク化
                Call Mozaiku
            Case 12 To 15   '色反転
                Call Hanten(iCboS - 11)
            Case 16         '回転（上下）
                Call KaitenJoge
            Case 17         '回転（左右）
                Call KaitenSayu
            End Select
        Case 4      'Ｅ
            Select Case iCboS
            Case 0          '量子化（モノクロ）
                Call Ryoshika
            Case 1          '濃度正規化（モノクロ）
                Call NodoSeikika
            Case 2          '雑音付加（モノクロ）
                Call MakeNoise
            Case 3          '大津の方法による２値化
                Call Shori2chiOtsu
            Case 4          '大津閾値を中心にずらす濃度調整
                Call ShoriNoudoCA1
            Case 5          '中央値濃度調整
                Call ShoriNoudoCA2
            End Select
        Case 5      'Ｆ
            Select Case iCboS
            Case 0          'ヒストグラム描写
                Call DrawHist
            Case 1          '少頻度濃度除去
                Call ShoriNDCut
            Case 2          '濃度詰め
                Call ShoriNDPress
            End Select
        End Select
        
        fn = lstFile.List(i)
        fn = fld & "\" & fn & ".bmp"
        SavePicture frmResult.picView.Image, fn
        
        If iLstF > 1 Then
            frmList.prgList.Value = CSng(i + 1) / CSng(iLstF) * 100!
        Else
            frmList.prgList.Value = 100!
        End If
        
        frmList.Caption = "複数画像処理 (" _
                & Format$(i + 1) & "/" & Format$(iLstF) & ")"
        
    Next i

    Screen.MousePointer = vbArrow
    
    frmLoad.Caption = "元画像 " & lstList.List(iLstF - 1)
    
    MsgBox fld & Chr$(13) & "上記のフォルダに保存されました。"
    
    frmList.Caption = "複数画像処理"
    
    Exit Sub

cmdShori_Click_Error:

    Screen.MousePointer = vbArrow
    
    MsgBox Error(Err), , "ERROR" & Str$(Err)
    
End Sub

Private Sub dirList_Change()
    filList.Path = dirList.Path
End Sub

Private Sub drvList_Change()
    dirList.Path = drvList.Drive
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim fn As String
    
    For i = 0 To 5
        cboTab.AddItem Chr$(Asc("Ａ") + i)
    Next i
    cboTab.ListIndex = 0
    
    fn = CurDir$()
    If StrComp(Right$(fn, 1), "\") <> 0 Then
        fn = fn & "\"
    End If
    txtFolder.Text = fn
    
    
End Sub

