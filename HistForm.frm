VERSION 5.00
Begin VB.Form frmHist 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  '固定(実線)
   Caption         =   "ヒストグラム"
   ClientHeight    =   4050
   ClientLeft      =   4035
   ClientTop       =   330
   ClientWidth     =   4050
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   270
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   270
   Begin VB.CommandButton cmdHistClose 
      Caption         =   "Close"
      Height          =   300
      Left            =   2925
      TabIndex        =   5
      Top             =   3675
      Width           =   975
   End
   Begin VB.ListBox lstHist2 
      Height          =   1500
      Left            =   150
      TabIndex        =   4
      Top             =   2475
      Visible         =   0   'False
      Width           =   2625
   End
   Begin VB.CommandButton cmdHist2 
      Caption         =   "DATA 2"
      Height          =   300
      Left            =   2925
      TabIndex        =   3
      Top             =   3300
      Width           =   975
   End
   Begin VB.CommandButton cmdHist 
      Caption         =   "DATA 1"
      Height          =   300
      Left            =   2925
      TabIndex        =   2
      Top             =   2925
      Width           =   975
   End
   Begin VB.ListBox lstHist 
      Height          =   1500
      Left            =   150
      TabIndex        =   1
      Top             =   2475
      Width           =   2625
   End
   Begin VB.PictureBox picView 
      AutoRedraw      =   -1  'True
      ForeColor       =   &H80000018&
      Height          =   2400
      Left            =   0
      ScaleHeight     =   156
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   265
      TabIndex        =   0
      Top             =   0
      Width           =   4035
   End
End
Attribute VB_Name = "frmHist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdHist_Click()

    lstHist.Visible = True
    lstHist2.Visible = False

End Sub

Private Sub cmdHist2_Click()

    lstHist.Visible = False
    lstHist2.Visible = True

End Sub

Private Sub cmdHistClose_Click()

    Unload Me

End Sub

