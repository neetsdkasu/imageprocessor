Attribute VB_Name = "ModuleShori"
Option Explicit



Public Sub ChangeNoudo()

    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim c As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim dR As Integer, dG As Integer, dB As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    dw = CInt(frmLoad.picView.Width)
    dh = CInt(frmLoad.picView.Height)
        
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
        
    dR = CInt(frmMain.sldAkarusa(0).Value)
    dG = CInt(frmMain.sldAkarusa(1).Value)
    dB = CInt(frmMain.sldAkarusa(2).Value)
        
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cR = dR + CInt(c Mod &H100&)
            cG = dG + CInt((c \ &H100&) Mod &H100&)
            cB = dB + CInt((c \ &H100&) \ &H100&)
            If cR < 0 Then cR = 0
            If cG < 0 Then cG = 0
            If cB < 0 Then cB = 0
            If cR > &HFF Then cR = &HFF
            If cG > &HFF Then cG = &HFF
            If cB > &HFF Then cB = &HFF
            c = RGB(cR, cG, cB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i

    Screen.MousePointer = vbArrow

End Sub
Public Sub ChangeSize()

    Dim i As Integer, j As Integer
    Dim c As Long
    Dim sw As Single, sh As Single
    Dim rw As Integer, rh As Integer
    Dim xx As Single, yy As Single
    
    If CInt(Val(frmMain.txtWidth.Text)) > 1280 Then
        Exit Sub
    End If
    If CInt(Val(frmMain.txtHeight.Text)) > 1024 Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    sw = frmLoad.picView.ScaleWidth
    sh = frmLoad.picView.ScaleHeight
    rw = CInt(Val(frmMain.txtWidth.Text))
    rh = CInt(Val(frmMain.txtHeight.Text))
    
    frmResult.picView.Width = frmLoad.picView.Width _
            + CSng(rw) - sw
    frmResult.picView.Height = frmLoad.picView.Height _
            + CSng(rh) - sh
    
    frmResult.picView.Cls
    
    For i = 0 To rh - 1
        yy = Int(CSng(i) / CSng(rh) * sh)
        If yy > (sh - 1!) Then yy = sh - 1!
        For j = 0 To rw - 1
            xx = Int(CSng(j) / CSng(rw) * sw)
            If xx > (sw - 1!) Then xx = sw - 1!
            c = frmLoad.picView.Point(xx, yy)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(rh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub


Public Sub Hanten(hflag As Integer)
    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim c As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cR = CInt(c Mod &H100&)
            cG = CInt((c \ &H100&) Mod &H100&)
            cB = CInt((c \ &H100&) \ &H100&)
            If hflag = 0 Or hflag = 1 Then
                cR = &HFF - cR
            End If
            If hflag = 0 Or hflag = 2 Then
                cG = &HFF - cG
            End If
            If hflag = 0 Or hflag = 3 Then
                cB = &HFF - cB
            End If
            c = RGB(cR, cG, cB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    Screen.MousePointer = vbArrow

End Sub

Public Sub KaitenJoge()
    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim c As Long
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(dh - 1 - i))
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub

Public Sub KaitenSayu()
    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim c As Long
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(dw - 1 - j), CSng(i))
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub

Public Function Kido(c As Long) As Integer
    Dim dcR As Single, dcG As Single, dcB As Single
    Dim dcC As Single
    dcR = CSng(c Mod &H100&)
    dcG = CSng((c \ &H100&) Mod &H100&)
    dcB = CSng((c \ &H100&) \ &H100&)
    dcC = 0.3! * dcR + 0.59! * dcG + 0.11! * dcB
    Kido = CInt(dcC)
End Function

Public Sub MakeNoise()

    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim c As Long, cC As Integer
    Dim cn As Integer, k As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    frmResult.picView.Cls
    With frmLoad.picView
        dw = CInt(.ScaleWidth)
        dh = CInt(.ScaleHeight)
        frmResult.picView.Width = .Width
        frmResult.picView.Height = .Height
    End With
    
    cn = CInt(Val(frmMain.txtNoise.Text))
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cC = Kido(c)
            For k = 1 To cn
                cC = cC + CInt(Rnd() * 2) - 1
                If cC < 0 Then
                    cC = 0
                End If
                If cC > 255 Then
                    cC = 255
                End If
            Next k
            c = RGB(cC, cC, cC)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i) / CSng(dh - 1) * 100!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub

Public Sub Mozaiku()
    Dim w1 As Integer, h1 As Integer
    Dim w2 As Integer, h2 As Integer
    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim sx As Single, sy As Single
    Dim sw As Single, sh As Single
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim dcR As Single, dcG As Single, dcB As Single
    Dim c As Long
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height

    sw = frmLoad.picView.ScaleWidth
    sh = frmLoad.picView.ScaleHeight
    
    If frmMain.optMozaiku(0).Value Then
        w2 = CInt(Val(frmMain.txtMzW.Text))
        h2 = CInt(Val(frmMain.txtMzH.Text))
        w1 = CInt(sw) \ w2
        h1 = CInt(sh) \ h2
        If (CInt(sw) Mod w2) > 0 Then
            w1 = w1 + 1
        End If
        If (CInt(sh) Mod h2) > 0 Then
            h1 = h1 + 1
        End If
    Else
        w1 = CInt(Val(frmMain.txtMzX.Text))
        h1 = CInt(Val(frmMain.txtMzY.Text))
        w2 = CInt(sw) \ w1
        h2 = CInt(sh) \ h1
        If (CInt(sw) Mod w1) > 0 Then
            w2 = w2 + 1
        End If
        If (CInt(sh) Mod h1) > 0 Then
            h2 = h2 + 1
        End If
    End If
    
    
    For i = 0 To h2 - 1
        For j = 0 To w2 - 1
            dcR = 0!: dcG = 0!: dcB = 0!
            For m = 0 To h1 - 1
                sy = CSng(i * h1 + m)
                If sy > (sh - 1!) Then sy = sh - 1!
                For n = 0 To w1 - 1
                    sx = CSng(j * w1 + n)
                    If sx > (sw - 1!) Then sx = sw - 1!
                    c = frmLoad.picView.Point(sx, sy)
                    dcR = dcR + CSng(c Mod &H100&)
                    dcG = dcG + CSng((c \ &H100&) Mod &H100&)
                    dcB = dcB + CSng((c \ &H100&) \ &H100&)
                Next n
            Next m
            dcR = dcR / CSng(m) / CSng(n)
            dcG = dcG / CSng(m) / CSng(n)
            dcB = dcB / CSng(m) / CSng(n)
            cR = CInt(dcR)
            cG = CInt(dcG)
            cB = CInt(dcB)
            c = RGB(cR, cG, cB)
            For m = 0 To h1 - 1
                sy = CSng(i * h1 + m)
                If sy > (sh - 1!) Then sy = sh - 1!
                For n = 0 To w1 - 1
                    sx = CSng(j * w1 + n)
                    If sx > (sw - 1!) Then sx = sw - 1!
                    frmResult.picView.PSet (sx, sy), c
                Next n
            Next m
            frmMain.prgShinko.Value = 100! _
                                * CSng(i * w2 + j + 1) _
                                / CSng(w2) / CSng(h2)
        Next j
    Next i
            
    Screen.MousePointer = vbArrow

End Sub

Public Sub NodoSeikika()
    Dim i As Integer, j As Integer
    Dim c As Long, cC As Integer, cp As Integer
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim cmx As Integer, cmn As Integer
    Dim dw As Integer, dh As Integer
    Dim dcC As Single
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    cmx = 0: cmn = 255
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cC = Kido(c)
            If cmx < cC Then cmx = cC
            If cmn > cC Then cmn = cC
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50!
    Next i
    
    cp = cmx - cmn
    
    If cp = 255 Or cp = 0 Then
        frmResult.picView.PaintPicture frmLoad.picView.Image, 0!, 0!
        Screen.MousePointer = vbArrow
        frmMain.prgShinko.Value = 100!
        Exit Sub
    End If
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cR = CInt(c Mod &H100&)
            cG = CInt((c \ &H100&) Mod &H100&)
            cB = CInt((c \ &H100&) \ &H100&)
            dcC = 0.3! * CSng(cR) + 0.59! * CSng(cG) _
                                    + 0.11! * CSng(cB)
            cC = CInt(dcC)
            dcC = dcC - CSng(cmn)
            dcC = dcC * (255! / CSng(cp))
            cC = CInt(dcC)
            'cR = cR + (CInt(dcC) - cC)
            'cG = cG + (CInt(dcC) - cC)
            'cB = cB + (CInt(dcC) - cC)
            'If cR < 0 Then cR = 0
            'If cG < 0 Then cG = 0
            'If cB < 0 Then cB = 0
            'f cR > 255 Then cR = 255
            'If cG > 255 Then cG = 255
            'If cB > 255 Then cB = 255
            c = RGB(cC, cC, cC)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50! + 50!
    Next i
    
    Screen.MousePointer = vbArrow
    

End Sub

Public Sub pCutting()
    Dim sw As Single, sh As Single
    Dim x0 As Single, y0 As Single
    Dim ww As Integer, hh As Integer
    Dim i As Integer, j As Integer
    Dim c As Long

    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    

    sw = CSng(frmLoad.picView.ScaleWidth)
    sh = CSng(frmLoad.picView.ScaleHeight)
    
    If frmMain.optUnit(0).Value Then
        x0 = CSng(Val(frmMain.txtLeft.Text))
        y0 = CSng(Val(frmMain.txtTop.Text))
        If frmMain.optMigishita(0).Value Then
            ww = CInt(CSng(Val(frmMain.txtRight.Text)) - x0 + 1!)
            hh = CInt(CSng(Val(frmMain.txtBottom.Text)) - y0 + 1!)
        Else
            ww = CInt(Val(frmMain.txtRight.Text))
            hh = CInt(Val(frmMain.txtBottom.Text))
        End If
    Else
        x0 = sw * CSng(Val(frmMain.txtLeft.Text)) / 100!
        y0 = sh * CSng(Val(frmMain.txtTop.Text)) / 100!
        If frmMain.optMigishita(0).Value Then
            ww = CInt(sw * CSng(Val(frmMain.txtRight.Text)) _
                    / 100! - x0 + 1!)
            hh = CInt(sh * CSng(Val(frmMain.txtBottom.Text)) _
                    / 100! - y0 + 1!)
        Else
            ww = CInt(sw * CSng(Val(frmMain.txtRight.Text)) / 100!)
            hh = CInt(sh * CSng(Val(frmMain.txtBottom.Text)) / 100!)
        End If
    End If
    
    If ((x0 + CSng(ww) - sw) > 0!) _
            Or ((y0 + CSng(hh) - sh) > 0!) Then
        Screen.MousePointer = vbArrow
        Exit Sub
    End If
    
    frmResult.picView.Width = (frmLoad.picView.Width _
                - frmLoad.picView.ScaleWidth) + CSng(ww)
    frmResult.picView.Height = (frmLoad.picView.Height _
                - frmLoad.picView.ScaleHeight) + CSng(hh)
    
    frmResult.picView.Cls
    
    For i = 0 To hh - 1
        For j = 0 To ww - 1
            c = frmLoad.picView.Point(x0 + CSng(j), y0 + CSng(i))
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(hh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub


Public Sub Ryoshika()
    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim c As Long, cC As Integer
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim pp As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    pp = CInt(Val(frmMain.txtRyoshika.Text))
    pp = &H100 \ pp
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cR = CInt(c Mod &H100&)
            cG = CInt((c \ &H100&) Mod &H100&)
            cB = CInt((c \ &H100&) \ &H100&)
            cC = CInt(0.3! * CSng(cR) + 0.59! * CSng(cG) + 0.11! * CSng(cB))
            cC = pp * (cC \ pp)
            'cR = pp * (cR \ pp)
            'cG = pp * (cG \ pp)
            'cB = pp * (cB \ pp)
            c = RGB(cC, cC, cC)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub

Public Function ShikiiOtsu() As Integer
    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim c As Long, cC As Integer
    Dim dcR As Single, dcG As Single, dcB As Single
    Dim hs(255) As Long, dhs(255) As Double
    Dim cMax As Integer
    Dim wk(255) As Double, uk(255) As Double
    Dim sig As Double, sigmax As Double
    Dim siki As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    For i = 0 To 255
        hs(i) = 0&
        frmMain.prgShinko.Value = CSng(i + 1) / 256! * 5!
    Next i
    
    cMax = 0
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            dcR = CSng(c Mod &H100&)
            dcG = CSng((c \ &H100&) Mod &H100&)
            dcB = CSng((c \ &H100&) \ &H100&)
            cC = CInt(0.3! * dcR + 0.59! * dcG + 0.11! * dcB)
            hs(cC) = hs(cC) + 1&
            If cMax < cC Then cMax = cC
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 70! + 5!
    Next i
    
    For i = 0 To 255
        dhs(i) = CDbl(hs(i)) / CDbl(dw) / CDbl(dh)
        wk(i) = 0#
        uk(i) = 0&
        frmMain.prgShinko.Value = CSng(i + 1) / 256! * 5! + 75!
    Next i
    
    For i = 0 To 255
        For j = 0 To i
            wk(i) = wk(i) + dhs(j)
            uk(i) = uk(i) + CDbl(j) / 255# * dhs(j)
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / 256! * 15! + 80!
    Next i
    
    sigmax = 0#
    
    For i = 0 To 255
        If (wk(i) > 0!) And (wk(i) < 1!) Then
            sig = (uk(cMax) * wk(i) - uk(i))
            sig = sig * sig
            sig = sig / wk(i)
            sig = sig / (1 - wk(i))
            If sigmax < sig Then
                sigmax = sig
                siki = i
            End If
        End If
        frmMain.prgShinko.Value = CSng(i + 1) / 256! * 5! + 95!
    Next i
    
    Screen.MousePointer = vbArrow
    
    ShikiiOtsu = siki
    
End Function


Public Sub ShoriNDCut()

    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim hs(255) As Long
    Dim c As Long, cC As Integer
    Dim hMax As Long, hK As Long
    Dim hr(255) As Integer, hz(255) As Integer
    Dim fs As Integer, fe As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cC = Kido(c)
            hs(cC) = hs(cC) + 1&
            If hMax < hs(cC) Then hMax = hs(cC)
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50!
    Next i
    
    If frmMain.optNDCut(0).Value Then
        hK = CLng(Val(frmMain.txtNDCut.Text))
    Else
        hK = CLng(CDbl(Val(frmMain.txtNDCut.Text)) / 100# * CDbl(dw) * CDbl(dh))
    End If
    For i = 0 To 255
        If hs(i) > hK Then hr(i) = 1
    Next i
    
    fs = -256
    For i = 0 To 255
    
        If hr(i) = 0 Then
                    
            fe = 512
            For j = i To 255
                If hr(j) = 1 Then
                    fe = j
                    Exit For
                End If
            Next j
            
            If Abs(i - fs) > Abs(i - fe) Then
                hz(i) = fe
            Else
                hz(i) = fs
            End If
            
        Else
            fs = i
            hz(i) = i
        End If
    Next i

    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cC = hz(Kido(c))
            c = RGB(cC, cC, cC)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50! + 50!
    Next i

    Screen.MousePointer = vbArrow

End Sub

Public Sub ShoriNDPress()

    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim hs(255) As Long
    Dim c As Long, cC As Integer
    Dim hz(255) As Integer, pp As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cC = Kido(c)
            hs(cC) = hs(cC) + 1&
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50!
    Next i
    
    pp = 0
    For i = 0 To 255
        If hs(i) > 0& Then
            hz(i) = pp
            pp = pp + 1
        Else
            hz(i) = i
        End If
    Next i
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cC = hz(Kido(c))
            c = RGB(cC, cC, cC)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50! + 50!
    Next i

    Screen.MousePointer = vbArrow

    
    
    
End Sub

Public Sub ShoriNoudoCA1()
    Dim cC As Integer
    Dim lcA(2) As Long
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    cC = ShikiiOtsu()
    
    Screen.MousePointer = vbHourglass
    
    If Abs(cC - 127) < 4 Then
        Exit Sub
    End If
    
    For i = 0 To 2
        lcA(i) = frmMain.sldAkarusa(i).Value
        frmMain.sldAkarusa(i).Value = CLng(127 - cC)
    Next i
    
    Call ChangeNoudo
    
    For i = 0 To 2
        frmMain.sldAkarusa(i).Value = lcA(i)
    Next i

End Sub


Public Sub ShoriNoudoCA2()

    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim hs(255) As Long
    Dim c As Long, cC As Integer, pp As Integer
    Dim fs As Integer, fe As Integer
    Dim hMax As Long, hMin As Long, hhalf As Long
    
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cC = Kido(c)
            hs(cC) = hs(cC) + 1&
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50!
    Next i
    
    hhalf = CLng(((CDbl(dw) * CDbl(dh)) / 2#))
    hMin = 0&
    hMax = 0&
    fs = -1
    fe = -1
    For i = 0 To 255
        If fs < 0 Then
            hMin = hMin + hs(i)
            If hMin > hhalf Then
                fs = i
            End If
        End If
        If fe < 0 Then
            hMax = hMax + hs(255 - i)
            If hMax > hhalf Then
                fe = 255 - i
            End If
        End If
        If (fs > 0) And (fe > 0) Then
            Exit For
        End If
    Next i

    pp = 127 - ((fs + fe) \ 2)

    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cC = Kido(c) + pp
            If cC < 0 Then cC = 0
            If cC > 255 Then cC = 255
            c = RGB(cC, cC, cC)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50! + 50!
    Next i

    Screen.MousePointer = vbArrow

    
End Sub


