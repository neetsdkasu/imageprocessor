Attribute VB_Name = "ModuleHeikatsu"
Option Explicit


        
Public Sub FltrHeikin()
        
    Dim dw As Integer, dh As Integer
    Dim dx As Integer, dy As Integer
    Dim c As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim dcR As Single, dcG As Single, dcB As Single
    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim pp As Integer, qq As Integer, ms As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    ms = CInt(frmMain.sldHeikatsu.Value)
    
    pp = ms \ 2
    qq = ms * ms
    
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    frmResult.picView.Cls
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            dcR = 0!: dcG = 0!: dcB = 0!
            For m = -pp To pp
                dy = i + m
                If dy < 0 Then dy = 0
                If dy >= dh Then dy = dh - 1
                For n = -pp To pp
                    dx = j + n
                    If dx < 0 Then dx = 0
                    If dx >= dw Then dx = dw - 1
                    c = frmLoad.picView.Point(CSng(dx), CSng(dy))
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    dcR = dcR + CSng(cR) / CSng(qq)
                    dcG = dcG + CSng(cG) / CSng(qq)
                    dcB = dcB + CSng(cB) / CSng(qq)
                Next n
            Next m
            cR = CInt(dcR)
            cG = CInt(dcG)
            cB = CInt(dcB)
            c = RGB(cR, cG, cB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow
    
End Sub

Public Sub FltrMedian()

    Dim dw As Integer, dh As Integer
    Dim dx As Integer, dy As Integer
    Dim c As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim i As Integer, j As Integer, k As Integer
    Dim m As Integer, n As Integer
    Dim p As Integer, q As Integer
    Dim pp As Integer, qq As Integer, ms As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    ms = CInt(frmMain.sldHeikatsu.Value)
    
    pp = ms \ 2
    qq = ms * ms
    
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    frmResult.picView.Cls
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    For i = 0 To dh - 1
        
        For j = 0 To dw - 1
        
            ReDim mcR(qq) As Integer
            ReDim mcG(qq) As Integer
            ReDim mcB(qq) As Integer
            k = 0
            For m = -pp To pp
                dy = i + m
                If dy < 0 Then dy = 0
                If dy >= dh Then dy = dh - 1
                For n = -pp To pp
                    dx = j + n
                    If dx < 0 Then dx = 0
                    If dx >= dw Then dx = dw - 1
                    c = frmLoad.picView.Point(CSng(dx), CSng(dy))
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    For p = 0 To k
                        If mcR(p) < cR Then
                            For q = (qq - 1) To p + 1 Step -1
                                mcR(q) = mcR(q - 1)
                            Next q
                            mcR(p) = cR
                            Exit For
                        End If
                    Next p
                    For p = 0 To k
                        If mcG(p) < cG Then
                            For q = (qq - 1) To p + 1 Step -1
                                mcG(q) = mcG(q - 1)
                            Next q
                            mcG(p) = cG
                            Exit For
                        End If
                    Next p
                    For p = 0 To k
                        If mcB(p) < cB Then
                            For q = (qq - 1) To p + 1 Step -1
                                mcB(q) = mcB(q - 1)
                            Next q
                            mcB(p) = cB
                            Exit For
                        End If
                    Next p
                    k = k + 1
                Next n
            Next m
            cR = mcR(qq \ 2)
            cG = mcG(qq \ 2)
            cB = mcB(qq \ 2)
            c = RGB(cR, cG, cB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub

Public Sub FltrMinMax(mflag As Integer)
    Dim dw As Integer, dh As Integer
    Dim dx As Integer, dy As Integer
    Dim c As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim ncR As Integer, ncG As Integer, ncB As Integer
    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim pp As Integer, ms As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    ms = CInt(frmMain.sldHeikatsu.Value)
    
    pp = ms \ 2
    
    frmMain.picTemp.Width = frmLoad.picView.Width
    frmMain.picTemp.Height = frmLoad.picView.Height
    frmMain.picTemp.Cls
    
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    frmResult.picView.Cls
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            If mflag = 0 Then
                ncR = &H100: ncG = &H100: ncB = &H100
            Else
                ncR = &H0: ncG = &H0: ncB = &H0
            End If
            For m = -pp To pp
                dy = i + m
                If dy < 0 Then dy = 0
                If dy >= dh Then dy = dh - 1
                For n = -pp To pp
                    dx = j + n
                    If dx < 0 Then dx = 0
                    If dx >= dw Then dx = dw - 1
                    c = frmLoad.picView.Point(CSng(dx), CSng(dy))
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    If mflag = 0 Then
                        If ncR > cR Then
                            ncR = cR
                        End If
                        If ncG > cG Then
                            ncG = cG
                        End If
                        If ncB > cB Then
                            ncB = cB
                        End If
                    Else
                        If ncR < cR Then
                            ncR = cR
                        End If
                        If ncG < cG Then
                            ncG = cG
                        End If
                        If ncB < cB Then
                            ncB = cB
                        End If
                    End If
                Next n
            Next m
            c = RGB(ncR, ncG, ncB)
            frmMain.picTemp.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 25!
    Next i
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            If mflag = 0 Then
                ncR = &H0: ncG = &H0: ncB = &H0
            Else
                ncR = &H100: ncG = &H100: ncB = &H100
            End If
            For m = -pp To pp
                dy = i + m
                If dy < 0 Then dy = 0
                If dy >= dh Then dy = dh - 1
                For n = -pp To pp
                    dx = j + n
                    If dx < 0 Then dx = 0
                    If dx >= dw Then dx = dw - 1
                    c = frmMain.picTemp.Point(CSng(dx), CSng(dy))
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    If mflag = 0 Then
                        If ncR < cR Then
                            ncR = cR
                        End If
                        If ncG < cG Then
                            ncG = cG
                        End If
                        If ncB < cB Then
                            ncB = cB
                        End If
                    Else
                        If ncR > cR Then
                            ncR = cR
                        End If
                        If ncG > cG Then
                            ncG = cG
                        End If
                        If ncB > cB Then
                            ncB = cB
                        End If
                    End If
                Next n
            Next m
            c = RGB(ncR, ncG, ncB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 25! + 25!
    Next i
    
    frmMain.picLoad.Cls
    frmMain.picLoad.Picture = frmResult.picView.Image
    frmMain.picTemp.Cls
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            If mflag = 1 Then
                ncR = &H100: ncG = &H100: ncB = &H100
            Else
                ncR = &H0: ncG = &H0: ncB = &H0
            End If
            For m = -pp To pp
                dy = i + m
                If dy < 0 Then dy = 0
                If dy >= dh Then dy = dh - 1
                For n = -pp To pp
                    dx = j + n
                    If dx < 0 Then dx = 0
                    If dx >= dw Then dx = dw - 1
                    c = frmMain.picLoad.Point(CSng(dx), CSng(dy))
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    If mflag = 1 Then
                        If ncR > cR Then
                            ncR = cR
                        End If
                        If ncG > cG Then
                            ncG = cG
                        End If
                        If ncB > cB Then
                            ncB = cB
                        End If
                    Else
                        If ncR < cR Then
                            ncR = cR
                        End If
                        If ncG < cG Then
                            ncG = cG
                        End If
                        If ncB < cB Then
                            ncB = cB
                        End If
                    End If
                Next n
            Next m
            c = RGB(ncR, ncG, ncB)
            frmMain.picTemp.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 25! + 50!
    Next i
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            If mflag = 1 Then
                ncR = &H0: ncG = &H0: ncB = &H0
            Else
                ncR = &H100: ncG = &H100: ncB = &H100
            End If
            For m = -pp To pp
                dy = i + m
                If dy < 0 Then dy = 0
                If dy >= dh Then dy = dh - 1
                For n = -pp To pp
                    dx = j + n
                    If dx < 0 Then dx = 0
                    If dx >= dw Then dx = dw - 1
                    c = frmMain.picTemp.Point(CSng(dx), CSng(dy))
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    If mflag = 1 Then
                        If ncR < cR Then
                            ncR = cR
                        End If
                        If ncG < cG Then
                            ncG = cG
                        End If
                        If ncB < cB Then
                            ncB = cB
                        End If
                    Else
                        If ncR > cR Then
                            ncR = cR
                        End If
                        If ncG > cG Then
                            ncG = cG
                        End If
                        If ncB > cB Then
                            ncB = cB
                        End If
                    End If
                Next n
            Next m
            c = RGB(ncR, ncG, ncB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 25! + 75!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub
