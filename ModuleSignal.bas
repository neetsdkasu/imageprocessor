Attribute VB_Name = "ModuleSignal"
Option Explicit

Public Const EXT_RED As Integer = 0
Public Const EXT_GREEN As Integer = 1
Public Const EXT_BLUE As Integer = 2

Public Sub ExtrctRGB(f As Integer)

    Dim dw As Integer, dh As Integer
    Dim c As Long, cC As Integer
    Dim i As Integer, j As Integer

    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    frmResult.picView.Cls

    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            Select Case f
            Case EXT_RED
                cC = CInt(c Mod &H100&)
            Case EXT_GREEN
                cC = CInt((c \ &H100&) Mod &H100&)
            Case EXT_BLUE
                cC = CInt((c \ &H100&) \ &H100&)
            End Select
            c = RGB(cC, cC, cC)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub

Public Sub ExtrctRGBTrans(dmR As Single, dmG As Single, dmB As Single, dP As Single)

    Dim dw As Integer, dh As Integer
    Dim c As Long, dc As Single, cC As Integer
    Dim dcR As Single, dcG As Single, dcB As Single
    Dim i As Integer, j As Integer

    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    frmResult.picView.Cls

    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            dcR = CSng(c Mod &H100&)
            dcG = CSng((c \ &H100&) Mod &H100&)
            dcB = CSng((c \ &H100&) \ &H100&)
            dc = dmR * dcR + dmG * dcG + dmB * dcB + dP
            cC = CInt(dc)
            If cC < 0 Then cC = 0
            If cC > &HFF Then cC = &HFF
            c = RGB(cC, cC, cC)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub
