Attribute VB_Name = "ModuleHist"
Option Explicit


Public Sub DrawHist()

    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer, ih As Integer
    Dim hs(255) As Long, hMax As Long, hMax2 As Long
    Dim c As Long, cC As Integer
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim sx As Single, sy As Single, sv As Single
    Dim pp As Integer, qq As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    frmResult.picView.Cls
    frmResult.picView.Width = 284!
    frmResult.picView.Height = 224!
    
    frmResult.picView.Line (9!, 9!)-(266!, 210!), vbWhite, B
    
    frmResult.picView.DrawStyle = vbDot
    frmResult.picView.Line (8!, 60!)-(268!, 60!), vbWhite
    frmResult.picView.Line (8!, 110!)-(268!, 110!), vbWhite
    frmResult.picView.Line (8!, 160!)-(268!, 160!), vbWhite
    For i = 0 To 15
        sx = 10! + CSng(16 * i)
    frmResult.picView.Line (sx, 8!)-(sx, 212!), vbWhite
    Next i
    frmResult.picView.DrawStyle = vbSolid
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cR = CInt(c Mod &H100&)
            cG = CInt((c \ &H100&) Mod &H100&)
            cB = CInt((c \ &H100&) \ &H100&)
            If frmMain.optHist2C(0).Value Then
                cC = cR
            ElseIf frmMain.optHist2C(1).Value Then
                cC = cG
            ElseIf frmMain.optHist2C(2).Value Then
                cC = cB
            Else
                cC = CInt(0.3! * CSng(cR) + 0.59! * CSng(cG) + 0.11! * CSng(cB))
            End If
            hs(cC) = hs(cC) + 1&
            If hMax < hs(cC) Then hMax = hs(cC)
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50!
    Next i
    
    
    If frmMain.optHist2(1).Value Then
        For i = 1 To 255
            hs(i) = hs(i) + hs(i - 1)
        Next i
        hMax = CLng(dw) * CLng(dh)
    ElseIf frmMain.chkHist2B.Value = vbChecked Then
        sv = CSng(frmMain.sldHist2.Value)
        frmResult.picView.CurrentX = 0!
        frmResult.picView.CurrentY = 210!
        frmResult.picView.ForeColor = vbWhite
        frmResult.picView.Print "1/" & Format$(sv)
    ElseIf frmMain.chkHist2.Value = vbChecked Then
        pp = 0
        For i = 0 To 255
            If hs(i) > 0& Then
                pp = pp + 1
            End If
        Next i
        hMax2 = hMax
        For j = 1 To 199
            qq = 0
            For i = 0 To 255
                If hs(i) > ((hMax2 * CLng(j)) \ 200&) Then
                    qq = qq + 1
                End If
            Next i
            If frmMain.chkHist2A.Value = vbChecked Then
                If qq <= (pp \ 5) Then
                    hMax = (hMax2 * CLng(j)) \ 200&
                    frmResult.picView.CurrentX = 0!
                    frmResult.picView.CurrentY = 210!
                    frmResult.picView.ForeColor = vbWhite
                    frmResult.picView.Print Format$(j) & "/200"
                    Exit For
                End If
            Else
                If qq <= (pp \ 10) Then
                    hMax = (hMax2 * CLng(j)) \ 200&
                    frmResult.picView.CurrentX = 0!
                    frmResult.picView.CurrentY = 210!
                    frmResult.picView.ForeColor = vbWhite
                    frmResult.picView.Print Format$(j) & "/200"
                    Exit For
                End If
            End If
        Next j
    End If
    
    
    For i = 0 To 255
        sx = CSng(i + 10)
        If frmMain.optHist2(1).Value Then
            sy = Int(200! * (CSng(hs(i)) / CSng(hMax)))
        ElseIf frmMain.chkHist2B.Value = vbChecked Then
            sy = Int((200! * CSng(hs(i))) / (CSng(dw) * CSng(dh) / sv))
            If sy > 300! Then sy = 300!
        Else
            sy = Int(200! * (CSng(hs(i)) / CSng(hMax)))
        End If
        ih = CInt(sy)
        For j = 0 To ih - 1
            sy = 210! - CSng(ih - j)
            frmResult.picView.PSet (sx, sy), vbBlack
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / 256! * 50! + 50!
    Next i
    
    Screen.MousePointer = vbArrow
    
End Sub

Public Sub RuisekiHist(sflag As Integer)

    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim c As Long, cC As Integer
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim hs(255) As Long, ss As String
    Dim sx As Single, sy As Single, sh As Single

    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    

    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cR = CInt(c Mod &H100&)
            cG = CInt((c \ &H100&) Mod &H100&)
            cB = CInt((c \ &H100&) \ &H100&)
            cC = CInt(0.3! * CSng(cR) + 0.59! * CSng(cG) + 0.11! * CSng(cB))
            Select Case sflag
            Case 0
                hs(cR) = hs(cR) + 1&
            Case 1
                hs(cG) = hs(cG) + 1&
            Case 2
                hs(cB) = hs(cB) + 1&
            Case 3
                hs(cC) = hs(cC) + 1&
            End Select
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 65!
    Next i
    
    frmHist.lstHist2.Clear
        ss = Format$(0) & " (" & Format$(hs(0)) & ")" _
            & " [" & Format$(100# * CDbl(hs(0)) _
                    / CDbl(dw) / CDbl(dh), "0.000") & "%]"
        frmHist.lstHist2.AddItem ss
    For i = 1 To 255
        ss = Format$(i) & " (" & Format$(hs(i)) & ")" _
            & " [" & Format$(100# * CDbl(hs(i)) _
                    / CDbl(dw) / CDbl(dh), "0.000") & "%]"
        frmHist.lstHist2.AddItem ss
        hs(i) = hs(i) + hs(i - 1)
        frmMain.prgShinko.Value = CSng(i) / 255! * 10! + 65!
    Next i
    
    frmHist.picView.Cls
    
    sh = frmHist.picView.ScaleHeight
    sy = sh - 10!
    frmHist.picView.Line (5!, sy)-(261!, sy), vbBlack
    sy = 10!
    frmHist.picView.Line (5!, sy)-(261!, sy)
    sy = (sh - 20!) / 4! + 10!
    frmHist.picView.Line (5!, sy)-(261!, sy)
    sy = (sh - 20!) / 2! + 10!
    frmHist.picView.Line (5!, sy)-(261!, sy)
    sy = (sh - 20!) * 3! / 4! + 10!
    frmHist.picView.Line (5!, sy)-(261!, sy)
    For i = 0 To 255
        sx = CSng(i + 5)
        If (i Mod &H10) = 0 Then
            frmHist.picView.DrawStyle = vbDot
            frmHist.picView.Line (sx, 10!)-(sx, sh - 10!)
            frmHist.picView.DrawStyle = vbSolid
        End If
        sy = (1! - CSng(hs(i)) / CSng(hs(255))) * (sh - 20!) + 10!
        frmHist.picView.Line (sx, sy)-(sx, sh - 10!), vbBlack
        frmMain.prgShinko.Value = CSng(i + 1) / 256! * 15! + 75!
    Next i
    
    frmHist.lstHist.Clear
    For i = 0 To 255
        ss = Format$(i) & " (" & Format$(hs(i)) & ")" _
            & " [" & Format$(100# * CDbl(hs(i)) _
                    / CDbl(hs(255)), "0.000") & "%]"
        frmHist.lstHist.AddItem ss
        frmMain.prgShinko.Value = CSng(i + 1) / 256! * 10! + 90!
    Next i
    
    Select Case sflag
    Case 0
        frmHist.Caption = "�ݐσq�X�g�O���� ��"
    Case 1
        frmHist.Caption = "�ݐσq�X�g�O���� ��"
    Case 2
        frmHist.Caption = "�ݐσq�X�g�O���� ��"
    Case 3
        frmHist.Caption = "�ݐσq�X�g�O���� �P�x"
    End Select
    
    Screen.MousePointer = vbArrow
    
    frmHist.Show vbModeless
            
End Sub


Public Sub showHist(Index As Integer)

    Dim hs(256) As Long
    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim c As Long, cC As Integer
    Dim mx As Long, sh As Single
    Dim sx As Integer, sy As Single
    Dim sc As Single, scav As Double, scv As Double
    Dim ss As String
    Dim mm As Integer, nn As Integer
    Dim cenc As Integer
    Dim maxc As Integer, minc As Integer
    Dim d1 As Double, d2 As Double
    Dim d3 As Double, d4 As Double
    Dim cenc2 As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    mx = 0&
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            Select Case Index
            Case 0
                cC = CInt(c Mod &H100&)
            Case 1
                cC = CInt((c \ &H100&) Mod &H100&)
            Case 2
                cC = CInt((c \ &H100&) \ &H100&)
            Case 3
                sc = 0.3! * CSng(c Mod &H100&) _
                        + 0.59! * CSng((c \ &H100&) Mod &H100&) _
                        + 0.11! * CSng((c \ &H100&) \ &H100&)
                cC = CInt(sc)
            End Select
            hs(cC) = hs(cC) + 1&
            If mx < hs(cC) Then mx = hs(cC)
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    

    scav = 0#
    mm = 0: nn = 0: maxc = -1: minc = &H100
    For i = 0 To 255
        scav = scav + CDbl(i + 1) * CDbl(hs(i)) _
                        / CDbl(dw) / CDbl(dh)
        If hs(i) > 0& Then
            nn = nn + 1
            If maxc < i Then
                maxc = i
            End If
            If minc > i Then
                minc = i
            End If
        End If
        If hs(i) = mx Then
            mm = i
        End If
    Next i
    scav = scav - 1#
    
    d1 = 0#
    scv = 0#
    cenc = -1: cenc2 = -1
    For i = 0 To 255
        If hs(i) > 0& Then
            If cenc < 0 Then
                d2 = d1
                d1 = d1 + CDbl(hs(i)) / CDbl(dw) / CDbl(dh)
                If d1 >= 0.5 Then
                    d3 = Sqr((d1 - 0.5) * (d1 - 0.5))
                    d4 = Sqr((d2 - 0.5) * (d2 - 0.5))
                    If d3 > d4 Then
                        cenc = cenc2
                    Else
                        cenc = i
                    End If
                End If
                cenc2 = i
            End If
            scv = scv + CDbl(hs(i)) / CDbl(dw) / CDbl(dh) _
                    * (CDbl(i) - scav) * (CDbl(i) - scav)
        End If
    Next i
    
    
    frmHist.lstHist.Clear
    frmHist.lstHist2.Clear
    frmHist.picView.Cls
    
    
    sh = frmHist.picView.ScaleHeight
    sy = sh - 10!
    frmHist.picView.Line (5!, sy)-(261!, sy), vbBlack
    sy = 10!
    frmHist.picView.Line (5!, sy)-(261!, sy)
    sy = (sh - 20!) / 4! + 10!
    frmHist.picView.Line (5!, sy)-(261!, sy)
    sy = (sh - 20!) / 2! + 10!
    frmHist.picView.Line (5!, sy)-(261!, sy)
    sy = (sh - 20!) * 3! / 4! + 10!
    frmHist.picView.Line (5!, sy)-(261!, sy)
    
    sy = (1! - CSng(dw) * CSng(dh) / CSng(nn) / CSng(mx)) _
                    * (sh - 20!) + 10!
    frmHist.picView.DrawStyle = vbDot
    frmHist.picView.Line (5!, sy)-(261!, sy), vbYellow
    frmHist.picView.DrawStyle = vbSolid
    
    
    For i = 0 To 255
        sx = CSng(i + 5)
        If (i Mod &H10) = 0 Then
            frmHist.picView.DrawStyle = vbDot
            frmHist.picView.Line (sx, 10!)-(sx, sh - 10!)
            frmHist.picView.DrawStyle = vbSolid
        End If
        sy = (1! - CSng(hs(i)) / CSng(mx)) * (sh - 20!) + 10!
        frmHist.picView.Line (sx, sy)-(sx, sh - 10!), vbBlack
        If hs(i) > 0& Then
            ss = Format$(i) & " (" & Format$(hs(i)) & ") [" _
                    & Format$(100# * CDbl(hs(i)) / CDbl(dw) / CDbl(dh), "0.000") & "%]"
            frmHist.lstHist2.AddItem ss
        End If
    Next i
    
    ss = "Width = " & Format$(dw)
    frmHist.lstHist.AddItem ss
    ss = "Height = " & Format$(dh)
    frmHist.lstHist.AddItem ss
    ss = "��f�� = " & Format$(CLng(dw) * CLng(dh))
    frmHist.lstHist.AddItem ss
    ss = "�Z�x�l�K�� = " & Format$(nn)
    frmHist.lstHist.AddItem ss
    ss = "�Z�x���ϓx�� = " & Format$(CDbl(dw) * CDbl(dh) / CDbl(nn), "0.00")
    frmHist.lstHist.AddItem ss
    ss = "�ŕp�Z�x�l = " & Format$(mm)
    frmHist.lstHist.AddItem ss
    ss = "�ŕp�x�� = " & Format$(hs(mm))
    frmHist.lstHist.AddItem ss
    ss = "�ŕp���̊��� = " & Format$(100# * CDbl(hs(mm)) / CDbl(dw) / CDbl(dh), "0.00") & " %"
    frmHist.lstHist.AddItem ss
    ss = "���ϔZ�x�l = " & Format$(scav, "0.0")
    frmHist.lstHist.AddItem ss
    ss = "�ő�Z�x�l = " & Format$(maxc)
    frmHist.lstHist.AddItem ss
    ss = "�ŏ��Z�x�l = " & Format$(minc)
    frmHist.lstHist.AddItem ss
    ss = "�Z�x�����l = " & Format$(cenc)
    frmHist.lstHist.AddItem ss
    ss = "�Z�x�l���U = " & Format$(scv, "0.00")
    frmHist.lstHist.AddItem ss
    ss = "�Z�x�l�W���΍� = " & Format$(Sqr(scv), "0.00")
    frmHist.lstHist.AddItem ss
    ss = "(�ő�+�ŏ�)/2 = " & Format$(CDbl(maxc + minc) / 2#, "0.0")
    frmHist.lstHist.AddItem ss
    
    Select Case Index
    Case 0
        frmHist.Caption = "�q�X�g�O���� ��"
    Case 1
        frmHist.Caption = "�q�X�g�O���� ��"
    Case 2
        frmHist.Caption = "�q�X�g�O���� ��"
    Case 3
        frmHist.Caption = "�q�X�g�O���� �P�x"
    End Select
    Screen.MousePointer = vbArrow
    
    frmHist.Show vbModeless
    
End Sub
