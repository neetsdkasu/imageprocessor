Attribute VB_Name = "Module2chi"
Option Explicit


Public Sub Shori2chiAnd()

    Dim jkNu(2) As Integer, jkNl(2) As Integer
    Dim jkKu(2) As Long, jkKl(2) As Long
    Dim i As Integer, j As Integer
    Dim c As Long, cC As Integer
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim dw As Integer, dh As Integer
    Dim pp As Integer, qq As Integer
    Dim hs(255) As Long, dv As Double
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    pp = 0
    qq = 0
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    For i = 0 To 255
        hs(i) = 0&
    Next i
    
    If frmMain.opt2chiC(0).Value Then
        For i = 0 To dh - 1: For j = 0 To dw - 1
                c = frmLoad.picView.Point(CSng(j), CSng(i))
                cR = CInt(c Mod &H100&)
                hs(cR) = hs(cR) + 1&
            Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 10!
        Next i
    ElseIf frmMain.opt2chiC(1).Value Then
        For i = 0 To dh - 1: For j = 0 To dw - 1
                c = frmLoad.picView.Point(CSng(j), CSng(i))
                cG = CInt((c \ &H100&) Mod &H100&)
                hs(cG) = hs(cG) + 1&
            Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 10!
        Next i
    ElseIf frmMain.opt2chiC(2).Value Then
        For i = 0 To dh - 1: For j = 0 To dw - 1
                c = frmLoad.picView.Point(CSng(j), CSng(i))
                cB = CInt((c \ &H100&) \ &H100&)
                hs(cB) = hs(cB) + 1&
            Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 10!
        Next i
    ElseIf frmMain.opt2chiC(3).Value Then
        For i = 0 To dh - 1: For j = 0 To dw - 1
                c = frmLoad.picView.Point(CSng(j), CSng(i))
                cR = CInt(c Mod &H100&)
                cG = CInt((c \ &H100&) Mod &H100&)
                cB = CInt((c \ &H100&) \ &H100&)
                cC = CInt(0.3! * CSng(cR) + 0.59! * CSng(cG) + 0.11! * CSng(cB))
                hs(cC) = hs(cC) + 1&
            Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 10!
        Next i
    End If
    
    
    For i = 0 To 2
        If frmMain.chk2chiN(i).Value = vbChecked Then
            jkNu(i) = CInt(Val(frmMain.txt2chiNUpper(i).Text))
            jkNl(i) = CInt(Val(frmMain.txt2chiNLower(i).Text))
        Else
            jkNu(i) = -1
            jkNl(i) = -1
            pp = pp + 1
        End If
        If frmMain.chk2chiK(i).Value = vbChecked Then
            dv = Val(frmMain.txt2chiKUpper(i).Text)
            dv = (CDbl(dw) * CDbl(dh)) * (dv / 100#)
            jkKu(i) = CLng(dv)
            dv = Val(frmMain.txt2chiKLower(i).Text)
            dv = (CDbl(dw) * CDbl(dh)) * (dv / 100#)
            jkKl(i) = CLng(dv)
        Else
            jkKu(i) = -1&
            jkKl(i) = -1&
            qq = qq + 1
        End If
    Next i
    
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cR = CInt(c Mod &H100&)
            cG = CInt((c \ &H100&) Mod &H100&)
            cB = CInt((c \ &H100&) \ &H100&)
            cC = CInt(0.3! * CSng(cR) + 0.59! * CSng(cG) + 0.11! * CSng(cB))
            If frmMain.opt2chiC(0).Value Then
                If (((cR >= jkNl(0)) And (cR <= jkNu(0))) _
                            Or ((cR >= jkNl(1)) And (cR <= jkNu(1))) _
                            Or ((cR >= jkNl(2)) And (cR <= jkNu(2))) _
                            Or (pp = 3)) _
                        And (((hs(cR) >= jkKl(0)) And (hs(cR) <= jkKu(0))) _
                            Or ((hs(cR) >= jkKl(1)) And (hs(cR) <= jkKu(1))) _
                            Or ((hs(cR) >= jkKl(2)) And (hs(cR) <= jkKu(2))) _
                            Or (qq = 3)) Then
                    If frmMain.chk2chi.Value = vbUnchecked Then
                        If frmMain.opt2chi01(1).Value Then
                            cR = 0
                        Else
                            cR = &HFF
                        End If
                    End If
                Else
                    If frmMain.opt2chi01(0).Value Then
                        cR = 0
                    Else
                        cR = &HFF
                    End If
                End If
            ElseIf frmMain.opt2chiC(1).Value Then
                If (((cG >= jkNl(0)) And (cG <= jkNu(0))) _
                            Or ((cG >= jkNl(1)) And (cG <= jkNu(1))) _
                            Or ((cG >= jkNl(2)) And (cG <= jkNu(2))) _
                            Or (pp = 3)) _
                        And (((hs(cG) >= jkKl(0)) And (hs(cG) <= jkKu(0))) _
                            Or ((hs(cG) >= jkKl(1)) And (hs(cG) <= jkKu(1))) _
                            Or ((hs(cG) >= jkKl(2)) And (hs(cG) <= jkKu(2))) _
                            Or (qq = 3)) Then
                    If frmMain.chk2chi.Value = vbUnchecked Then
                        If frmMain.opt2chi01(1).Value Then
                            cG = 0
                        Else
                            cG = &HFF
                        End If
                    End If
                Else
                    If frmMain.opt2chi01(0).Value Then
                        cG = 0
                    Else
                        cG = &HFF
                    End If
                End If
            ElseIf frmMain.opt2chiC(2).Value Then
                If (((cB >= jkNl(0)) And (cB <= jkNu(0))) _
                            Or ((cB >= jkNl(1)) And (cB <= jkNu(1))) _
                            Or ((cB >= jkNl(2)) And (cB <= jkNu(2))) _
                            Or (pp = 3)) _
                        And (((hs(cB) >= jkKl(0)) And (hs(cB) <= jkKu(0))) _
                            Or ((hs(cB) >= jkKl(1)) And (hs(cB) <= jkKu(1))) _
                            Or ((hs(cB) >= jkKl(2)) And (hs(cB) <= jkKu(2))) _
                            Or (qq = 3)) Then
                    If frmMain.chk2chi.Value = vbUnchecked Then
                        If frmMain.opt2chi01(1).Value Then
                            cB = 0
                        Else
                            cB = &HFF
                        End If
                    End If
                Else
                    If frmMain.opt2chi01(0).Value Then
                        cB = 0
                    Else
                        cB = &HFF
                    End If
                End If
            ElseIf frmMain.opt2chiC(3).Value Then
                If (((cC >= jkNl(0)) And (cC <= jkNu(0))) _
                            Or ((cC >= jkNl(1)) And (cC <= jkNu(1))) _
                            Or ((cC >= jkNl(2)) And (cC <= jkNu(2))) _
                            Or (pp = 3)) _
                        And (((hs(cC) >= jkKl(0)) And (hs(cC) <= jkKu(0))) _
                            Or ((hs(cC) >= jkKl(1)) And (hs(cC) <= jkKu(1))) _
                            Or ((hs(cC) >= jkKl(2)) And (hs(cC) <= jkKu(2))) _
                            Or (qq = 3)) Then
                    If frmMain.chk2chi.Value = vbUnchecked Then
                        If frmMain.opt2chi01(1).Value Then
                            cR = 0: cG = 0: cB = 0
                        Else
                            cR = &HFF: cG = &HFF: cB = &HFF
                        End If
                    End If
                Else
                    If frmMain.opt2chi01(0).Value Then
                        cR = 0: cG = 0: cB = 0
                    Else
                        cR = &HFF: cG = &HFF: cB = &HFF
                    End If
                End If
            End If
            c = RGB(cR, cG, cB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 90! + 10!
    Next i
        
    Screen.MousePointer = vbArrow

End Sub
Public Sub Shori2chiKokkaku(n1 As Integer, c3 As Long, c4 As Long)
    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim c1 As Long, c2 As Long
    Dim pp As Integer, qq As Integer
    Dim pL As PictureBox, pR As PictureBox
    Dim pT As PictureBox, pT2 As PictureBox
    Dim pML As PictureBox, prgS As ProgressBar

    Set pL = frmLoad.picView
    Set pR = frmResult.picView
    Set pT = frmMain.picTemp
    Set pT2 = frmMain.picTemp2
    Set pML = frmMain.picLoad
    Set prgS = frmMain.prgShinko

    Screen.MousePointer = vbHourglass
    prgS.Value = 0!

    dw = CInt(pL.ScaleWidth)
    dh = CInt(pL.ScaleHeight)

    frmNichi.Caption = "�O������"

    pp = 0
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c1 = pL.Point(CSng(j), CSng(i))
            If (c1 <> vbBlack) And (c1 <> vbWhite) Then
                pp = 1
                Exit For
            End If
        Next j
        If pp = 1 Then
            Exit For
        End If
        prgS.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    If pp = 1 Then
        Screen.MousePointer = vbArrow
        Exit Sub
    End If

    pR.Cls
    pR.Width = pL.Width
    pR.Height = pL.Height
    
    pT.Cls
    pT.Width = pL.Width
    pT.Height = pL.Height
    
    pT2.Cls
    pT2.Width = pL.Width
    pT2.Height = pL.Height
    
    pML.Cls
    pML.Picture = pL.Image
    
    prgS.Value = 0!
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            pR.PSet (CSng(j), CSng(i)), vbBlack
        Next j
        prgS.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    qq = 0
    Do
        frmNichi.Caption = "�v�Z�� (" & Format$(qq) & ")"
        qq = qq + 1
        If n1 = 4 Then
            Shori2chiM4Kin pML, pT, c3, c4
        Else
            Shori2chiM8Kin pML, pT, c3, c4
        End If
        Shori2chiM4Kin pT, pT2, c4, c3
        Screen.MousePointer = vbHourglass
        prgS.Value = 0!
        For i = 0 To dh - 1
            For j = 0 To dw - 1
                c1 = pML.Point(CSng(j), CSng(i))
                c2 = pT2.Point(CSng(j), CSng(i))
                If c1 <> c2 Then
                    pR.PSet (CSng(j), CSng(i)), vbWhite
                End If
            Next j
            prgS.Value = CSng(i + 1) / CSng(dh) * 100!
        Next i
        If n1 = 4 Then
            Shori2chiM4Kin pML, pT, c3, c4
        Else
            Shori2chiM8Kin pML, pT, c3, c4
        End If
        Screen.MousePointer = vbHourglass
        pp = 0
        prgS.Value = 0!
        For i = 0 To dh - 1
            For j = 0 To dw - 1
                c1 = pT.Point(CSng(j), CSng(i))
                If c1 <> c4 Then
                    pp = 1
                    Exit For
                End If
            Next j
            If pp = 1 Then
                Exit For
            End If
            prgS.Value = CSng(i + 1) / CSng(dh) * 100!
        Next i
        pML.Picture = pT.Image
    Loop While (pp <> 0)
    
    frmNichi.Caption = "�Q�l�摜�_�����Z����"
    Screen.MousePointer = vbArrow

End Sub

Public Sub Shori2chiM8Kin(pL As PictureBox, pR As PictureBox, _
                        c1 As Long, c2 As Long)
    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim dw As Integer, dh As Integer
    Dim c As Long, cC As Integer
    Dim pp As Integer
    Dim sx As Single, sy As Single
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    dw = CInt(pL.ScaleWidth)
    dh = CInt(pL.ScaleHeight)
    
    pR.Cls
    pR.Width = pL.Width
    pR.Height = pL.Height
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            pp = 0
            For m = -1 To 1
                sy = CSng(i + m)
                If sy < 0! Then sy = 0!
                If sy > CSng(dh - 1) Then sy = CSng(dh - 1)
                For n = -1 To 1
                    sx = CSng(j + n)
                    If sx < 0! Then sx = 0!
                    If sx > CSng(dw - 1) Then sx = CSng(dw - 1)
                    c = pL.Point(sx, sy)
                    If c = c1 Then
                        pp = pp + 1
                    End If
                Next n
            Next m
            If pp = 9 Then
                c = c1
            Else
                c = c2
            End If
            pR.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub

Public Sub Shori2chiKaiHei(n1 As Integer, n2 As Integer, _
                    n3 As Integer, c1 As Long, c2 As Long)
    Dim i As Integer
    Dim pL As PictureBox
    Dim pR As PictureBox
    Dim pT As PictureBox
    
    Set pL = frmLoad.picView
    Set pR = frmResult.picView
    Set pT = frmMain.picTemp
    
    pT.Cls
    pT.Width = pL.Width
    pT.Height = pL.Height
    pT.PaintPicture pL.Image, 0!, 0!
    
    If n2 = 4 Then
        For i = 0 To n1 - 1
            Call Shori2chiM4Kin(pT, pR, c1, c2)
            pT.PaintPicture pR.Image, 0!, 0!
        Next i
    Else
        For i = 0 To n1 - 1
            Call Shori2chiM8Kin(pT, pR, c1, c2)
            pT.PaintPicture pR.Image, 0!, 0!
        Next i
    End If
    
    If n3 = 4 Then
        For i = 0 To n1 - 1
            Call Shori2chiM4Kin(pT, pR, c2, c1)
            pT.PaintPicture pR.Image, 0!, 0!
        Next i
    Else
        For i = 0 To n1 - 1
            Call Shori2chiM8Kin(pT, pR, c2, c1)
            pT.PaintPicture pR.Image, 0!, 0!
        Next i
    End If
    
End Sub

Public Sub Shori2chiOr()

    Dim jkNu(2) As Integer, jkNl(2) As Integer
    Dim jkKu(2) As Long, jkKl(2) As Long
    Dim i As Integer, j As Integer
    Dim c As Long, cC As Integer
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim dw As Integer, dh As Integer
    Dim pp As Integer, qq As Integer
    Dim hs(255) As Long
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    pp = 0
    qq = 0
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    For i = 0 To 255
        hs(i) = 0&
    Next i
    
    If frmMain.opt2chiC(0).Value Then
        For i = 0 To dh - 1: For j = 0 To dw - 1
                c = frmLoad.picView.Point(CSng(j), CSng(i))
                cR = CInt(c Mod &H100&)
                hs(cR) = hs(cR) + 1&
            Next j
            frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 20!
        Next i
    ElseIf frmMain.opt2chiC(1).Value Then
        For i = 0 To dh - 1: For j = 0 To dw - 1
                c = frmLoad.picView.Point(CSng(j), CSng(i))
                cG = CInt((c \ &H100&) Mod &H100&)
                hs(cG) = hs(cG) + 1&
            Next j
            frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 20!
        Next i
    ElseIf frmMain.opt2chiC(2).Value Then
        For i = 0 To dh - 1: For j = 0 To dw - 1
                c = frmLoad.picView.Point(CSng(j), CSng(i))
                cB = CInt((c \ &H100&) \ &H100&)
                hs(cB) = hs(cB) + 1&
            Next j
            frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 20!
        Next i
    ElseIf frmMain.opt2chiC(3).Value Then
        For i = 0 To dh - 1: For j = 0 To dw - 1
                c = frmLoad.picView.Point(CSng(j), CSng(i))
                cR = CInt(c Mod &H100&)
                cG = CInt((c \ &H100&) Mod &H100&)
                cB = CInt((c \ &H100&) \ &H100&)
                cC = CInt(0.3! * CSng(cR) + 0.59! * CSng(cG) + 0.11! * CSng(cB))
                hs(cC) = hs(cC) + 1&
            Next j
            frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 20!
        Next i
    End If
    
    
    For i = 0 To 2
        If frmMain.chk2chiN(i).Value = vbChecked Then
            jkNu(i) = CInt(Val(frmMain.txt2chiNUpper(i).Text))
            jkNl(i) = CInt(Val(frmMain.txt2chiNLower(i).Text))
        Else
            jkNu(i) = -1
            jkNl(i) = -1
            pp = pp + 1
        End If
        If frmMain.chk2chiK(i).Value = vbChecked Then
            jkKu(i) = CLng(Val(frmMain.txt2chiKUpper(i).Text))
            jkKl(i) = CLng(Val(frmMain.txt2chiKLower(i).Text))
        Else
            jkKu(i) = -1&
            jkKl(i) = -1&
            qq = qq + 1
        End If
    Next i
    
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cR = CInt(c Mod &H100&)
            cG = CInt((c \ &H100&) Mod &H100&)
            cB = CInt((c \ &H100&) \ &H100&)
            cC = CInt(0.3! * CSng(cR) + 0.59! * CSng(cG) + 0.11! * CSng(cB))
            If frmMain.opt2chiC(0).Value Then
                If (((cR >= jkNl(0)) And (cR <= jkNu(0))) _
                            Or ((cR >= jkNl(1)) And (cR <= jkNu(1))) _
                            Or ((cR >= jkNl(2)) And (cR <= jkNu(2)))) _
                        Or (((hs(cR) >= jkKl(0)) And (hs(cR) <= jkKu(0))) _
                            Or ((hs(cR) >= jkKl(1)) And (hs(cR) <= jkKu(1))) _
                            Or ((hs(cR) >= jkKl(2)) And (hs(cR) <= jkKu(2)))) Then
                    If frmMain.chk2chi.Value = vbUnchecked Then
                        If frmMain.opt2chi01(1).Value Then
                            cR = 0
                        Else
                            cR = &HFF
                        End If
                    End If
                Else
                    If frmMain.opt2chi01(0).Value Then
                        cR = 0
                    Else
                        cR = &HFF
                    End If
                End If
            ElseIf frmMain.opt2chiC(1).Value Then
                If (((cG >= jkNl(0)) And (cG <= jkNu(0))) _
                            Or ((cG >= jkNl(1)) And (cG <= jkNu(1))) _
                            Or ((cG >= jkNl(2)) And (cG <= jkNu(2)))) _
                        Or (((hs(cG) >= jkKl(0)) And (hs(cG) <= jkKu(0))) _
                            Or ((hs(cG) >= jkKl(1)) And (hs(cG) <= jkKu(1))) _
                            Or ((hs(cG) >= jkKl(2)) And (hs(cG) <= jkKu(2)))) Then
                    If frmMain.chk2chi.Value = vbUnchecked Then
                        If frmMain.opt2chi01(1).Value Then
                            cG = 0
                        Else
                            cG = &HFF
                        End If
                    End If
                Else
                    If frmMain.opt2chi01(0).Value Then
                        cG = 0
                    Else
                        cG = &HFF
                    End If
                End If
            ElseIf frmMain.opt2chiC(2).Value Then
                If (((cB >= jkNl(0)) And (cB <= jkNu(0))) _
                            Or ((cB >= jkNl(1)) And (cB <= jkNu(1))) _
                            Or ((cB >= jkNl(2)) And (cB <= jkNu(2)))) _
                        Or (((hs(cB) >= jkKl(0)) And (hs(cB) <= jkKu(0))) _
                            Or ((hs(cB) >= jkKl(1)) And (hs(cB) <= jkKu(1))) _
                            Or ((hs(cB) >= jkKl(2)) And (hs(cB) <= jkKu(2)))) Then
                    If frmMain.chk2chi.Value = vbUnchecked Then
                        If frmMain.opt2chi01(1).Value Then
                            cB = 0
                        Else
                            cB = &HFF
                        End If
                    End If
                Else
                    If frmMain.opt2chi01(0).Value Then
                        cB = 0
                    Else
                        cB = &HFF
                    End If
                End If
            ElseIf frmMain.opt2chiC(3).Value Then
                If (((cC >= jkNl(0)) And (cC <= jkNu(0))) _
                            Or ((cC >= jkNl(1)) And (cC <= jkNu(1))) _
                            Or ((cC >= jkNl(2)) And (cC <= jkNu(2)))) _
                        Or (((hs(cC) >= jkKl(0)) And (hs(cC) <= jkKu(0))) _
                            Or ((hs(cC) >= jkKl(1)) And (hs(cC) <= jkKu(1))) _
                            Or ((hs(cC) >= jkKl(2)) And (hs(cC) <= jkKu(2)))) Then
                    If frmMain.chk2chi.Value = vbUnchecked Then
                        If frmMain.opt2chi01(1).Value Then
                            cR = 0: cG = 0: cB = 0
                        Else
                            cR = &HFF: cG = &HFF: cB = &HFF
                        End If
                    End If
                Else
                    If frmMain.opt2chi01(0).Value Then
                        cR = 0: cG = 0: cB = 0
                    Else
                        cR = &HFF: cG = &HFF: cB = &HFF
                    End If
                End If
            End If
            c = RGB(cR, cG, cB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 80! + 20!
    Next i
        
    Screen.MousePointer = vbArrow


End Sub


Public Sub Shori2chiOtsu()
    Dim i As Integer, j As Integer
    Dim c As Long, cC As Integer
    Dim dcR As Single, dcG As Single, dcB As Single
    Dim dw As Integer, dh As Integer
    Dim siki As Integer
    
    siki = ShikiiOtsu
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!

    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cC = Kido(c)
            If cC <= siki Then
                c = vbBlack
            Else
                c = vbWhite
            End If
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow
    
End Sub


Public Sub Shori2chiM4Kin(pL As PictureBox, pR As PictureBox, _
                        c1 As Long, c2 As Long)
    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim dw As Integer, dh As Integer
    Dim c As Long, cC As Integer
    Dim pp As Integer
    Dim sx As Single, sy As Single
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    dw = CInt(pL.ScaleWidth)
    dh = CInt(pL.ScaleHeight)
    
    pR.Cls
    pR.Width = pL.Width
    pR.Height = pL.Height
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            pp = 0
            sx = CSng(j)
                sy = CSng(i - 1)
                If sy < 0! Then sy = 0!
                c = pL.Point(sx, sy)
                If c = c1 Then pp = pp + 1
                sy = CSng(i + 1)
                If sy > CSng(dh - 1) Then sy = CSng(dh - 1)
                c = pL.Point(sx, sy)
                If c = c1 Then pp = pp + 1
            sy = CSng(i)
            c = pL.Point(sx, sy)
            If c = c1 Then pp = pp + 1
                sx = CSng(j - 1)
                If sx < 0! Then sx = 0!
                c = pL.Point(sx, sy)
                If c = c1 Then pp = pp + 1
                sx = CSng(j + 1)
                If sx > CSng(dw - 1) Then sx = CSng(dw - 1)
                c = pL.Point(sx, sy)
                If c = c1 Then pp = pp + 1
            If pp = 5 Then
                c = c1
            Else
                c = c2
            End If
            pR.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow


End Sub


Public Sub Shori2chiLogic(pL As PictureBox, pR As PictureBox, _
                    n1 As Integer, c1 As Long, c2 As Long)
    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim dw As Integer, dh As Integer
    Dim sx As Single, sy As Single
    Dim c As Long, pp As Integer
    Dim mk(2, 2) As Long
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    Select Case n1
    Case 0      '�_�E��
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 1      '�E�ʁE����
        mk(0, 0) = c2: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = c1: mk(2, 2) = c1
    Case 2      '���ʁE�E��
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c2
    Case 3      '��ʁE����
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = c2: mk(2, 2) = c2
    Case 4      '���ʁE�㉚
        mk(0, 0) = c2: mk(1, 0) = c2: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 5      '���[�_
        mk(0, 0) = c1: mk(1, 0) = c2: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 6      '���[�_
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 7      '��[�_
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c2: mk(2, 2) = c1
    Case 8      '�E�[�_
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 9      '�����[�_
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 10     '����[�_
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c2
    Case 11     '�E��[�_
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = c1: mk(2, 2) = c1
    Case 12     '�E���[�_
        mk(0, 0) = c2: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 13     '�p�@��
        mk(0, 0) = -1&: mk(1, 0) = c2: mk(2, 0) = c1
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 14     '�p�@��
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = -1&: mk(1, 2) = c2: mk(2, 2) = c1
    Case 15     '�p�@��
        mk(0, 0) = c1: mk(1, 0) = c2: mk(2, 0) = -1&
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 16     '�p�@��
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c1: mk(1, 2) = c2: mk(2, 2) = -1&
    Case 17     '�p�@��
        mk(0, 0) = c2: mk(1, 0) = -1&: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 18     '�p�@��
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = -1&: mk(2, 2) = c2
    Case 19     '�p�@��
        mk(0, 0) = c2: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = -1&: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = c1: mk(2, 2) = c1
    Case 20     '�p�@��
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = -1&
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c2
    Case 21     '�s�p�@�ڌn
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 22     '�s�p�@�ڌn �㉺���]
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c2
    Case 23     '�s�p�@�ڌn ���E���]
        mk(0, 0) = c2: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 24     '�s�p�@�ڌn �Ώ̔��]
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = c1: mk(2, 2) = c1
    Case 25     '�s�p�@�ڌn �_�b
        mk(0, 0) = c2: mk(1, 0) = c2: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 26     '�s�p�@�ڌn �b�^
        mk(0, 0) = c1: mk(1, 0) = c2: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 27     '�s�p�@�ڌn �^�b
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = c2: mk(2, 2) = c1
    Case 28     '�s�p�@�ڌn �b�_
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c2: mk(2, 2) = c2
    Case 29     '�݊p�@�Q�^
        mk(0, 0) = -1&: mk(1, 0) = -1&: mk(2, 0) = c2
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 30     '�݊p�@�_�Q
        mk(0, 0) = c2: mk(1, 0) = -1&: mk(2, 0) = -1&
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 31     '�݊p�@�^�P
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c2: mk(1, 2) = -1&: mk(2, 2) = -1&
    Case 32     '�݊p�@�P�_
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = -1&: mk(1, 2) = -1&: mk(2, 2) = c2
    Case 33     '�݊p
        mk(0, 0) = c1: mk(1, 0) = c2: mk(2, 0) = -1&
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = -1&
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c2
    Case 34     '�݊p
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = -1&
        mk(0, 2) = c1: mk(1, 2) = c2: mk(2, 2) = -1&
    Case 35     '�݊p
        mk(0, 0) = c2: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = -1&: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = -1&: mk(1, 2) = c2: mk(2, 2) = c1
    Case 36     '�݊p
        mk(0, 0) = -1&: mk(1, 0) = c2: mk(2, 0) = c1
        mk(0, 1) = -1&: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = c1: mk(2, 2) = c1
    Case 37     '�s���@��
        mk(0, 0) = c1: mk(1, 0) = c2: mk(2, 0) = c1
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c1
    Case 38     '�s���@��
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c1: mk(1, 2) = c2: mk(2, 2) = c1
    Case 39     '�s���@��
        mk(0, 0) = c1: mk(1, 0) = c2: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c1: mk(1, 2) = c2: mk(2, 2) = c1
    Case 40     '�s���@��
        mk(0, 0) = c1: mk(1, 0) = c2: mk(2, 0) = c1
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c2: mk(2, 2) = c1
    Case 41     '�\���@��
        mk(0, 0) = c1: mk(1, 0) = c2: mk(2, 0) = c1
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c1: mk(1, 2) = c2: mk(2, 2) = c1
    Case 42     '�s���@�ȁ{��
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = c1: mk(2, 2) = c2
    Case 43     '�s���@���{��
        mk(0, 0) = c2: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = c1: mk(2, 2) = c2
    Case 44     '�s���@���{��
        mk(0, 0) = c2: mk(1, 0) = c1: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = c1: mk(2, 2) = c1
    Case 45     '�s���@�Ɂ{��
        mk(0, 0) = c2: mk(1, 0) = c1: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c2
    Case 46     '�\���@�~
        mk(0, 0) = c2: mk(1, 0) = c1: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = c1: mk(2, 2) = c2
    Case 47     '�x��
        mk(0, 0) = c1: mk(1, 0) = c2: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c2: mk(1, 2) = c1: mk(2, 2) = c2
    Case 48     '�x��
        mk(0, 0) = c2: mk(1, 0) = c1: mk(2, 0) = c2
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c2: mk(2, 2) = c1
    Case 49     '�x��
        mk(0, 0) = c2: mk(1, 0) = c1: mk(2, 0) = c1
        mk(0, 1) = c1: mk(1, 1) = c2: mk(2, 1) = c2
        mk(0, 2) = c2: mk(1, 2) = c1: mk(2, 2) = c1
    Case 50     '�x��
        mk(0, 0) = c1: mk(1, 0) = c1: mk(2, 0) = c2
        mk(0, 1) = c2: mk(1, 1) = c2: mk(2, 1) = c1
        mk(0, 2) = c1: mk(1, 2) = c1: mk(2, 2) = c2
    End Select
    
    dw = CInt(pL.ScaleWidth)
    dh = CInt(pL.ScaleHeight)
    
    pR.Cls
    pR.Width = pL.Width
    pR.Height = pL.Height
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            pp = 0
            For m = 0 To 2
                sy = CSng(i + m - 1)
                If sy < 0! Then sy = 0!
                If sy > CSng(dh - 1) Then sy = CSng(dh - 1)
                For n = 0 To 2
                    sx = CSng(j + n - 1)
                    If sx < 0! Then sx = 0!
                    If sx > CSng(dw - 1) Then sx = CSng(dw - 1)
                    c = pL.Point(sx, sy)
                    If c = mk(n, m) Then
                        pp = pp + 1
                    End If
                    If mk(n, m) < 0 Then
                        pp = pp + 1
                    End If
                Next n
            Next m
            If pp = 9 Then
                c = c1
            Else
                c = pL.Point(CSng(j), CSng(i))
            End If
            pR.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow
    
End Sub


