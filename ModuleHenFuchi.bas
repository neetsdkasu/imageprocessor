Attribute VB_Name = "ModuleHenFuchi"
Option Explicit

Public Const HEN_1GEN As Integer = 0
Public Const HEN_2GEN As Integer = 1
Public Const HEN_SOBEL As Integer = 2





Public Sub MinMaxg1()

    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim sx As Single, sy As Single
    Dim dw As Integer, dh As Integer
    Dim c As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim ncR As Integer, ncG As Integer, ncB As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    dw = frmLoad.picView.ScaleWidth
    dh = frmLoad.picView.ScaleHeight
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            ncR = &HFF: ncG = &HFF: ncB = &HFF
            For m = -1 To 1
                sy = CSng(i + m)
                If sy < 0! Then sy = 0!
                If sy > CSng(dh - 1) Then sy = CSng(dh - 1)
                For n = -1 To 1
                    sx = CSng(j + n)
                    If sx < 0! Then sx = 0!
                    If sx > CSng(dw - 1) Then sx = CSng(dw - 1)
                    c = frmLoad.picView.Point(sx, sy)
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    If ncR > cR Then ncR = cR
                    If ncG > cG Then ncG = cG
                    If ncB > cB Then ncB = cB
                Next n
            Next m
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cR = CInt(c Mod &H100&) - ncR
            cG = CInt((c \ &H100&) Mod &H100&) - ncG
            cB = CInt((c \ &H100&) \ &H100&) - ncB
            c = RGB(cR, cG, cB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow
    

End Sub


Public Sub MinMaxg2()

    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim sx As Single, sy As Single
    Dim dw As Integer, dh As Integer
    Dim c As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim ncR As Integer, ncG As Integer, ncB As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    frmMain.picTemp.Cls
    frmMain.picTemp.Width = frmLoad.picView.Width
    frmMain.picTemp.Height = frmLoad.picView.Height
    
    dw = frmLoad.picView.ScaleWidth
    dh = frmLoad.picView.ScaleHeight
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            ncR = &HFF: ncG = &HFF: ncB = &HFF
            For m = -1 To 1
                sy = CSng(i + m)
                If sy < 0! Then sy = 0!
                If sy > CSng(dh - 1) Then sy = CSng(dh - 1)
                For n = -1 To 1
                    sx = CSng(j + n)
                    If sx < 0! Then sx = 0!
                    If sx > CSng(dw - 1) Then sx = CSng(dw - 1)
                    c = frmLoad.picView.Point(sx, sy)
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    If ncR > cR Then ncR = cR
                    If ncG > cG Then ncG = cG
                    If ncB > cB Then ncB = cB
                Next n
            Next m
            c = RGB(ncR, ncG, ncB)
            frmMain.picTemp.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50!
    Next i
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            ncR = 0: ncG = 0: ncB = 0
            For m = -1 To 1
                sy = CSng(i + m)
                If sy < 0! Then sy = 0!
                If sy > CSng(dh - 1) Then sy = CSng(dh - 1)
                For n = -1 To 1
                    sx = CSng(j + n)
                    If sx < 0! Then sx = 0!
                    If sx > CSng(dw - 1) Then sx = CSng(dw - 1)
                    c = frmMain.picTemp.Point(sx, sy)
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    If ncR < cR Then ncR = cR
                    If ncG < cG Then ncG = cG
                    If ncB < cB Then ncB = cB
                Next n
            Next m
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cR = CInt(c Mod &H100&) - ncR
            cG = CInt((c \ &H100&) Mod &H100&) - ncG
            cB = CInt((c \ &H100&) \ &H100&) - ncB
            If cR < 0 Then cR = 0
            If cG < 0 Then cG = 0
            If cB < 0 Then cB = 0
            c = RGB(cR, cG, cB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50! + 50!
    Next i
    
    Screen.MousePointer = vbArrow
    

End Sub

Public Sub MinMaxg3()

    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim sx As Single, sy As Single
    Dim dw As Integer, dh As Integer
    Dim c As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim ncR As Integer, ncG As Integer, ncB As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    dw = frmLoad.picView.ScaleWidth
    dh = frmLoad.picView.ScaleHeight
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            ncR = 0: ncG = 0: ncB = 0
            For m = -1 To 1
                sy = CSng(i + m)
                If sy < 0! Then sy = 0!
                If sy > CSng(dh - 1) Then sy = CSng(dh - 1)
                For n = -1 To 1
                    sx = CSng(j + n)
                    If sx < 0! Then sx = 0!
                    If sx > CSng(dw - 1) Then sx = CSng(dw - 1)
                    c = frmLoad.picView.Point(sx, sy)
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    If ncR < cR Then ncR = cR
                    If ncG < cG Then ncG = cG
                    If ncB < cB Then ncB = cB
                Next n
            Next m
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cR = ncR - CInt(c Mod &H100&)
            cG = ncG - CInt((c \ &H100&) Mod &H100&)
            cB = ncB - CInt((c \ &H100&) \ &H100&)
            c = RGB(cR, cG, cB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow
    
End Sub

Public Sub MinMaxg4()

    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim sx As Single, sy As Single
    Dim dw As Integer, dh As Integer
    Dim c As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim ncR As Integer, ncG As Integer, ncB As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    frmMain.picTemp.Cls
    frmMain.picTemp.Width = frmLoad.picView.Width
    frmMain.picTemp.Height = frmLoad.picView.Height
    
    dw = frmLoad.picView.ScaleWidth
    dh = frmLoad.picView.ScaleHeight
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            ncR = 0: ncG = 0: ncB = 0
            For m = -1 To 1
                sy = CSng(i + m)
                If sy < 0! Then sy = 0!
                If sy > CSng(dh - 1) Then sy = CSng(dh - 1)
                For n = -1 To 1
                    sx = CSng(j + n)
                    If sx < 0! Then sx = 0!
                    If sx > CSng(dw - 1) Then sx = CSng(dw - 1)
                    c = frmLoad.picView.Point(sx, sy)
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    If ncR < cR Then ncR = cR
                    If ncG < cG Then ncG = cG
                    If ncB < cB Then ncB = cB
                Next n
            Next m
            c = RGB(ncR, ncG, ncB)
            frmMain.picTemp.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50!
    Next i
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            ncR = &HFF: ncG = &HFF: ncB = &HFF
            For m = -1 To 1
                sy = CSng(i + m)
                If sy < 0! Then sy = 0!
                If sy > CSng(dh - 1) Then sy = CSng(dh - 1)
                For n = -1 To 1
                    sx = CSng(j + n)
                    If sx < 0! Then sx = 0!
                    If sx > CSng(dw - 1) Then sx = CSng(dw - 1)
                    c = frmMain.picTemp.Point(sx, sy)
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    If ncR > cR Then ncR = cR
                    If ncG > cG Then ncG = cG
                    If ncB > cB Then ncB = cB
                Next n
            Next m
            c = frmLoad.picView.Point(CSng(j), CSng(i))
            cR = ncR - CInt(c Mod &H100&)
            cG = ncG - CInt((c \ &H100&) Mod &H100&)
            cB = ncB - CInt((c \ &H100&) \ &H100&)
            If cR < 0 Then cR = 0
            If cG < 0 Then cG = 0
            If cB < 0 Then cB = 0
            c = RGB(cR, cG, cB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 50! + 50!
    Next i
    
    Screen.MousePointer = vbArrow
    


End Sub

Public Sub MinMaxg5()

    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim sx As Single, sy As Single
    Dim dw As Integer, dh As Integer
    Dim c As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim ncR As Integer, ncG As Integer, ncB As Integer
    Dim mcR As Integer, mcG As Integer, mcB As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    dw = frmLoad.picView.ScaleWidth
    dh = frmLoad.picView.ScaleHeight
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            mcR = 0: mcG = 0: mcB = 0
            ncR = &HFF: ncG = &HFF: ncB = &HFF
            For m = -1 To 1
                sy = CSng(i + m)
                If sy < 0! Then sy = 0!
                If sy > CSng(dh - 1) Then sy = CSng(dh - 1)
                For n = -1 To 1
                    sx = CSng(j + n)
                    If sx < 0! Then sx = 0!
                    If sx > CSng(dw - 1) Then sx = CSng(dw - 1)
                    c = frmLoad.picView.Point(sx, sy)
                    cR = CInt(c Mod &H100&)
                    cG = CInt((c \ &H100&) Mod &H100&)
                    cB = CInt((c \ &H100&) \ &H100&)
                    If ncR > cR Then ncR = cR
                    If ncG > cG Then ncG = cG
                    If ncB > cB Then ncB = cB
                    If mcR < cR Then mcR = cR
                    If mcG < cG Then mcG = cG
                    If mcB < cB Then mcB = cB
                Next n
            Next m
            cR = mcR - ncR
            cG = mcG - ncG
            cB = mcB - ncB
            c = RGB(cR, cG, cB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow
    

End Sub

Public Sub MskMtrx(mflag As Integer)
    Dim i As Integer, j As Integer, k As Integer
    Dim m As Integer, n As Integer
    Dim c As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim ncR As Integer, ncG As Integer, ncB As Integer
    Dim sx As Single, sy As Single
    Dim dw As Integer, dh As Integer
    Dim mtrx(3, 2, 2) As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    Select Case mflag
    Case HEN_1GEN
        mtrx(0, 0, 1) = -1: mtrx(0, 2, 1) = 1
        mtrx(1, 1, 0) = 1: mtrx(1, 1, 2) = -1
        mtrx(2, 2, 0) = 1: mtrx(2, 0, 2) = -1
        mtrx(3, 0, 0) = 1: mtrx(3, 2, 2) = -1
    Case HEN_2GEN
        mtrx(0, 0, 0) = -1: mtrx(0, 2, 0) = 1
        mtrx(0, 0, 1) = -1: mtrx(0, 2, 1) = 1
        mtrx(0, 0, 2) = -1: mtrx(0, 2, 2) = 1
        mtrx(1, 0, 2) = -1: mtrx(1, 1, 0) = 1
        mtrx(1, 1, 2) = -1: mtrx(1, 1, 0) = 1
        mtrx(1, 2, 2) = -1: mtrx(1, 1, 0) = 1
        mtrx(2, 0, 1) = -1: mtrx(2, 1, 0) = 1
        mtrx(2, 0, 2) = -1: mtrx(2, 2, 0) = 1
        mtrx(2, 1, 1) = -1: mtrx(2, 2, 1) = 1
        mtrx(3, 1, 2) = -1: mtrx(3, 0, 1) = 1
        mtrx(3, 2, 2) = -1: mtrx(3, 0, 0) = 1
        mtrx(3, 2, 1) = -1: mtrx(3, 1, 0) = 1
    Case HEN_SOBEL
        mtrx(0, 0, 0) = -1: mtrx(0, 2, 0) = 1
        mtrx(0, 0, 1) = -2: mtrx(0, 2, 1) = 2
        mtrx(0, 0, 2) = -1: mtrx(0, 2, 2) = 1
        mtrx(1, 0, 2) = -1: mtrx(1, 1, 0) = 1
        mtrx(1, 1, 2) = -2: mtrx(1, 1, 0) = 2
        mtrx(1, 2, 2) = -1: mtrx(1, 1, 0) = 1
        mtrx(2, 0, 1) = -1: mtrx(2, 1, 0) = 1
        mtrx(2, 0, 2) = -2: mtrx(2, 2, 0) = 2
        mtrx(2, 1, 1) = -1: mtrx(2, 2, 1) = 1
        mtrx(3, 1, 2) = -1: mtrx(3, 0, 1) = 1
        mtrx(3, 2, 2) = -2: mtrx(3, 0, 0) = 2
        mtrx(3, 2, 1) = -1: mtrx(3, 1, 0) = 1
    End Select
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    dw = frmLoad.picView.ScaleWidth
    dh = frmLoad.picView.ScaleHeight
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            ncR = 0
            ncG = 0
            ncB = 0
            For k = 0 To 3
                cR = 0
                cG = 0
                cB = 0
                For m = 0 To 2
                    sy = CSng(i + m - 1)
                    If sy < 0! Then sy = 0!
                    If sy > CSng(dh - 1) Then sy = CSng(dh - 1)
                    For n = 0 To 2
                        sx = CSng(j + n - 1)
                        If sx < 0! Then sx = 0!
                        If sx > CSng(dw - 1) Then sx = CSng(dw - 1)
                        c = frmLoad.picView.Point(sx, sy)
                        cR = cR + mtrx(k, n, m) * CInt(c Mod &H100&)
                        cG = cG + mtrx(k, n, m) * CInt((c \ &H100&) Mod &H100&)
                        cB = cB + mtrx(k, n, m) * CInt((c \ &H100&) \ &H100&)
                    Next n
                Next m
                If ncR < cR Then ncR = cR
                If ncG < cG Then ncG = cG
                If ncB < cB Then ncB = cB
            Next k
            If ncR < &H0 Then ncR = &H0
            If ncR > &HFF Then ncR = &HFF
            If ncG < &H0 Then ncG = &H0
            If ncG > &HFF Then ncG = &HFF
            If ncB < &H0 Then ncB = &H0
            If ncB > &HFF Then ncB = &HFF
            c = RGB(ncR, ncG, ncB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow
            
End Sub


Public Sub MskLaplacian(mflag As Integer)

    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim dw As Integer, dh As Integer
    Dim sx As Integer, sy As Integer
    Dim c As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim mtrx(2, 2) As Integer
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    Select Case mflag
    Case 0
        mtrx(0, 0) = 0: mtrx(1, 0) = 1: mtrx(2, 0) = 0
        mtrx(0, 1) = 1: mtrx(1, 1) = -4: mtrx(2, 1) = 1
        mtrx(0, 2) = 0: mtrx(1, 2) = 1: mtrx(2, 2) = 0
    Case 1
        mtrx(0, 0) = 1: mtrx(1, 0) = 1: mtrx(2, 0) = 1
        mtrx(0, 1) = 1: mtrx(1, 1) = -8: mtrx(2, 1) = 1
        mtrx(0, 2) = 1: mtrx(1, 2) = 1: mtrx(2, 2) = 1
    Case 2
        mtrx(0, 0) = 0: mtrx(1, 0) = 1: mtrx(2, 0) = 0
        mtrx(0, 1) = 1: mtrx(1, 1) = -5: mtrx(2, 1) = 1
        mtrx(0, 2) = 0: mtrx(1, 2) = 1: mtrx(2, 2) = 1
    Case 3
        mtrx(0, 0) = 1: mtrx(1, 0) = 1: mtrx(2, 0) = 1
        mtrx(0, 1) = 1: mtrx(1, 1) = -9: mtrx(2, 1) = 1
        mtrx(0, 2) = 1: mtrx(1, 2) = 1: mtrx(2, 2) = 1
    End Select
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    dw = frmLoad.picView.ScaleWidth
    dh = frmLoad.picView.ScaleHeight
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            cR = 0: cG = 0: cB = 0
            For m = 0 To 2
                sy = CSng(i + m - 1)
                If sy < 0! Then sy = 0!
                If sy > CSng(dh - 1) Then sy = CSng(dh - 1)
                For n = 0 To 2
                    sx = CSng(j + n - 1)
                    If sx < 0! Then sx = 0!
                    If sx > CSng(dw - 1) Then sx = CSng(dw - 1)
                    c = frmLoad.picView.Point(sx, sy)
                    cR = cR + mtrx(n, m) * CInt(c Mod &H100&)
                    cG = cG + mtrx(n, m) * CInt((c \ &H100&) Mod &H100&)
                    cB = cB + mtrx(n, m) * CInt((c \ &H100&) \ &H100&)
                Next n
            Next m
            If cR < 0 Then cR = 0
            If cR > &HFF Then cR = &HFF
            If cG < 0 Then cG = 0
            If cG > &HFF Then cG = &HFF
            If cB < 0 Then cB = 0
            If cB > &HFF Then cB = &HFF
            c = RGB(cR, cG, cB)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub


