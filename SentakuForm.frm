VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSentaku 
   BackColor       =   &H8000000C&
   Caption         =   "切り出し範囲選択"
   ClientHeight    =   3825
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   7215
   ControlBox      =   0   'False
   FillColor       =   &H00FFFFFF&
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   255
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   481
   StartUpPosition =   2  '画面の中央
   Begin MSComctlLib.StatusBar staSentaku 
      Align           =   2  '下揃え
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Top             =   3465
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   635
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   5
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   3704
            MinWidth        =   3704
            Text            =   "ポインタ-左上-右下-大きさ"
            TextSave        =   "ポインタ-左上-右下-大きさ"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2117
            MinWidth        =   2117
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2117
            MinWidth        =   2117
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2117
            MinWidth        =   2117
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2117
            MinWidth        =   2117
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "ＭＳ Ｐゴシック"
         Size            =   8.25
         Charset         =   128
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picView 
      Appearance      =   0  'ﾌﾗｯﾄ
      AutoRedraw      =   -1  'True
      BackColor       =   &H8000000C&
      ForeColor       =   &H80000008&
      Height          =   3000
      Left            =   0
      ScaleHeight     =   198
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   247
      TabIndex        =   0
      Top             =   0
      Width           =   3735
      Begin VB.Shape shpSentaku 
         BorderColor     =   &H000000FF&
         BorderStyle     =   3  '点線
         DrawMode        =   5  'Not Copy Pen
         Height          =   1350
         Left            =   0
         Top             =   0
         Width           =   1635
      End
   End
   Begin VB.Menu mnuSentakuOk 
      Caption         =   "OK"
   End
   Begin VB.Menu mnuSentakuCancel 
      Caption         =   "Cancel"
   End
End
Attribute VB_Name = "frmSentaku"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim vX As Single, vY As Single

Dim pX As Single, pY As Single

Dim sdx As Single, sdy As Single


Private Sub Form_Load()

    Dim sx As Single, sy As Single
    
    sdx = 10!
    sdy = 10!

    picView.Cls
    picView.Width = frmLoad.picView.Width + sdx * 2!
    picView.Height = frmLoad.picView.Height + sdy * 2!
    picView.PaintPicture frmLoad.picView.Image, sdx, sdy
        
    If frmMain.optUnit(0).Value Then
        vX = CSng(Val(frmMain.txtLeft.Text))
        vY = CSng(Val(frmMain.txtTop.Text))
        shpSentaku.Left = vX + sdx
        shpSentaku.Top = vY + sdy
        If frmMain.optMigishita(0).Value Then
            sx = CSng(Val(frmMain.txtRight.Text)) - vX + 1!
            sy = CSng(Val(frmMain.txtBottom.Text)) - vY + 1!
            staSentaku.Panels(4).Text = _
                    frmMain.txtRight.Text & "," _
                    & frmMain.txtBottom.Text
            staSentaku.Panels(5).Text = _
                    Format$(sx) & "," & Format$(sy)
        Else
            sx = CSng(Val(frmMain.txtRight.Text))
            sy = CSng(Val(frmMain.txtBottom.Text))
            staSentaku.Panels(5).Text = _
                    frmMain.txtRight.Text & "," _
                    & frmMain.txtBottom.Text
            staSentaku.Panels(4).Text = _
                    Format$(sx + vX - 1!) _
                    & "," & Format$(sy + vY - 1!)
        End If
        shpSentaku.Width = sx
        shpSentaku.Height = sy
    Else
        sx = CSng(Val(frmMain.txtLeft.Text))
        sy = CSng(Val(frmMain.txtTop.Text))
        vX = frmLoad.picView.ScaleWidth * sx / 100!
        vY = frmLoad.picView.ScaleHeight * sy / 100!
        shpSentaku.Left = vX + sdx
        shpSentaku.Top = vY + sdy
        If frmMain.optMigishita(0).Value Then
            sx = CSng(Val(frmMain.txtRight.Text))
            sy = CSng(Val(frmMain.txtBottom.Text))
            sx = frmLoad.picView.ScaleWidth * sx / 100!
            sy = frmLoad.picView.ScaleHeight * sy / 100!
            sx = sx - vX + 1!
            sy = sy - vY + 1!
            staSentaku.Panels(4).Text = _
                    frmMain.txtRight.Text & "," _
                    & frmMain.txtBottom.Text
            staSentaku.Panels(5).Text = _
                    Format$(sx / frmLoad.picView.ScaleWidth * 100!, "0.00") _
                    & "," & Format$(sy / frmLoad.picView.ScaleHeight * 100!, "0.00")
        Else
            sx = CSng(Val(frmMain.txtRight.Text))
            sy = CSng(Val(frmMain.txtBottom.Text))
            sx = frmLoad.picView.ScaleWidth * sx / 100!
            sy = frmLoad.picView.ScaleHeight * sy / 100!
            staSentaku.Panels(5).Text = _
                    frmMain.txtRight.Text & "," _
                    & frmMain.txtBottom.Text
            staSentaku.Panels(4).Text = _
                    Format$(sx / frmLoad.picView.ScaleWidth * 100!, "0.00") _
                    & "," & Format$(sy / frmLoad.picView.ScaleHeight * 100!, "0.00")
        End If
        shpSentaku.Width = sx
        shpSentaku.Height = sy
    End If
    
    staSentaku.Panels(3).Text = _
            frmMain.txtLeft.Text & "," & frmMain.txtTop.Text



End Sub


Private Sub mnuSentakuCancel_Click()

    Unload Me

End Sub

Private Sub mnuSentakuOk_Click()

    Dim sx As Single, sy As Single

    If frmMain.optUnit(0).Value Then
        frmMain.txtLeft = Format$(vX)
        frmMain.txtTop = Format$(vY)
        If frmMain.optMigishita(0).Value Then
            sx = vX + shpSentaku.Width - 1!
            sy = vY + shpSentaku.Height - 1!
            frmMain.txtRight = Format$(sx)
            frmMain.txtBottom = Format$(sy)
        Else
            sx = shpSentaku.Width
            sy = shpSentaku.Height
            frmMain.txtRight = Format$(sx)
            frmMain.txtBottom = Format$(sy)
        End If
    Else
        sx = vX / frmLoad.picView.ScaleWidth * 100!
        sy = vY / frmLoad.picView.ScaleHeight * 100!
        frmMain.txtLeft = Format$(sx, "0.00")
        frmMain.txtTop = Format$(sy, "0.00")
        If frmMain.optMigishita(0).Value Then
            sx = sx + shpSentaku.Width - 1!
            sy = sy + shpSentaku.Height - 1!
            sx = sx / frmLoad.picView.ScaleWidth * 100!
            sy = sy / frmLoad.picView.ScaleHeight * 100!
            frmMain.txtRight = Format$(sx, "0.00")
            frmMain.txtBottom = Format$(sy, "0.00")
        Else
            sx = shpSentaku.Width
            sy = shpSentaku.Height
            sx = sx / frmLoad.picView.ScaleWidth * 100!
            sy = sy / frmLoad.picView.ScaleHeight * 100!
            frmMain.txtRight = Format$(sx, "0.00")
            frmMain.txtBottom = Format$(sy, "0.00")
        End If
        
    End If
    
    Unload Me

End Sub

Private Sub picView_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

    Dim sx As Single, sy As Single

    If Button = vbRightButton Then
        pX = X
        pY = Y
    ElseIf Button = vbLeftButton Then
        vX = X - sdx
        vY = Y - sdy
        If vX < 0! Or vX > frmLoad.picView.ScaleWidth Then
            vX = 0!
        End If
        If vY < 0! Or vY > frmLoad.picView.ScaleHeight Then
            vY = 0!
        End If
        If frmMain.optUnit(0).Value Then
            staSentaku.Panels(3).Text = _
                Format$(vX) & ", " & Format$(vY)
        Else
            sx = vX / frmLoad.picView.ScaleWidth * 100!
            sy = vY / frmLoad.picView.ScaleHeight * 100!
            staSentaku.Panels(3).Text = _
                Format$(sx, "0.00") & ", " & Format$(sy, "0.00")
        End If
        shpSentaku.Left = vX + sdx
        shpSentaku.Top = vY + sdy
        shpSentaku.Width = 1!
        shpSentaku.Height = 1!
        
    End If

End Sub

Private Sub picView_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    Dim sx As Single, sy As Single

    If frmMain.optUnit(0).Value Then
        sx = X - sdx
        sy = Y - sdy
        staSentaku.Panels(2).Text = _
                Format$(sx) & "," & Format$(sy)
    Else
        sx = (X - sdx) / frmLoad.picView.ScaleWidth * 100!
        sy = (Y - sdy) / frmLoad.picView.ScaleHeight * 100!
        staSentaku.Panels(2).Text = _
                Format$(sx, "0.00") & "," & Format$(sy, "0.00")
    End If
    
    If Button = vbRightButton Then
        
        picView.Left = picView.Left + (X - pX)
        picView.Top = picView.Top + (Y - pY)
    ElseIf Button = vbLeftButton Then
        sx = (X - sdx) - vX + 1!
        sy = (Y - sdy) - vY + 1!
        If sx < 1! Then sx = 1!
        If sy < 1! Then sy = 1!
        If (X - sdx) > frmLoad.picView.ScaleWidth Then _
                sx = frmLoad.picView.ScaleWidth - vX
        If (Y - sdy) > frmLoad.picView.ScaleHeight Then _
                sy = frmLoad.picView.ScaleHeight - vY
        shpSentaku.Width = sx
        shpSentaku.Height = sy
        If frmMain.optUnit(0).Value Then
            staSentaku.Panels(4).Text = Format$(vX + sx - 1!) _
                    & "," & Format$(vY + sy - 1!)
            staSentaku.Panels(5).Text = Format$(sx) _
                    & "," & Format$(sy)
        Else
            sx = sx / frmLoad.picView.ScaleWidth * 100!
            sy = sy / frmLoad.picView.ScaleHeight * 100!
            staSentaku.Panels(5).Text = Format$(sx, "0.00") _
                    & "," & Format$(sy, "0.00")
            staSentaku.Panels(4).Text = _
                    Format$(100! - sx, "0.00") _
                    & "," & Format$(100! - sy, "0.00")
        End If
    End If
    
End Sub

