VERSION 5.00
Begin VB.Form frmFukuGou 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  '固定(実線)
   Caption         =   "複数画像合成"
   ClientHeight    =   4440
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6585
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   296
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   439
   StartUpPosition =   2  '画面の中央
   Begin VB.ComboBox cboFG 
      Height          =   300
      Left            =   150
      Style           =   2  'ﾄﾞﾛｯﾌﾟﾀﾞｳﾝ ﾘｽﾄ
      TabIndex        =   9
      Top             =   4050
      Width           =   2865
   End
   Begin VB.CommandButton cmdFG 
      Caption         =   "実行"
      Height          =   300
      Left            =   3150
      TabIndex        =   8
      Top             =   4050
      Width           =   1500
   End
   Begin VB.ListBox lstPath 
      Height          =   420
      Left            =   195
      TabIndex        =   7
      Top             =   4560
      Visible         =   0   'False
      Width           =   1800
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "削除"
      Height          =   300
      Left            =   5625
      TabIndex        =   6
      Top             =   3600
      Width           =   900
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "追加"
      Height          =   300
      Left            =   4575
      TabIndex        =   5
      Top             =   3600
      Width           =   900
   End
   Begin VB.ListBox lstFG 
      Height          =   3480
      Left            =   4575
      MultiSelect     =   2  '拡張
      TabIndex        =   4
      Top             =   75
      Width           =   1950
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "閉じる"
      Height          =   300
      Left            =   4875
      TabIndex        =   3
      Top             =   4050
      Width           =   1500
   End
   Begin VB.FileListBox filFG 
      Height          =   3870
      Left            =   2550
      MultiSelect     =   2  '拡張
      Pattern         =   "*.bmp;*.jpg;*.jpeg;*.gif;*.png"
      TabIndex        =   2
      Top             =   75
      Width           =   1950
   End
   Begin VB.DirListBox dirFG 
      Height          =   3450
      Left            =   75
      TabIndex        =   1
      Top             =   450
      Width           =   2400
   End
   Begin VB.DriveListBox drvFG 
      Height          =   300
      Left            =   75
      TabIndex        =   0
      Top             =   75
      Width           =   2400
   End
End
Attribute VB_Name = "frmFukuGou"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAdd_Click()
    Dim i As Integer, j As Integer
    Dim pp As Integer
    Dim fn As String, pth As String
    
    pth = filFG.Path
    
    If StrComp(Right$(pth, 1), "\") <> 0 Then
        pth = pth & "\"
    End If
    
    For i = 0 To filFG.ListCount - 1
        If filFG.Selected(i) Then
            fn = filFG.List(i)
            pp = 0
            For j = 0 To lstFG.ListCount - 1
                If StrComp(fn, lstFG.List(j)) = 0 Then
                    pp = 1
                    Exit For
                End If
            Next j
            If pp = 0 Then
                lstFG.AddItem fn
                lstPath.AddItem pth & fn
            End If
        End If
    Next i
    
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdDelete_Click()
    Dim i As Integer
    For i = (lstFG.ListCount - 1) To 0 Step -1
        If lstFG.Selected(i) Then
            lstFG.RemoveItem i
            lstPath.RemoveItem i
        End If
    Next i
End Sub

Private Sub cmdFG_Click()
    
    Dim i As Integer, j As Integer, k As Integer
    Dim dw As Integer, dh As Integer
    Dim c As Long, lcC As Long
    Dim dcC As Single, cC As Integer
    Dim lFG As Integer, pp As Integer
    
    lFG = lstFG.ListCount
    
    If lFG < 2 Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    frmFukuGou.Caption = "複数画像合成 ...前処理中"
    
    frmMain.picLoad.Picture = frmLoad.picView.Image
    
    frmLoad.picView.Cls
    frmLoad.picView.Picture = LoadPicture(lstPath.List(0))
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    ReDim lPicF(dw - 1, dh - 1) As Long
    
    If cboFG.ListIndex = 2 Then
        For i = 0 To dh - 1
            For j = 0 To dw - 1
                lPicF(j, i) = 255&
            Next j
            frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 5!
        Next i
    Else
        For i = 0 To dh - 1
            For j = 0 To dw - 1
                lPicF(j, i) = 0&
            Next j
            frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 5!
        Next i
    End If
    
    pp = 0
    
    For k = 0 To lFG - 1
    
        frmFukuGou.Caption = "複数画像合成 ...計算中(" _
                    & Format$(k + 1) & "/" & Format$(lFG) & ")"
        
        frmLoad.picView.Cls
        frmLoad.picView.Picture = LoadPicture(lstPath.List(k))
        
        If (dw = CInt(frmLoad.picView.ScaleWidth)) _
                And (dh = CInt(frmLoad.picView.ScaleHeight)) Then
        
        
            For i = 0 To dh - 1
                For j = 0 To dw - 1
                    
                    c = frmLoad.picView.Point(CSng(j), CSng(i))
                    lcC = CLng(Kido(c))
                       
                    Select Case cboFG.ListIndex
                    Case 0
                        lPicF(j, i) = lPicF(j, i) + lcC
                    Case 1
                        If lPicF(j, i) < lcC Then
                            lPicF(j, i) = lcC
                        End If
                    Case 2
                        If lPicF(j, i) > lcC Then
                            lPicF(j, i) = lcC
                        End If
                    End Select
                Next j
                frmMain.prgShinko.Value = _
                            (CSng(k) * CSng(dh) + CSng(i + 1)) _
                                    / CSng(dh) / CSng(lFG) * 80! + 5!
            Next i
        
        Else
            pp = pp + 1
            frmMain.prgShinko.Value = CSng(k + 1) / CSng(lFG) * 80! + 5!
        End If
        
        
    Next k
    
    frmFukuGou.Caption = "複数画像合成 ...描写中"
    
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            If cboFG.ListIndex = 0 Then
                dcC = CSng(lPicF(j, i)) / CSng(lFG - pp)
                cC = CInt(dcC)
            Else
                cC = CInt(lPicF(j, i))
            End If
            c = RGB(cC, cC, cC)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 15! + 85!
    Next i
    
    frmLoad.picView.Picture = frmMain.picLoad.Image
    
    frmFukuGou.Caption = "複数画像合成"
    
    Screen.MousePointer = vbArrow
    
End Sub

Private Sub dirFG_Change()
    filFG.Path = dirFG.Path
End Sub

Private Sub drvFG_Change()
    dirFG.Path = drvFG.Drive
End Sub

Private Sub Form_Load()
    
    cboFG.AddItem "平均"
    cboFG.AddItem "最大"
    cboFG.AddItem "最小"
    cboFG.ListIndex = 0
    
End Sub
