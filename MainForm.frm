VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMain 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "画像処理"
   ClientHeight    =   5760
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   5940
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   384
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   396
   StartUpPosition =   2  '画面の中央
   Begin VB.CommandButton cmd2chiWnd2 
      Caption         =   "２値処理2"
      Height          =   375
      Left            =   4800
      TabIndex        =   199
      Top             =   2850
      Width           =   1050
   End
   Begin VB.CommandButton cmdFukuGou 
      Caption         =   "複数合成"
      Height          =   375
      Left            =   4800
      TabIndex        =   197
      Top             =   3300
      Width           =   1050
   End
   Begin VB.PictureBox picTemp2 
      AutoRedraw      =   -1  'True
      Height          =   405
      Left            =   5400
      ScaleHeight     =   23
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   29
      TabIndex        =   188
      Top             =   1395
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CommandButton cmd2chiWnd 
      Caption         =   "２値処理1"
      Height          =   375
      Left            =   4800
      TabIndex        =   187
      Top             =   2400
      Width           =   1050
   End
   Begin VB.CommandButton cmdFukusu 
      Caption         =   "複数同時"
      Height          =   375
      Left            =   4800
      TabIndex        =   184
      Top             =   3750
      Width           =   1050
   End
   Begin VB.PictureBox picLoad 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   465
      Left            =   5400
      ScaleHeight     =   27
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   31
      TabIndex        =   121
      Top             =   720
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.CommandButton cmdCopy 
      Caption         =   "移す"
      Height          =   375
      Left            =   4800
      TabIndex        =   2
      Top             =   4650
      Width           =   1050
   End
   Begin MSComDlg.CommonDialog dlgSave 
      Left            =   4845
      Top             =   750
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "BMP"
      DialogTitle     =   "保存"
      Filter          =   "BITMAP|*.bmp"
      Flags           =   6
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "保存"
      Height          =   375
      Left            =   4800
      TabIndex        =   3
      Top             =   5100
      Width           =   1050
   End
   Begin VB.PictureBox picTemp 
      AutoRedraw      =   -1  'True
      Height          =   405
      Left            =   5400
      ScaleHeight     =   23
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   29
      TabIndex        =   4
      Top             =   165
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CommandButton cmdOpen 
      Caption         =   "開く"
      Height          =   375
      Left            =   4800
      TabIndex        =   1
      Top             =   4200
      Width           =   1050
   End
   Begin MSComDlg.CommonDialog dlgOpen 
      Left            =   4845
      Top             =   165
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "開く"
      Filter          =   "BITMAP|*.bmp|JPEG|*.jpg;*.jpeg|GIF|*.gif|PNG|*.png"
      Flags           =   4
   End
   Begin MSComctlLib.ProgressBar prgShinko 
      Height          =   150
      Left            =   75
      TabIndex        =   0
      Top             =   5550
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   265
      _Version        =   393216
      Appearance      =   1
   End
   Begin MSComDlg.CommonDialog dlgOpen2 
      Left            =   4845
      Top             =   1350
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "開く"
      Filter          =   "BITMAP|*.bmp|JPEG|*.jpg;*.jpeg|GIF|*.gif|PNG|*.png"
      Flags           =   4
   End
   Begin VB.PictureBox picContainer 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'なし
      Height          =   4950
      Index           =   5
      Left            =   150
      ScaleHeight     =   330
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   300
      TabIndex        =   202
      Top             =   150
      Visible         =   0   'False
      Width           =   4500
      Begin VB.Frame fraNDCut 
         Caption         =   "濃度処理"
         Height          =   1095
         Left            =   0
         TabIndex        =   214
         Top             =   1440
         Width           =   4500
         Begin VB.CommandButton cmdNDPress 
            Caption         =   "濃度詰め"
            Height          =   330
            Left            =   2760
            TabIndex        =   219
            Top             =   645
            Width           =   1665
         End
         Begin VB.OptionButton optNDCut 
            Caption         =   "百分率"
            Height          =   315
            Index           =   1
            Left            =   1830
            TabIndex        =   218
            Top             =   210
            Width           =   840
         End
         Begin VB.OptionButton optNDCut 
            Caption         =   "個数"
            Height          =   315
            Index           =   0
            Left            =   1110
            TabIndex        =   217
            Top             =   195
            Value           =   -1  'True
            Width           =   735
         End
         Begin VB.TextBox txtNDCut 
            Height          =   315
            IMEMode         =   3  'ｵﾌ固定
            Left            =   75
            MaxLength       =   9
            TabIndex        =   216
            Text            =   "100"
            Top             =   195
            Width           =   945
         End
         Begin VB.CommandButton cmdNDCut 
            Caption         =   "小頻度濃度除去"
            Height          =   330
            Left            =   2760
            TabIndex        =   215
            Top             =   210
            Width           =   1665
         End
      End
      Begin VB.Frame fraHist2 
         Caption         =   "ヒストグラム描写"
         Height          =   1425
         Left            =   0
         TabIndex        =   203
         Top             =   0
         Width           =   4500
         Begin VB.CheckBox chkHist2A 
            Caption         =   "20%"
            Enabled         =   0   'False
            Height          =   255
            Left            =   3735
            TabIndex        =   213
            Top             =   525
            Width           =   600
         End
         Begin VB.CheckBox chkHist2 
            Caption         =   "10%"
            Height          =   240
            Left            =   2940
            TabIndex        =   212
            Top             =   555
            Width           =   585
         End
         Begin VB.CommandButton cmdHist2 
            Caption         =   "実行"
            Height          =   300
            Left            =   3045
            TabIndex        =   211
            Top             =   930
            Width           =   1320
         End
         Begin VB.OptionButton optHist2 
            Caption         =   "累積ヒストグラム"
            Height          =   300
            Index           =   1
            Left            =   1875
            TabIndex        =   205
            Top             =   210
            Width           =   1620
         End
         Begin VB.OptionButton optHist2 
            Caption         =   "通常のヒストグラム"
            Height          =   300
            Index           =   0
            Left            =   60
            TabIndex        =   204
            Top             =   210
            Value           =   -1  'True
            Width           =   1860
         End
         Begin VB.CheckBox chkHist2B 
            Height          =   180
            Left            =   105
            TabIndex        =   221
            Top             =   1020
            Width           =   315
         End
         Begin VB.Frame fraHist2C 
            BorderStyle     =   0  'なし
            Caption         =   "Frame1"
            Height          =   300
            Left            =   75
            TabIndex        =   206
            Top             =   585
            Width           =   2730
            Begin VB.OptionButton optHist2C 
               Caption         =   "輝度"
               Height          =   300
               Index           =   3
               Left            =   1875
               TabIndex        =   210
               Top             =   0
               Value           =   -1  'True
               Width           =   735
            End
            Begin VB.OptionButton optHist2C 
               Caption         =   "青"
               Height          =   300
               Index           =   2
               Left            =   1230
               TabIndex        =   209
               Top             =   0
               Width           =   540
            End
            Begin VB.OptionButton optHist2C 
               Caption         =   "緑"
               Height          =   300
               Index           =   1
               Left            =   600
               TabIndex        =   208
               Top             =   0
               Width           =   540
            End
            Begin VB.OptionButton optHist2C 
               Caption         =   "赤"
               Height          =   300
               Index           =   0
               Left            =   0
               TabIndex        =   207
               Top             =   0
               Width           =   540
            End
         End
         Begin MSComctlLib.Slider sldHist2 
            Height          =   480
            Left            =   390
            TabIndex        =   222
            Top             =   900
            Width           =   2580
            _ExtentX        =   4551
            _ExtentY        =   847
            _Version        =   393216
            LargeChange     =   20
            Min             =   2
            Max             =   200
            SelStart        =   2
            TickFrequency   =   20
            Value           =   2
         End
      End
   End
   Begin VB.PictureBox picContainer 
      BorderStyle     =   0  'なし
      Height          =   4950
      Index           =   4
      Left            =   150
      ScaleHeight     =   330
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   300
      TabIndex        =   145
      Top             =   150
      Visible         =   0   'False
      Width           =   4500
      Begin VB.Frame fraNoudoCA 
         Caption         =   "濃度調整"
         Height          =   975
         Left            =   0
         TabIndex        =   200
         Top             =   3600
         Width           =   4500
         Begin VB.CommandButton cmdNoudoCA2 
            Caption         =   "中央値？"
            Height          =   300
            Left            =   1890
            TabIndex        =   220
            Top             =   210
            Width           =   1500
         End
         Begin VB.CommandButton cmdNoudoCA1 
            Caption         =   "大津"
            Height          =   300
            Left            =   90
            TabIndex        =   201
            Top             =   210
            Width           =   1500
         End
      End
      Begin VB.Frame fraShikiichi 
         Caption         =   "しきい値選定"
         Height          =   600
         Left            =   0
         TabIndex        =   182
         Top             =   3000
         Width           =   4500
         Begin VB.CommandButton cmd2chiOtsu 
            Caption         =   "大津閾値による２値化"
            Height          =   300
            Left            =   2100
            TabIndex        =   186
            Top             =   210
            Width           =   2220
         End
         Begin VB.CommandButton cmdOtsu 
            Caption         =   "大津の方法"
            Height          =   300
            Left            =   120
            TabIndex        =   183
            Top             =   210
            Width           =   1260
         End
      End
      Begin VB.Frame fraNoise 
         Caption         =   "ノイズ付加"
         Height          =   600
         Left            =   0
         TabIndex        =   179
         Top             =   2400
         Width           =   4500
         Begin VB.CommandButton cmdNoise 
            Caption         =   "実行（モノクロ）"
            Height          =   285
            Left            =   1980
            TabIndex        =   181
            Top             =   210
            Width           =   1290
         End
         Begin VB.TextBox txtNoise 
            Height          =   270
            IMEMode         =   3  'ｵﾌ固定
            Left            =   285
            MaxLength       =   4
            TabIndex        =   180
            Text            =   "100"
            Top             =   210
            Width           =   630
         End
      End
      Begin VB.Frame fraNodoSeikika 
         Caption         =   "濃度正規化"
         Height          =   600
         Left            =   0
         TabIndex        =   177
         Top             =   1800
         Width           =   4500
         Begin VB.CommandButton cmdIroiro1 
            Caption         =   "濃度正規化（モノクロ）"
            Height          =   300
            Left            =   105
            TabIndex        =   178
            Top             =   210
            Width           =   1920
         End
      End
      Begin VB.Frame fraHist 
         Caption         =   "ヒストグラム"
         Height          =   600
         Left            =   0
         TabIndex        =   172
         Top             =   600
         Width           =   4500
         Begin VB.CommandButton cmdHist 
            Caption         =   "赤"
            Height          =   300
            Index           =   0
            Left            =   105
            TabIndex        =   176
            Top             =   210
            Width           =   990
         End
         Begin VB.CommandButton cmdHist 
            Caption         =   "緑"
            Height          =   300
            Index           =   1
            Left            =   1200
            TabIndex        =   175
            Top             =   210
            Width           =   990
         End
         Begin VB.CommandButton cmdHist 
            Caption         =   "青"
            Height          =   300
            Index           =   2
            Left            =   2310
            TabIndex        =   174
            Top             =   210
            Width           =   990
         End
         Begin VB.CommandButton cmdHist 
            Caption         =   "輝度"
            Height          =   300
            Index           =   3
            Left            =   3420
            TabIndex        =   173
            Top             =   210
            Width           =   990
         End
      End
      Begin VB.Frame fraRuiseki 
         Caption         =   "累積ヒストグラム"
         Height          =   600
         Left            =   0
         TabIndex        =   167
         Top             =   1200
         Width           =   4500
         Begin VB.CommandButton cmdRuiseki 
            Caption         =   "輝度"
            Height          =   300
            Index           =   3
            Left            =   3420
            TabIndex        =   171
            Top             =   210
            Width           =   990
         End
         Begin VB.CommandButton cmdRuiseki 
            Caption         =   "青"
            Height          =   300
            Index           =   2
            Left            =   2310
            TabIndex        =   170
            Top             =   210
            Width           =   990
         End
         Begin VB.CommandButton cmdRuiseki 
            Caption         =   "緑"
            Height          =   300
            Index           =   1
            Left            =   1200
            TabIndex        =   169
            Top             =   210
            Width           =   990
         End
         Begin VB.CommandButton cmdRuiseki 
            Caption         =   "赤"
            Height          =   300
            Index           =   0
            Left            =   105
            TabIndex        =   168
            Top             =   210
            Width           =   990
         End
      End
      Begin VB.Frame fraRyoshika 
         Caption         =   "量子化"
         Height          =   600
         Left            =   0
         TabIndex        =   163
         Top             =   0
         Width           =   4500
         Begin VB.CommandButton cmdRyoshika 
            Caption         =   "実行（モノクロ）"
            Height          =   300
            Left            =   2535
            TabIndex        =   165
            Top             =   210
            Width           =   1485
         End
         Begin VB.TextBox txtRyoshika 
            Height          =   285
            IMEMode         =   3  'ｵﾌ固定
            Left            =   990
            MaxLength       =   3
            TabIndex        =   164
            Text            =   "16"
            Top             =   180
            Width           =   570
         End
         Begin VB.Label lblRyoshika 
            AutoSize        =   -1  'True
            Caption         =   "階調"
            Height          =   180
            Left            =   1620
            TabIndex        =   166
            Top             =   240
            Width           =   360
         End
      End
   End
   Begin VB.PictureBox picContainer 
      BorderStyle     =   0  'なし
      Height          =   4950
      Index           =   2
      Left            =   150
      ScaleHeight     =   330
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   300
      TabIndex        =   17
      Top             =   150
      Visible         =   0   'False
      Width           =   4500
      Begin VB.Frame fraHeikatsu 
         Caption         =   "雑音除去・平滑化フィルタリング"
         Height          =   1425
         Left            =   0
         TabIndex        =   20
         Top             =   0
         Width           =   4500
         Begin MSComctlLib.Slider sldHeikatsu 
            Height          =   240
            Left            =   1215
            TabIndex        =   146
            Top             =   1050
            Width           =   2010
            _ExtentX        =   3545
            _ExtentY        =   423
            _Version        =   393216
            LargeChange     =   2
            SmallChange     =   2
            Min             =   3
            Max             =   9
            SelStart        =   3
            TickFrequency   =   2
            Value           =   3
         End
         Begin VB.CommandButton cmdMinMax 
            Caption         =   "MinMax法（MaxMin）"
            Height          =   360
            Index           =   1
            Left            =   2265
            TabIndex        =   139
            Top             =   630
            Width           =   2130
         End
         Begin VB.CommandButton cmdMinMax 
            Caption         =   "MinMax法（MinMax）"
            Height          =   360
            Index           =   0
            Left            =   105
            TabIndex        =   23
            Top             =   630
            Width           =   2130
         End
         Begin VB.CommandButton cmdMedian 
            Caption         =   "メディアン法"
            Height          =   360
            Left            =   2265
            TabIndex        =   22
            Top             =   240
            Width           =   2130
         End
         Begin VB.CommandButton cmdHeikin 
            Caption         =   "平均化法"
            Height          =   360
            Left            =   105
            TabIndex        =   21
            Top             =   240
            Width           =   2130
         End
         Begin VB.Label lblHeikatsu2 
            AutoSize        =   -1  'True
            Caption         =   "マスクサイズ"
            Height          =   180
            Left            =   105
            TabIndex        =   148
            Top             =   1050
            Width           =   990
         End
         Begin VB.Label lblHeikatsu 
            AutoSize        =   -1  'True
            Caption         =   "3x3"
            Height          =   180
            Left            =   3345
            TabIndex        =   147
            Top             =   1050
            Width           =   270
         End
      End
      Begin VB.Frame fraHenFuchi 
         Caption         =   "画像強調・辺縁抽出フィルタリング"
         Height          =   3075
         Left            =   0
         TabIndex        =   19
         Top             =   1500
         Width           =   4500
         Begin VB.CommandButton cmdMinMaxg4 
            Caption         =   "MinMax系 g4"
            Height          =   360
            Left            =   105
            TabIndex        =   144
            Top             =   2595
            Width           =   2130
         End
         Begin VB.CommandButton cmdMinMaxg2 
            Caption         =   "MinMax系 g2"
            Height          =   360
            Left            =   105
            TabIndex        =   143
            Top             =   2205
            Width           =   2130
         End
         Begin VB.CommandButton cmdMinMaxg5 
            Caption         =   "MinMax系 g5"
            Height          =   360
            Left            =   2265
            TabIndex        =   142
            Top             =   2595
            Width           =   2130
         End
         Begin VB.CommandButton cmdMinMaxg3 
            Caption         =   "MinMax系 g3"
            Height          =   360
            Left            =   2265
            TabIndex        =   141
            Top             =   2205
            Width           =   2130
         End
         Begin VB.CommandButton cmdMinMaxg1 
            Caption         =   "MinMax系 g1"
            Height          =   360
            Left            =   2265
            TabIndex        =   140
            Top             =   1815
            Width           =   2130
         End
         Begin VB.CommandButton cmdLaplacian 
            Caption         =   "ラプラシアン（８近傍エッジ強調）"
            Height          =   360
            Index           =   3
            Left            =   105
            TabIndex        =   138
            Top             =   1425
            Width           =   4290
         End
         Begin VB.CommandButton cmdLaplacian 
            Caption         =   "ラプラシアン（４近傍エッジ強調）"
            Height          =   360
            Index           =   2
            Left            =   105
            TabIndex        =   137
            Top             =   1035
            Width           =   4290
         End
         Begin VB.CommandButton cmdLaplacian 
            Caption         =   "ラプラシアン（８近傍差分）"
            Height          =   360
            Index           =   1
            Left            =   2265
            TabIndex        =   136
            Top             =   645
            Width           =   2130
         End
         Begin VB.CommandButton cmdLaplacian 
            Caption         =   "ラプラシアン（４近傍差分）"
            Height          =   360
            Index           =   0
            Left            =   105
            TabIndex        =   135
            Top             =   645
            Width           =   2130
         End
         Begin VB.CommandButton cmdMskMtrx 
            Caption         =   "ソーベル微分"
            Height          =   360
            Index           =   2
            Left            =   105
            TabIndex        =   134
            Top             =   1815
            Width           =   2130
         End
         Begin VB.CommandButton cmdMskMtrx 
            Caption         =   "一次微分（二次元マスク）"
            Height          =   360
            Index           =   1
            Left            =   2265
            TabIndex        =   133
            Top             =   255
            Width           =   2130
         End
         Begin VB.CommandButton cmdMskMtrx 
            Caption         =   "一次微分（一次元マスク）"
            Height          =   360
            Index           =   0
            Left            =   105
            TabIndex        =   132
            Top             =   255
            Width           =   2130
         End
      End
   End
   Begin VB.PictureBox picContainer 
      BorderStyle     =   0  'なし
      Height          =   4950
      Index           =   0
      Left            =   150
      ScaleHeight     =   330
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   300
      TabIndex        =   6
      Top             =   150
      Width           =   4500
      Begin VB.Frame fraCut 
         Caption         =   "切り抜き"
         Height          =   1275
         Left            =   0
         TabIndex        =   45
         Top             =   3150
         Width           =   4500
         Begin VB.TextBox txtLeft 
            Alignment       =   1  '右揃え
            Height          =   300
            IMEMode         =   3  'ｵﾌ固定
            Left            =   600
            MaxLength       =   7
            TabIndex        =   47
            Text            =   "0.00"
            Top             =   240
            Width           =   720
         End
         Begin VB.TextBox txtTop 
            Alignment       =   1  '右揃え
            Height          =   300
            IMEMode         =   3  'ｵﾌ固定
            Left            =   600
            MaxLength       =   7
            TabIndex        =   48
            Text            =   "0.00"
            Top             =   600
            Width           =   720
         End
         Begin VB.TextBox txtRight 
            Alignment       =   1  '右揃え
            Height          =   300
            IMEMode         =   3  'ｵﾌ固定
            Left            =   1995
            MaxLength       =   7
            TabIndex        =   49
            Text            =   "100.00"
            Top             =   240
            Width           =   720
         End
         Begin VB.TextBox txtBottom 
            Alignment       =   1  '右揃え
            Height          =   300
            IMEMode         =   3  'ｵﾌ固定
            Left            =   1995
            MaxLength       =   7
            TabIndex        =   50
            Text            =   "100.00"
            Top             =   600
            Width           =   720
         End
         Begin VB.OptionButton optMigishita 
            Caption         =   "座標"
            Height          =   240
            Index           =   0
            Left            =   2865
            TabIndex        =   53
            Top             =   585
            Width           =   720
         End
         Begin VB.OptionButton optMigishita 
            Caption         =   "幅"
            Height          =   240
            Index           =   1
            Left            =   3630
            TabIndex        =   54
            Top             =   585
            Value           =   -1  'True
            Width           =   720
         End
         Begin VB.CommandButton cmdCutting 
            Caption         =   "実行"
            Height          =   300
            Left            =   3645
            TabIndex        =   56
            Top             =   900
            Width           =   750
         End
         Begin VB.Frame fraUnit 
            BorderStyle     =   0  'なし
            Caption         =   "単位"
            Height          =   270
            Left            =   2865
            TabIndex        =   46
            Top             =   225
            Width           =   1485
            Begin VB.OptionButton optUnit 
               Caption         =   "Pixel"
               Height          =   240
               Index           =   0
               Left            =   0
               TabIndex        =   51
               Top             =   0
               Width           =   720
            End
            Begin VB.OptionButton optUnit 
               Caption         =   "％"
               Height          =   240
               Index           =   1
               Left            =   765
               TabIndex        =   52
               Top             =   0
               Value           =   -1  'True
               Width           =   720
            End
         End
         Begin VB.CommandButton cmdSentaku 
            Caption         =   "視覚的"
            Height          =   300
            Left            =   2820
            TabIndex        =   55
            Top             =   900
            Width           =   750
         End
         Begin VB.Label lblLeft 
            Caption         =   "左端"
            Height          =   210
            Left            =   120
            TabIndex        =   60
            Top             =   270
            Width           =   450
         End
         Begin VB.Label lblTop 
            Caption         =   "上端"
            Height          =   210
            Left            =   120
            TabIndex        =   59
            Top             =   630
            Width           =   450
         End
         Begin VB.Label lblRight 
            Caption         =   "幅"
            Height          =   210
            Left            =   1500
            TabIndex        =   58
            Top             =   270
            Width           =   450
         End
         Begin VB.Label lblBottom 
            Caption         =   "高さ"
            Height          =   210
            Left            =   1500
            TabIndex        =   57
            Top             =   630
            Width           =   450
         End
      End
      Begin VB.Frame fraNoudo 
         Caption         =   "濃度値調整"
         Height          =   1800
         Left            =   15
         TabIndex        =   33
         Top             =   1275
         Width           =   4500
         Begin VB.CommandButton cmdAkReset 
            Caption         =   "リセット"
            Height          =   330
            Left            =   3660
            TabIndex        =   40
            Top             =   945
            Width           =   720
         End
         Begin VB.TextBox txtAkarusa 
            Height          =   300
            Index           =   2
            Left            =   3000
            MaxLength       =   4
            TabIndex        =   39
            Text            =   "0"
            Top             =   1290
            Width           =   555
         End
         Begin VB.TextBox txtAkarusa 
            Height          =   300
            Index           =   1
            Left            =   3000
            MaxLength       =   4
            TabIndex        =   38
            Text            =   "0"
            Top             =   810
            Width           =   555
         End
         Begin VB.TextBox txtAkarusa 
            Height          =   300
            Index           =   0
            Left            =   3000
            MaxLength       =   4
            TabIndex        =   37
            Text            =   "0"
            Top             =   330
            Width           =   555
         End
         Begin VB.CommandButton cmdAkarusa 
            Caption         =   "実行"
            Height          =   330
            Left            =   3660
            TabIndex        =   41
            Top             =   1350
            Width           =   720
         End
         Begin MSComctlLib.Slider sldAkarusa 
            Height          =   510
            Index           =   0
            Left            =   300
            TabIndex        =   34
            Top             =   255
            Width           =   2700
            _ExtentX        =   4763
            _ExtentY        =   900
            _Version        =   393216
            LargeChange     =   15
            Min             =   -255
            Max             =   255
            TickFrequency   =   15
         End
         Begin MSComctlLib.Slider sldAkarusa 
            Height          =   510
            Index           =   1
            Left            =   300
            TabIndex        =   35
            Top             =   735
            Width           =   2700
            _ExtentX        =   4763
            _ExtentY        =   900
            _Version        =   393216
            LargeChange     =   15
            Min             =   -255
            Max             =   255
            TickFrequency   =   15
         End
         Begin MSComctlLib.Slider sldAkarusa 
            Height          =   510
            Index           =   2
            Left            =   300
            TabIndex        =   36
            Top             =   1215
            Width           =   2700
            _ExtentX        =   4763
            _ExtentY        =   900
            _Version        =   393216
            LargeChange     =   15
            Min             =   -255
            Max             =   255
            TickFrequency   =   15
         End
         Begin VB.Label lblAkarusa 
            Caption         =   "Ｂ"
            Height          =   180
            Index           =   2
            Left            =   120
            TabIndex        =   44
            Top             =   1290
            Width           =   180
         End
         Begin VB.Label lblAkarusa 
            Caption         =   "Ｇ"
            Height          =   180
            Index           =   1
            Left            =   120
            TabIndex        =   43
            Top             =   810
            Width           =   180
         End
         Begin VB.Label lblAkarusa 
            Caption         =   "Ｒ"
            Height          =   180
            Index           =   0
            Left            =   120
            TabIndex        =   42
            Top             =   330
            Width           =   180
         End
      End
      Begin VB.Frame fraSize 
         Caption         =   "拡大縮小"
         Height          =   1200
         Left            =   0
         TabIndex        =   8
         Top             =   0
         Width           =   4500
         Begin VB.CommandButton cmdSizeHi 
            Caption         =   "同比"
            Height          =   255
            Left            =   2880
            TabIndex        =   14
            Top             =   825
            Width           =   600
         End
         Begin VB.CommandButton cmdChngSize 
            Caption         =   "実行"
            Height          =   330
            Left            =   3660
            TabIndex        =   15
            Top             =   750
            Width           =   720
         End
         Begin VB.CommandButton cmdSize05 
            Caption         =   "0.5倍"
            Height          =   255
            Left            =   2880
            TabIndex        =   13
            Top             =   510
            Width           =   600
         End
         Begin VB.CommandButton cmdSize2 
            Caption         =   "2倍"
            Height          =   255
            Left            =   2880
            TabIndex        =   12
            Top             =   195
            Width           =   600
         End
         Begin VB.TextBox txtWidth 
            Alignment       =   1  '右揃え
            Height          =   270
            IMEMode         =   3  'ｵﾌ固定
            Left            =   2040
            MaxLength       =   5
            TabIndex        =   11
            Text            =   "txtWidth"
            Top             =   795
            Width           =   660
         End
         Begin VB.TextBox txtHeight 
            Alignment       =   1  '右揃え
            Height          =   270
            IMEMode         =   3  'ｵﾌ固定
            Left            =   2040
            MaxLength       =   5
            TabIndex        =   10
            Text            =   "txtHeight"
            Top             =   435
            Width           =   660
         End
         Begin VB.Label lblReSize 
            Caption         =   "lblReSize"
            Height          =   825
            Left            =   1305
            TabIndex        =   16
            Top             =   285
            Width           =   1110
         End
         Begin VB.Label lblOrgSize 
            Caption         =   "lblOrgSize"
            Height          =   780
            Left            =   150
            TabIndex        =   9
            Top             =   285
            Width           =   1140
         End
      End
   End
   Begin VB.PictureBox picContainer 
      BorderStyle     =   0  'なし
      Height          =   4950
      Index           =   1
      Left            =   150
      ScaleHeight     =   330
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   300
      TabIndex        =   7
      Top             =   150
      Visible         =   0   'False
      Width           =   4500
      Begin VB.Frame fra2chi 
         Caption         =   "２値化処理"
         Height          =   3075
         Left            =   0
         TabIndex        =   61
         Top             =   1650
         Width           =   4500
         Begin VB.Frame fra2chiC 
            BorderStyle     =   0  'なし
            Caption         =   "Frame1"
            Height          =   210
            Left            =   930
            TabIndex        =   106
            Top             =   2745
            Width           =   2490
            Begin VB.OptionButton opt2chiC 
               Caption         =   "輝度"
               Height          =   210
               Index           =   3
               Left            =   1770
               TabIndex        =   89
               Top             =   0
               Value           =   -1  'True
               Width           =   660
            End
            Begin VB.OptionButton opt2chiC 
               Caption         =   "青"
               Height          =   210
               Index           =   2
               Left            =   1170
               TabIndex        =   88
               Top             =   0
               Width           =   510
            End
            Begin VB.OptionButton opt2chiC 
               Caption         =   "緑"
               Height          =   210
               Index           =   1
               Left            =   570
               TabIndex        =   87
               Top             =   0
               Width           =   510
            End
            Begin VB.OptionButton opt2chiC 
               Caption         =   "赤"
               Height          =   210
               Index           =   0
               Left            =   0
               TabIndex        =   86
               Top             =   0
               Width           =   510
            End
         End
         Begin VB.OptionButton opt2chiAndOr 
            Caption         =   "Or"
            Height          =   225
            Index           =   1
            Left            =   2385
            TabIndex        =   85
            Top             =   2445
            Width           =   660
         End
         Begin VB.OptionButton opt2chiAndOr 
            Caption         =   "And"
            Height          =   225
            Index           =   0
            Left            =   1710
            TabIndex        =   84
            Top             =   2445
            Value           =   -1  'True
            Width           =   660
         End
         Begin VB.Frame fra2chiKosu 
            Caption         =   "個数条件（％）"
            Height          =   930
            Left            =   75
            TabIndex        =   98
            Top             =   1440
            Width           =   4350
            Begin VB.TextBox txt2chiKLower 
               Enabled         =   0   'False
               Height          =   285
               IMEMode         =   3  'ｵﾌ固定
               Index           =   2
               Left            =   3210
               MaxLength       =   12
               TabIndex        =   83
               Text            =   "0.000"
               Top             =   570
               Width           =   540
            End
            Begin VB.TextBox txt2chiKUpper 
               Enabled         =   0   'False
               Height          =   285
               IMEMode         =   3  'ｵﾌ固定
               Index           =   2
               Left            =   3210
               MaxLength       =   12
               TabIndex        =   82
               Text            =   "100.000"
               Top             =   225
               Width           =   540
            End
            Begin VB.CheckBox chk2chiK 
               Height          =   210
               Index           =   2
               Left            =   2925
               TabIndex        =   81
               Top             =   420
               Width           =   210
            End
            Begin VB.TextBox txt2chiKLower 
               Enabled         =   0   'False
               Height          =   285
               IMEMode         =   3  'ｵﾌ固定
               Index           =   1
               Left            =   1740
               MaxLength       =   12
               TabIndex        =   80
               Text            =   "0.000"
               Top             =   570
               Width           =   540
            End
            Begin VB.TextBox txt2chiKUpper 
               Enabled         =   0   'False
               Height          =   285
               IMEMode         =   3  'ｵﾌ固定
               Index           =   1
               Left            =   1740
               MaxLength       =   12
               TabIndex        =   79
               Text            =   "100.000"
               Top             =   225
               Width           =   540
            End
            Begin VB.CheckBox chk2chiK 
               Height          =   210
               Index           =   1
               Left            =   1455
               TabIndex        =   78
               Top             =   420
               Width           =   210
            End
            Begin VB.TextBox txt2chiKLower 
               Enabled         =   0   'False
               Height          =   285
               IMEMode         =   3  'ｵﾌ固定
               Index           =   0
               Left            =   360
               MaxLength       =   12
               TabIndex        =   77
               Text            =   "0.000"
               Top             =   570
               Width           =   540
            End
            Begin VB.TextBox txt2chiKUpper 
               Enabled         =   0   'False
               Height          =   285
               IMEMode         =   3  'ｵﾌ固定
               Index           =   0
               Left            =   360
               MaxLength       =   12
               TabIndex        =   76
               Text            =   "100.000"
               Top             =   225
               Width           =   540
            End
            Begin VB.CheckBox chk2chiK 
               Height          =   210
               Index           =   0
               Left            =   75
               TabIndex        =   75
               Top             =   420
               Width           =   210
            End
            Begin VB.Label lbl2chiKLower 
               Caption         =   "以上"
               Height          =   255
               Index           =   2
               Left            =   3810
               TabIndex        =   104
               Top             =   615
               Width           =   420
            End
            Begin VB.Label lbl2chiKUpper 
               Caption         =   "以下"
               Height          =   255
               Index           =   2
               Left            =   3810
               TabIndex        =   103
               Top             =   285
               Width           =   420
            End
            Begin VB.Label lbl2chiKLower 
               Caption         =   "以上"
               Height          =   255
               Index           =   1
               Left            =   2340
               TabIndex        =   102
               Top             =   615
               Width           =   420
            End
            Begin VB.Label lbl2chiKUpper 
               Caption         =   "以下"
               Height          =   255
               Index           =   1
               Left            =   2340
               TabIndex        =   101
               Top             =   285
               Width           =   420
            End
            Begin VB.Label lbl2chiKLower 
               Caption         =   "以上"
               Height          =   255
               Index           =   0
               Left            =   960
               TabIndex        =   100
               Top             =   615
               Width           =   420
            End
            Begin VB.Label lbl2chiKUpper 
               Caption         =   "以下"
               Height          =   255
               Index           =   0
               Left            =   960
               TabIndex        =   99
               Top             =   285
               Width           =   420
            End
         End
         Begin VB.CommandButton cmd2chi 
            Caption         =   "実行"
            Height          =   300
            Left            =   3540
            TabIndex        =   90
            Top             =   2640
            Width           =   855
         End
         Begin VB.CheckBox chk2chi 
            Caption         =   "半しきい値を使用"
            Height          =   300
            Left            =   2445
            TabIndex        =   65
            Top             =   210
            Width           =   1860
         End
         Begin VB.Frame fra2chiNoudo 
            Caption         =   "濃度値条件"
            Height          =   930
            Left            =   75
            TabIndex        =   92
            Top             =   495
            Width           =   4350
            Begin VB.CheckBox chk2chiN 
               Height          =   210
               Index           =   2
               Left            =   2925
               TabIndex        =   72
               Top             =   420
               Width           =   210
            End
            Begin VB.TextBox txt2chiNLower 
               Enabled         =   0   'False
               Height          =   285
               IMEMode         =   3  'ｵﾌ固定
               Index           =   2
               Left            =   3210
               MaxLength       =   3
               TabIndex        =   74
               Text            =   "0"
               Top             =   570
               Width           =   540
            End
            Begin VB.TextBox txt2chiNUpper 
               Enabled         =   0   'False
               Height          =   285
               IMEMode         =   3  'ｵﾌ固定
               Index           =   2
               Left            =   3210
               MaxLength       =   3
               TabIndex        =   73
               Text            =   "255"
               Top             =   225
               Width           =   540
            End
            Begin VB.CheckBox chk2chiN 
               Height          =   210
               Index           =   1
               Left            =   1455
               TabIndex        =   69
               Top             =   420
               Width           =   210
            End
            Begin VB.TextBox txt2chiNLower 
               Enabled         =   0   'False
               Height          =   285
               IMEMode         =   3  'ｵﾌ固定
               Index           =   1
               Left            =   1740
               MaxLength       =   3
               TabIndex        =   71
               Text            =   "0"
               Top             =   570
               Width           =   540
            End
            Begin VB.TextBox txt2chiNUpper 
               Enabled         =   0   'False
               Height          =   285
               IMEMode         =   3  'ｵﾌ固定
               Index           =   1
               Left            =   1740
               MaxLength       =   3
               TabIndex        =   70
               Text            =   "255"
               Top             =   225
               Width           =   540
            End
            Begin VB.CheckBox chk2chiN 
               Height          =   240
               Index           =   0
               Left            =   75
               TabIndex        =   66
               Top             =   420
               Width           =   210
            End
            Begin VB.TextBox txt2chiNLower 
               Enabled         =   0   'False
               Height          =   285
               IMEMode         =   3  'ｵﾌ固定
               Index           =   0
               Left            =   360
               MaxLength       =   3
               TabIndex        =   68
               Text            =   "0"
               Top             =   570
               Width           =   540
            End
            Begin VB.TextBox txt2chiNUpper 
               Enabled         =   0   'False
               Height          =   285
               IMEMode         =   3  'ｵﾌ固定
               Index           =   0
               Left            =   360
               MaxLength       =   3
               TabIndex        =   67
               Text            =   "255"
               Top             =   225
               Width           =   540
            End
            Begin VB.Label lbl2chiNLower 
               Caption         =   "以上"
               Height          =   255
               Index           =   2
               Left            =   3810
               TabIndex        =   97
               Top             =   615
               Width           =   420
            End
            Begin VB.Label lbl2chiNUpper 
               Caption         =   "以下"
               Height          =   255
               Index           =   2
               Left            =   3810
               TabIndex        =   91
               Top             =   285
               Width           =   420
            End
            Begin VB.Label lbl2chiNLower 
               Caption         =   "以上"
               Height          =   255
               Index           =   1
               Left            =   2340
               TabIndex        =   96
               Top             =   615
               Width           =   420
            End
            Begin VB.Label lbl2chiNUpper 
               Caption         =   "以下"
               Height          =   255
               Index           =   1
               Left            =   2340
               TabIndex        =   95
               Top             =   285
               Width           =   420
            End
            Begin VB.Label lbl2chiNLower 
               Caption         =   "以上"
               Height          =   255
               Index           =   0
               Left            =   960
               TabIndex        =   94
               Top             =   615
               Width           =   420
            End
            Begin VB.Label lbl2chiNUpper 
               Caption         =   "以下"
               Height          =   255
               Index           =   0
               Left            =   960
               TabIndex        =   93
               Top             =   285
               Width           =   420
            End
         End
         Begin VB.Frame fra2chi01 
            BorderStyle     =   0  'なし
            Caption         =   "Frame1"
            Height          =   255
            Left            =   75
            TabIndex        =   62
            Top             =   210
            Width           =   2145
            Begin VB.OptionButton opt2chi01 
               Caption         =   "条件0"
               Height          =   255
               Index           =   1
               Left            =   1110
               TabIndex        =   64
               Top             =   0
               Width           =   960
            End
            Begin VB.OptionButton opt2chi01 
               Caption         =   "条件255"
               Height          =   255
               Index           =   0
               Left            =   0
               TabIndex        =   63
               Top             =   0
               Value           =   -1  'True
               Width           =   960
            End
         End
         Begin VB.Label lbl2chiAndOr 
            Caption         =   "使用濃度"
            Height          =   210
            Index           =   1
            Left            =   75
            TabIndex        =   107
            Top             =   2745
            Width           =   810
         End
         Begin VB.Label lbl2chiAndOr 
            Caption         =   "濃度値と個数の条件"
            Height          =   210
            Index           =   0
            Left            =   75
            TabIndex        =   105
            Top             =   2475
            Width           =   1635
         End
      End
      Begin VB.Frame fraSignal 
         Caption         =   "信号抽出"
         Height          =   1650
         Left            =   0
         TabIndex        =   24
         Top             =   0
         Width           =   4500
         Begin VB.TextBox txtSignal 
            Height          =   270
            IMEMode         =   3  'ｵﾌ固定
            Index           =   3
            Left            =   3375
            MaxLength       =   12
            TabIndex        =   193
            Text            =   "0.000"
            Top             =   990
            Width           =   735
         End
         Begin VB.TextBox txtSignal 
            Height          =   270
            IMEMode         =   3  'ｵﾌ固定
            Index           =   2
            Left            =   2265
            MaxLength       =   12
            TabIndex        =   192
            Text            =   "0.333"
            Top             =   990
            Width           =   735
         End
         Begin VB.TextBox txtSignal 
            Height          =   270
            IMEMode         =   3  'ｵﾌ固定
            Index           =   1
            Left            =   1185
            MaxLength       =   12
            TabIndex        =   191
            Text            =   "0.333"
            Top             =   990
            Width           =   735
         End
         Begin VB.TextBox txtSignal 
            Height          =   270
            IMEMode         =   3  'ｵﾌ固定
            Index           =   0
            Left            =   105
            MaxLength       =   12
            TabIndex        =   190
            Text            =   "0.333"
            Top             =   990
            Width           =   735
         End
         Begin VB.CommandButton cmdSignal 
            Caption         =   "オリジナル変換"
            Height          =   300
            Left            =   2475
            TabIndex        =   189
            Top             =   1290
            Width           =   1935
         End
         Begin VB.CommandButton cmdShikisaCr 
            Caption         =   "Ｃｒ（色差）"
            Height          =   300
            Left            =   3390
            TabIndex        =   32
            Top             =   570
            Width           =   990
         End
         Begin VB.CommandButton cmdShikisaCb 
            Caption         =   "Ｃｂ（色差）"
            Height          =   300
            Left            =   2295
            TabIndex        =   31
            Top             =   570
            Width           =   990
         End
         Begin VB.CommandButton cmdShikisaQ 
            Caption         =   "Ｑ（色差）"
            Height          =   300
            Left            =   1200
            TabIndex        =   30
            Top             =   570
            Width           =   990
         End
         Begin VB.CommandButton cmdShikisaI 
            Caption         =   "Ｉ（色差）"
            Height          =   300
            Left            =   120
            TabIndex        =   29
            Top             =   570
            Width           =   990
         End
         Begin VB.CommandButton cmdKidoY 
            Caption         =   "Ｙ（輝度）"
            Height          =   300
            Left            =   2640
            TabIndex        =   28
            Top             =   210
            Width           =   990
         End
         Begin VB.CommandButton cmdBlue 
            Caption         =   "Ｂ（青）"
            Height          =   300
            Left            =   1800
            TabIndex        =   27
            Top             =   210
            Width           =   720
         End
         Begin VB.CommandButton cmdGreen 
            Caption         =   "Ｇ（緑）"
            Height          =   300
            Left            =   945
            TabIndex        =   26
            Top             =   210
            Width           =   720
         End
         Begin VB.CommandButton cmdRed 
            Caption         =   "Ｒ（赤）"
            Height          =   300
            Left            =   120
            TabIndex        =   25
            Top             =   210
            Width           =   720
         End
         Begin VB.Label lblSignal 
            AutoSize        =   -1  'True
            Caption         =   "xB+"
            Height          =   180
            Index           =   2
            Left            =   3030
            TabIndex        =   196
            Top             =   1035
            Width           =   300
         End
         Begin VB.Label lblSignal 
            AutoSize        =   -1  'True
            Caption         =   "xG+"
            Height          =   180
            Index           =   1
            Left            =   1965
            TabIndex        =   195
            Top             =   1035
            Width           =   300
         End
         Begin VB.Label lblSignal 
            AutoSize        =   -1  'True
            Caption         =   "xR+"
            Height          =   180
            Index           =   0
            Left            =   855
            TabIndex        =   194
            Top             =   1035
            Width           =   300
         End
      End
   End
   Begin VB.PictureBox picContainer 
      BorderStyle     =   0  'なし
      Height          =   4950
      Index           =   3
      Left            =   150
      ScaleHeight     =   330
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   300
      TabIndex        =   18
      Top             =   150
      Visible         =   0   'False
      Width           =   4500
      Begin VB.Frame fraKaiten 
         Caption         =   "回転"
         Height          =   885
         Left            =   0
         TabIndex        =   160
         Top             =   3150
         Width           =   4500
         Begin VB.CommandButton cmdSayu 
            Caption         =   "左右回転"
            Height          =   330
            Left            =   1200
            TabIndex        =   162
            Top             =   210
            Width           =   990
         End
         Begin VB.CommandButton cmdJoge 
            Caption         =   "上下回転"
            Height          =   330
            Left            =   105
            TabIndex        =   161
            Top             =   210
            Width           =   990
         End
      End
      Begin VB.Frame fraHanten 
         Caption         =   "色の反転"
         Height          =   615
         Left            =   0
         TabIndex        =   130
         Top             =   2550
         Width           =   4500
         Begin VB.CommandButton cmdHanten 
            Caption         =   "青色反転"
            Height          =   300
            Index           =   3
            Left            =   3360
            TabIndex        =   159
            Top             =   210
            Width           =   990
         End
         Begin VB.CommandButton cmdHanten 
            Caption         =   "緑色反転"
            Height          =   300
            Index           =   2
            Left            =   2250
            TabIndex        =   158
            Top             =   210
            Width           =   990
         End
         Begin VB.CommandButton cmdHanten 
            Caption         =   "赤色反転"
            Height          =   300
            Index           =   1
            Left            =   1170
            TabIndex        =   157
            Top             =   210
            Width           =   990
         End
         Begin VB.CommandButton cmdHanten 
            Caption         =   "全色反転"
            Height          =   300
            Index           =   0
            Left            =   90
            TabIndex        =   131
            Top             =   210
            Width           =   990
         End
      End
      Begin VB.Frame fraMozaiku 
         Caption         =   "モザイク"
         Height          =   1125
         Left            =   0
         TabIndex        =   122
         Top             =   1425
         Width           =   4500
         Begin VB.OptionButton optMozaiku 
            Caption         =   "分割サイズ"
            Height          =   195
            Index           =   1
            Left            =   1785
            TabIndex        =   126
            Top             =   255
            Width           =   1200
         End
         Begin VB.OptionButton optMozaiku 
            Caption         =   "分割数"
            Height          =   195
            Index           =   0
            Left            =   105
            TabIndex        =   123
            Top             =   255
            Value           =   -1  'True
            Width           =   840
         End
         Begin VB.TextBox txtMzX 
            Height          =   270
            Left            =   2160
            MaxLength       =   4
            TabIndex        =   127
            Text            =   "8"
            Top             =   465
            Width           =   630
         End
         Begin VB.TextBox txtMzY 
            Height          =   270
            Left            =   2160
            MaxLength       =   4
            TabIndex        =   128
            Text            =   "8"
            Top             =   780
            Width           =   630
         End
         Begin VB.TextBox txtMzH 
            Height          =   240
            Left            =   480
            MaxLength       =   4
            TabIndex        =   125
            Text            =   "8"
            Top             =   780
            Width           =   630
         End
         Begin VB.TextBox txtMzW 
            Height          =   240
            Left            =   480
            MaxLength       =   4
            TabIndex        =   124
            Text            =   "8"
            Top             =   480
            Width           =   630
         End
         Begin VB.CommandButton cmdMozaiku 
            Caption         =   "実行"
            Height          =   300
            Left            =   3465
            TabIndex        =   129
            Top             =   720
            Width           =   945
         End
         Begin VB.Label lblMozaiku 
            AutoSize        =   -1  'True
            Caption         =   "幅"
            Height          =   180
            Index           =   0
            Left            =   105
            TabIndex        =   156
            Top             =   525
            Width           =   180
         End
         Begin VB.Label lblMozaiku 
            AutoSize        =   -1  'True
            Caption         =   "pixel"
            Height          =   180
            Index           =   7
            Left            =   2865
            TabIndex        =   155
            Top             =   825
            Width           =   360
         End
         Begin VB.Label lblMozaiku 
            AutoSize        =   -1  'True
            Caption         =   "pixel"
            Height          =   180
            Index           =   6
            Left            =   2850
            TabIndex        =   154
            Top             =   525
            Width           =   360
         End
         Begin VB.Label lblMozaiku 
            AutoSize        =   -1  'True
            Caption         =   "高さ"
            Height          =   180
            Index           =   5
            Left            =   1785
            TabIndex        =   153
            Top             =   825
            Width           =   315
         End
         Begin VB.Label lblMozaiku 
            AutoSize        =   -1  'True
            Caption         =   "幅"
            Height          =   180
            Index           =   4
            Left            =   1785
            TabIndex        =   152
            Top             =   525
            Width           =   180
         End
         Begin VB.Label lblMozaiku 
            AutoSize        =   -1  'True
            Caption         =   "分割"
            Height          =   180
            Index           =   3
            Left            =   1170
            TabIndex        =   151
            Top             =   825
            Width           =   360
         End
         Begin VB.Label lblMozaiku 
            AutoSize        =   -1  'True
            Caption         =   "分割"
            Height          =   180
            Index           =   2
            Left            =   1170
            TabIndex        =   150
            Top             =   525
            Width           =   360
         End
         Begin VB.Label lblMozaiku 
            AutoSize        =   -1  'True
            Caption         =   "高さ"
            Height          =   180
            Index           =   1
            Left            =   105
            TabIndex        =   149
            Top             =   825
            Width           =   315
         End
      End
      Begin VB.Frame fraGosei 
         Caption         =   "合成"
         Height          =   1425
         Left            =   0
         TabIndex        =   108
         Top             =   0
         Width           =   4500
         Begin VB.CommandButton cmdGoseiCopy 
            Caption         =   "移す"
            Height          =   330
            Left            =   960
            TabIndex        =   198
            Top             =   585
            Width           =   570
         End
         Begin VB.CommandButton cmdGosei 
            Caption         =   "XOR"
            Height          =   330
            Index           =   10
            Left            =   3120
            TabIndex        =   185
            Top             =   990
            Width           =   570
         End
         Begin VB.CommandButton cmdGosei 
            Caption         =   "AND"
            Height          =   330
            Index           =   9
            Left            =   2490
            TabIndex        =   119
            Top             =   990
            Width           =   570
         End
         Begin VB.CommandButton cmdGosei 
            Caption         =   "OR"
            Height          =   330
            Index           =   8
            Left            =   1860
            TabIndex        =   118
            Top             =   990
            Width           =   570
         End
         Begin VB.CommandButton cmdGosei 
            Caption         =   "同値"
            Height          =   330
            Index           =   7
            Left            =   3750
            TabIndex        =   117
            Top             =   600
            Width           =   570
         End
         Begin VB.CommandButton cmdGosei 
            Caption         =   "平均"
            Height          =   330
            Index           =   6
            Left            =   3120
            TabIndex        =   113
            Top             =   600
            Width           =   570
         End
         Begin VB.CommandButton cmdGosei 
            Caption         =   "最小"
            Height          =   330
            Index           =   5
            Left            =   2490
            TabIndex        =   116
            Top             =   600
            Width           =   570
         End
         Begin VB.CommandButton cmdGosei 
            Caption         =   "最大"
            Height          =   330
            Index           =   4
            Left            =   1860
            TabIndex        =   112
            Top             =   600
            Width           =   570
         End
         Begin VB.CommandButton cmdGosei 
            Caption         =   "除算"
            Height          =   330
            Index           =   3
            Left            =   3750
            TabIndex        =   115
            Top             =   210
            Width           =   570
         End
         Begin VB.PictureBox picGPreview 
            AutoRedraw      =   -1  'True
            Height          =   780
            Left            =   90
            ScaleHeight     =   48
            ScaleMode       =   3  'ﾋﾟｸｾﾙ
            ScaleWidth      =   48
            TabIndex        =   120
            Top             =   195
            Width           =   780
         End
         Begin VB.CommandButton cmdGoseiOpen 
            Caption         =   "開く"
            Height          =   330
            Left            =   960
            TabIndex        =   109
            Top             =   210
            Width           =   570
         End
         Begin VB.CommandButton cmdGosei 
            Caption         =   "全差"
            Height          =   330
            Index           =   2
            Left            =   3120
            TabIndex        =   114
            Top             =   210
            Width           =   570
         End
         Begin VB.CommandButton cmdGosei 
            Caption         =   "負差"
            Height          =   330
            Index           =   1
            Left            =   2490
            TabIndex        =   111
            Top             =   210
            Width           =   570
         End
         Begin VB.CommandButton cmdGosei 
            Caption         =   "正差"
            Height          =   330
            Index           =   0
            Left            =   1860
            TabIndex        =   110
            Top             =   210
            Width           =   570
         End
      End
   End
   Begin MSComctlLib.TabStrip tabTabStrip 
      Height          =   5400
      Left            =   75
      TabIndex        =   5
      Top             =   75
      Width           =   4650
      _ExtentX        =   8202
      _ExtentY        =   9525
      Placement       =   1
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   6
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Ａ"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Ｂ"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Ｃ"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Ｄ"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab5 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Ｅ"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab6 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Ｆ"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "メニュー(&M)"
      Begin VB.Menu mnuOpen 
         Caption         =   "開く(&O)"
      End
      Begin VB.Menu mnuSave 
         Caption         =   "保存(&S)"
      End
      Begin VB.Menu mnuBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "終了(&X)"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub InfoSize()
    lblOrgSize.Caption = "元画像" _
        & Chr$(13) & "Height = " _
        & Format$(frmLoad.picView.ScaleHeight) _
        & Chr$(13) & Chr$(13) & "Width  = " _
        & Format$(frmLoad.picView.ScaleWidth)
    txtHeight.Text = Str$(frmLoad.picView.ScaleHeight)
    txtWidth.Text = Str$(frmLoad.picView.ScaleWidth)
End Sub

Private Sub Seltxt(txt1 As TextBox)
    txt1.SelStart = 0
    txt1.SelLength = Len(txt1.Text)
End Sub

Private Sub chk2chiK_Click(Index As Integer)
    If chk2chiK(Index).Value = vbChecked Then
        txt2chiKUpper(Index).Enabled = True
        txt2chiKLower(Index).Enabled = True
    Else
        txt2chiKUpper(Index).Enabled = False
        txt2chiKLower(Index).Enabled = False
    End If
End Sub

Private Sub chk2chiN_Click(Index As Integer)
    If chk2chiN(Index).Value = vbChecked Then
        txt2chiNUpper(Index).Enabled = True
        txt2chiNLower(Index).Enabled = True
    Else
        txt2chiNUpper(Index).Enabled = False
        txt2chiNLower(Index).Enabled = False
    End If
End Sub


Private Sub chkHist2_Click()
    If chkHist2.Value = vbChecked Then
        chkHist2A.Enabled = True
    Else
        chkHist2A.Enabled = False
    End If
End Sub

Private Sub chkHist2B_Click()
    If chkHist2B.Value = vbChecked Then
        chkHist2.Enabled = False
        chkHist2A.Enabled = False
    Else
        chkHist2.Enabled = True
        If chkHist2.Value = vbChecked Then
            chkHist2A.Enabled = True
        End If
    End If
End Sub

Private Sub cmd2chi_Click()
    If chk2chiN(0).Value = vbChecked _
            Or chk2chiN(1).Value = vbChecked _
            Or chk2chiN(2).Value = vbChecked _
            Or chk2chiK(0).Value = vbChecked _
            Or chk2chiK(1).Value = vbChecked _
            Or chk2chiK(1).Value = vbChecked Then
        If opt2chiAndOr(0).Value Then
            Call Shori2chiAnd
        Else
            Call Shori2chiOr
        End If
    End If
End Sub

Private Sub cmd2chiOtsu_Click()
    Call Shori2chiOtsu
End Sub

Private Sub cmd2chiWnd_Click()
    frmNichi.Show vbModeless, Me
End Sub

Private Sub cmd2chiWnd2_Click()
    frmNichi2.Show vbModeless, Me
End Sub

Private Sub cmdAkarusa_Click()
    Call ChangeNoudo
End Sub

Private Sub cmdAkReset_Click()
    Dim i As Integer
    For i = 0 To 2
        sldAkarusa(i).Value = 0&
        txtAkarusa(i).Text = "0"
    Next i
End Sub

Private Sub cmdBlue_Click()
    Call ExtrctRGB(EXT_BLUE)
End Sub

Private Sub cmdChngSize_Click()
    Call ChangeSize
End Sub

Private Sub cmdCopy_Click()
    Dim rmsg As VbMsgBoxResult
    rmsg = MsgBox("処理後の画像を元画像として設定します。", vbOKCancel, "確認")
    If rmsg = vbCancel Then
        Exit Sub
    End If
    frmLoad.picView.Cls
    frmLoad.picView.Picture = frmResult.picView.Image
    frmLoad.Caption = frmLoad.Caption & "*"
    Call InfoSize
End Sub

Private Sub cmdCutting_Click()
    Call pCutting
End Sub


Private Sub cmdFukuGou_Click()
    frmFukuGou.Show vbModal, Me
End Sub

Private Sub cmdFukusu_Click()
    frmList.Show vbModeless
End Sub

Private Sub cmdGosei_Click(Index As Integer)
    Call Gosei(Index)
End Sub

Private Sub cmdGoseiCopy_Click()
    Dim rmsg As VbMsgBoxResult
    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim sw As Single, sh As Single
    Dim sx As Single, sy As Single
    Dim c As Long
    
    rmsg = MsgBox("処理画像を合成用画像として使用しますか？", vbOKCancel, "合成")
    
    If rmsg = vbCancel Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    fraGosei.Caption = "合成"
    frmMain.picLoad.Picture = frmResult.picView.Image
    
    dw = CInt(picGPreview.ScaleWidth)
    dh = CInt(picGPreview.ScaleHeight)
    sw = picLoad.ScaleWidth
    sh = picLoad.ScaleHeight
    For i = 0 To dh - 1
        sy = Int(CSng(i) / CSng(dh) * sh)
        For j = 0 To dw - 1
            sx = Int(CSng(j) / CSng(dw) * sw)
            c = picLoad.Point(sx, sy)
            picGPreview.PSet (CSng(j), CSng(i)), c
        Next j
        prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow
    
End Sub

Private Sub cmdGoseiOpen_Click()
    On Error GoTo cmdGoseiOpen_Click_Error
    Dim fn As String
    Dim dw As Integer, dh As Integer
    Dim sw As Single, sh As Single
    Dim sx As Single, sy As Single
    Dim i As Integer, j As Integer
    Dim c As Long
    dlgOpen2.ShowOpen
    fn = dlgOpen2.FileName
    fraGosei.Caption = "合成 " & dlgOpen2.FileTitle
    picLoad.Picture = LoadPicture(fn)
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    dw = CInt(picGPreview.ScaleWidth)
    dh = CInt(picGPreview.ScaleHeight)
    sw = picLoad.ScaleWidth
    sh = picLoad.ScaleHeight
    For i = 0 To dh - 1
        sy = Int(CSng(i) / CSng(dh) * sh)
        For j = 0 To dw - 1
            sx = Int(CSng(j) / CSng(dw) * sw)
            c = picLoad.Point(sx, sy)
            picGPreview.PSet (CSng(j), CSng(i)), c
        Next j
        prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    Screen.MousePointer = vbArrow
    Exit Sub
cmdGoseiOpen_Click_Error:
    MsgBox Error(Err), , "ERROR" & Str$(Err)
End Sub

Private Sub cmdGreen_Click()
    Call ExtrctRGB(EXT_GREEN)
End Sub

Private Sub cmdHanten_Click(Index As Integer)
    Call Hanten(Index)
End Sub

Private Sub cmdHeikin_Click()
    Call FltrHeikin
End Sub

Private Sub cmdHist_Click(Index As Integer)
    Call showHist(Index)
End Sub

Private Sub cmdHist2_Click()
    Call DrawHist
End Sub

Private Sub cmdIroiro1_Click()
    Call NodoSeikika
End Sub

Private Sub cmdJoge_Click()
    Call KaitenJoge
End Sub

Private Sub cmdKidoY_Click()
    Call ExtrctRGBTrans(0.3!, 0.59!, 0.11!, 0!)
End Sub

Private Sub cmdLaplacian_Click(Index As Integer)
    Call MskLaplacian(Index)
End Sub

Private Sub cmdMedian_Click()
    Call FltrMedian
End Sub

Private Sub cmdMinMax_Click(Index As Integer)
    Call FltrMinMax(Index)
End Sub

Private Sub cmdMinMaxg1_Click()
    Call MinMaxg1
End Sub

Private Sub cmdMinMaxg2_Click()
    Call MinMaxg2
End Sub

Private Sub cmdMinMaxg3_Click()
    Call MinMaxg3
End Sub

Private Sub cmdMinMaxg4_Click()
    Call MinMaxg4
End Sub

Private Sub cmdMinMaxg5_Click()
    Call MinMaxg5
End Sub

Private Sub cmdMozaiku_Click()
    Call Mozaiku
End Sub

Private Sub cmdMskMtrx_Click(Index As Integer)
    Call MskMtrx(Index)
End Sub

Private Sub cmdNDCut_Click()
    Call ShoriNDCut
End Sub

Private Sub cmdNDPress_Click()
    Call ShoriNDPress
End Sub

Private Sub cmdNoise_Click()
    Call MakeNoise
End Sub

Private Sub cmdNoudoCA1_Click()
    Call ShoriNoudoCA1
End Sub

Private Sub cmdNoudoCA2_Click()
    Call ShoriNoudoCA2
End Sub

Private Sub cmdOpen_Click()
    On Error GoTo cmdOpen_Click_Error
    Dim fn As String
    dlgOpen.ShowOpen
    fn = dlgOpen.FileName
    frmLoad.picView.Picture = LoadPicture(fn)
    frmLoad.Caption = "元画像 " & dlgOpen.FileTitle
    Call InfoSize
    Me.SetFocus
    Exit Sub
cmdOpen_Click_Error:
    MsgBox Error(Err), , "ERROR" & Str$(Err)
End Sub

Private Sub cmdRed_Click()
    Call ExtrctRGB(EXT_RED)
End Sub

Private Sub cmdRuiseki_Click(Index As Integer)
    Call RuisekiHist(Index)
End Sub

Private Sub cmdRyoshika_Click()
    Call Ryoshika
End Sub

Private Sub cmdSave_Click()
    On Error GoTo cmdSave_Click_Error
    Dim fn As String
    dlgSave.ShowSave
    fn = dlgSave.FileName
    SavePicture frmResult.picView.Image, fn
    MsgBox "保存に成功しました。" & Chr$(13) & fn, , "メッセージ"
    Exit Sub
cmdSave_Click_Error:
    MsgBox Error(Err), , "ERROR" & Str$(Err)
End Sub

Private Sub cmdSayu_Click()
    Call KaitenSayu
End Sub

Private Sub cmdSentaku_Click()
    frmSentaku.Show vbModal, Me
End Sub

Private Sub cmdShikisaCb_Click()
    Call ExtrctRGBTrans(-0.17!, -0.33!, 0.5!, 128!)
End Sub

Private Sub cmdShikisaCr_Click()
    Call ExtrctRGBTrans(0.5!, -0.42!, -0.08!, 128!)
End Sub

Private Sub cmdShikisaI_Click()
    Call ExtrctRGBTrans(0.6!, -0.28!, -0.32!, 0!)
End Sub

Private Sub cmdShikisaQ_Click()
    Call ExtrctRGBTrans(0.21!, -0.52!, 0.31!, 0!)
End Sub

Private Sub cmdSignal_Click()
    Dim fl(3) As Single
    Dim i As Integer
    For i = 0 To 3
        fl(i) = CSng(Val(txtSignal(i).Text))
    Next i
    Call ExtrctRGBTrans(fl(0), fl(1), fl(2), fl(3))
End Sub

Private Sub cmdSize05_Click()
    txtHeight.Text = Str$(CInt(0.5! * frmLoad.picView.ScaleHeight))
    txtWidth.Text = Str$(CInt(0.5! * frmLoad.picView.ScaleWidth))
End Sub

Private Sub cmdSize2_Click()
    txtHeight.Text = Str$(2! * frmLoad.picView.ScaleHeight)
    txtWidth.Text = Str$(2! * frmLoad.picView.ScaleWidth)
End Sub

Private Sub cmdSizeHi_Click()
    txtWidth.Text = Str$(CSng(Val(txtHeight.Text)) _
            * frmLoad.picView.ScaleWidth _
            / frmLoad.picView.ScaleHeight)
End Sub

Private Sub cmdOtsu_Click()
    MsgBox "閾値 " & Str$(ShikiiOtsu), , "大津の方法"
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_Load()
    
    ChDir App.Path
    frmLoad.Show vbModeless
    frmResult.Show vbModeless
    Call InfoSize
    opt2chi01(0).ToolTipText = _
        "以下の条件を満たす場合は濃度値は 255 に、" _
            & "満たさない場合は濃度値を 0 にする。"
    opt2chi01(1).ToolTipText = _
        "以下の条件を満たす場合は濃度値は 0 に、" _
            & "満たさない場合は濃度値を 255 にする。"
    chk2chi.ToolTipText = _
        "以下の条件を満たした場合に、" _
            & "濃度値を変更せず使用する。"
    lblReSize.Caption = "変更サイズ" _
        & Chr$(13) & "Height =" _
        & Chr$(13) & Chr$(13) & "Width  ="
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub lblMzY_Click()

End Sub

Private Sub mnuExit_Click()
    End
End Sub

Private Sub mnuOpen_Click()
    Call cmdOpen_Click
End Sub

Private Sub mnuSave_Click()
    Call cmdSave_Click
End Sub

Private Sub optMigishita_Click(Index As Integer)
    Dim d As Single
    Select Case Index
    Case 0
        lblRight.Caption = "右端"
        lblBottom.Caption = "下端"
        d = CSng(Val(txtRight.Text)) + CSng(Val(txtLeft.Text)) - 1!
        txtRight.Text = Format$(d)
        d = CSng(Val(txtBottom.Text)) + CSng(Val(txtTop.Text)) - 1!
        txtBottom.Text = Format$(d)
    Case 1
        lblRight.Caption = "幅"
        lblBottom.Caption = "高さ"
        d = CSng(Val(txtRight.Text)) - CSng(Val(txtLeft.Text)) + 1!
        txtRight.Text = Format$(d)
        d = CSng(Val(txtBottom.Text)) - CSng(Val(txtTop.Text)) + 1!
        txtBottom.Text = Format$(d)
    End Select
End Sub

Private Sub optNDCut_Click(Index As Integer)
    Dim dv As Double
    Dim dw As Integer, dh As Integer
    dv = CDbl(Val(txtNDCut.Text))
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    Select Case Index
    Case 0
        dv = Int(dv / 100 * CDbl(dw) * CDbl(dh))
        If CLng(dv) > (CLng(dw) * CLng(dh)) Then dv = CDbl(dw) * CDbl(dh)
        If dv < 1# Then dv = 1#
        txtNDCut.Text = Format$(dv)
    Case 1
        dv = 100# * dv / CDbl(dw) / CDbl(dh)
        If dv >= 100# Then dv = 99.999
        If dv <= 0# Then dv = 0.001
        txtNDCut.Text = Format$(dv, "0.000")
    End Select
End Sub

Private Sub optUnit_Click(Index As Integer)
    Dim sw As Single, sh As Single
    sw = frmLoad.picView.ScaleWidth
    sh = frmLoad.picView.ScaleHeight
    Select Case Index
    Case 0
        txtLeft.Text = Format$(CInt(sw _
                * CSng(Val(txtLeft.Text)) / 100!))
        txtTop.Text = Format$(CInt(sh _
                * CSng(Val(txtTop.Text)) / 100!))
        txtRight.Text = Format$(CInt(sw _
                * CSng(Val(txtRight.Text)) / 100!))
        txtBottom.Text = Format$(CInt(sh _
                * CSng(Val(txtBottom.Text)) / 100!))
    Case 1
        txtLeft.Text = Format$(100! _
                * CSng(Val(txtLeft.Text)) / sw, "0.00")
        txtTop.Text = Format$(100! _
                * CSng(Val(txtTop.Text)) / sh, "0.00")
        txtRight.Text = Format$(100! _
                * CSng(Val(txtRight.Text)) / sw, "0.00")
        txtBottom.Text = Format$(100! _
                * CSng(Val(txtBottom.Text)) / sh, "0.00")
    End Select
End Sub

Private Sub sldAkarusa_Scroll(Index As Integer)
    txtAkarusa(Index).Text = Format$(sldAkarusa(Index).Value)
End Sub

Private Sub sldHeikatsu_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If (sldHeikatsu.Value Mod 2&) = 0& Then
        sldHeikatsu.Value = 2& * (sldHeikatsu.Value \ 2&) + 1&
    End If

End Sub

Private Sub sldHeikatsu_Scroll()
    lblHeikatsu.Caption = Format$(sldHeikatsu.Value) _
            & "x" & Format$(sldHeikatsu.Value)

End Sub

Private Sub tabTabStrip_BeforeClick(Cancel As Integer)
    Dim i As Integer
    i = tabTabStrip.SelectedItem.Index - 1
    picContainer(i).Visible = False
End Sub

Private Sub tabTabStrip_Click()
    Dim i As Integer
    i = tabTabStrip.SelectedItem.Index - 1
    picContainer(i).Visible = True
End Sub

Private Sub txt2chiKLower_GotFocus(Index As Integer)
    Call Seltxt(txt2chiKLower(Index))
End Sub

Private Sub txt2chiKLower_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then cmd2chi.SetFocus
End Sub

Private Sub txt2chiKLower_LostFocus(Index As Integer)
    Dim dv As Double
    dv = Val(txt2chiKLower(Index).Text)
    If dv < 0# Then dv = 0#
    If dv > 100# Then dv = 100#
    txt2chiKLower(Index).Text = Format$(dv, "0.000")
End Sub

Private Sub txt2chiKUpper_GotFocus(Index As Integer)
    Call Seltxt(txt2chiKUpper(Index))
End Sub

Private Sub txt2chiKUpper_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then txt2chiKLower(Index).SetFocus
End Sub

Private Sub txt2chiKUpper_LostFocus(Index As Integer)
    Dim dv As Double
    dv = Val(txt2chiKUpper(Index).Text)
    If dv < 0# Then dv = 0#
    If dv > 100# Then dv = 100#
    txt2chiKUpper(Index).Text = Format$(dv, "0.000")
End Sub

Private Sub txt2chiNLower_GotFocus(Index As Integer)
    Call Seltxt(txt2chiNLower(Index))
End Sub

Private Sub txt2chiNLower_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then cmd2chi.SetFocus
End Sub

Private Sub txt2chiNLower_LostFocus(Index As Integer)
    Dim iv As Integer
    iv = CInt(Val(txt2chiNLower(Index).Text))
    If iv < 0 Then iv = 0
    If iv > &HFF Then iv = &HFF
    txt2chiNLower(Index).Text = Format$(iv)
End Sub

Private Sub txt2chiNUpper_GotFocus(Index As Integer)
    Call Seltxt(txt2chiNUpper(Index))
End Sub

Private Sub txt2chiNUpper_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then txt2chiNLower(Index).SetFocus
End Sub

Private Sub txt2chiNUpper_LostFocus(Index As Integer)
    Dim iv As Integer
    iv = CInt(Val(txt2chiNUpper(Index).Text))
    If iv < 0 Then iv = 0
    If iv > &HFF Then iv = &HFF
    txt2chiNUpper(Index).Text = Format$(iv)
End Sub

Private Sub txtAkarusa_GotFocus(Index As Integer)
    Call Seltxt(txtAkarusa(Index))
End Sub

Private Sub txtAkarusa_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If Index = txtAkarusa.Count - 1 Then
            cmdAkarusa.SetFocus
        Else
            txtAkarusa(Index + 1).SetFocus
        End If
    End If
End Sub

Private Sub txtAkarusa_LostFocus(Index As Integer)
    Dim a As Integer
    a = CInt(Val(txtAkarusa(Index)))
    If a < -&HFF Then a = -&HFF
    If a > &HFF Then a = &HFF
    txtAkarusa(Index).Text = Format$(a)
    sldAkarusa(Index).Value = CLng(a)
End Sub

Private Sub txtBottom_GotFocus()
    Call Seltxt(txtBottom)
End Sub

Private Sub txtBottom_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then cmdCutting.SetFocus
End Sub

Private Sub txtBottom_LostFocus()
    Dim dy As Long, sy As Single
    If optUnit(0).Value Then
        dy = CLng(Val(txtBottom.Text))
        If dy < 0& Then dy = 0&
        If dy > CLng(frmLoad.picView.ScaleHeight) Then _
                dy = CLng(frmLoad.picView.ScaleHeight)
        txtBottom.Text = Format$(dy)
    Else
        sy = CSng(Val(txtBottom.Text))
        If sy < 0! Then sy = 0!
        If sy > 100! Then sy = 100!
        txtBottom.Text = Format$(sy, "0.00")
    End If
End Sub

Private Sub txtHeight_Change()
    Dim i As Integer, l As Integer
    Dim s As String, ss As String
    l = Len(txtHeight.Text)
    s = ""
    For i = 1 To l
        ss = Mid$(txtHeight.Text, i, 1)
        If ss = Format$(Val(ss)) Then
            s = s & ss
        End If
    Next i
    txtHeight.Text = s
End Sub

Private Sub txtHeight_GotFocus()
    Call Seltxt(txtHeight)
End Sub

Private Sub txtHeight_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then txtWidth.SetFocus
End Sub

Private Sub txtLeft_GotFocus()
    Call Seltxt(txtLeft)
End Sub

Private Sub txtLeft_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then txtTop.SetFocus
End Sub

Private Sub txtLeft_LostFocus()
    Dim dx As Long, sx As Single
    If optUnit(0).Value Then
        dx = CLng(Val(txtLeft.Text))
        If dx < 0& Then dx = 0&
        If dx > CLng(frmLoad.picView.ScaleWidth) Then _
                dx = CLng(frmLoad.picView.ScaleWidth)
        txtLeft.Text = Format$(dx)
    Else
        sx = CSng(Val(txtLeft.Text))
        If sx < 0! Then sx = 0!
        If sx > 100! Then sx = 100!
        txtLeft.Text = Format$(sx, "0.00")
    End If
End Sub

Private Sub txtMzH_GotFocus()
    Call Seltxt(txtMzH)
End Sub

Private Sub txtMzH_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If optMozaiku(1).Value Then
            optMozaiku(0).Value = True
        End If
        cmdMozaiku.SetFocus
    End If
End Sub

Private Sub txtMzH_LostFocus()
    Dim iv As Integer
    iv = CInt(Val(txtMzH.Text))
    If iv >= CInt(frmLoad.picView.ScaleHeight) _
        Then iv = CInt(frmLoad.picView.ScaleHeight - 1!)
    If iv < 1 Then iv = 1
    txtMzH.Text = Format$(iv)
End Sub

Private Sub txtMzW_GotFocus()
    Call Seltxt(txtMzW)
End Sub

Private Sub txtMzW_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If optMozaiku(1).Value Then
            optMozaiku(0).Value = True
        End If
        txtMzH.SetFocus
    End If
End Sub

Private Sub txtMzW_LostFocus()
    Dim iv As Integer
    iv = CInt(Val(txtMzW.Text))
    If iv >= CInt(frmLoad.picView.ScaleWidth) _
        Then iv = CInt(frmLoad.picView.ScaleWidth - 1!)
    If iv < 1 Then iv = 1
    txtMzW.Text = Format$(iv)
End Sub

Private Sub txtMzX_GotFocus()
    Call Seltxt(txtMzX)
End Sub

Private Sub txtMzX_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If optMozaiku(0).Value Then
            optMozaiku(1).Value = True
        End If
        txtMzY.SetFocus
    End If
End Sub

Private Sub txtMzX_LostFocus()
    Dim lv As Long
    lv = CLng(Val(txtMzX.Text))
    If lv >= CLng(frmLoad.picView.ScaleWidth) _
        Then lv = CLng(frmLoad.picView.ScaleWidth - 1!)
    If lv < 1& Then lv = 1&
    txtMzX.Text = Format$(lv)
End Sub

Private Sub txtMzY_GotFocus()
    Call Seltxt(txtMzY)
End Sub

Private Sub txtMzY_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        If optMozaiku(0).Value Then
            optMozaiku(1).Value = True
        End If
        cmdMozaiku.SetFocus
    End If
End Sub

Private Sub txtMzY_LostFocus()
    Dim iv As Integer
    iv = CInt(Val(txtMzY.Text))
    If iv >= CInt(frmLoad.picView.ScaleHeight) _
        Then iv = CInt(frmLoad.picView.ScaleHeight - 1!)
    If iv < 1 Then iv = 1
    txtMzY.Text = Format$(iv)
End Sub

Private Sub txtNDCut_LostFocus()
    Dim dv As Double
    Dim dw As Integer, dh As Integer
    dv = CDbl(Val(txtNDCut.Text))
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    If optNDCut(0).Value Then
        If CLng(dv) > (CLng(dw) * CLng(dh)) Then dv = CDbl(dw) * CDbl(dh)
        If dv < 1# Then dv = 1#
        txtNDCut.Text = Format$(dv)
    Else
        If dv >= 100# Then dv = 99.999
        If dv <= 0# Then dv = 0.001
        txtNDCut.Text = Format$(dv, "0.000")
    End If
End Sub

Private Sub txtNoise_GotFocus()
    Call Seltxt(txtNoise)
End Sub


Private Sub txtNoise_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then cmdNoise.SetFocus
End Sub


Private Sub txtNoise_LostFocus()
    Dim iv As Integer
    iv = CInt(Val(txtNoise.Text))
    If iv < 1 Then iv = 1
    If iv > 9999 Then iv = 9999
    txtNoise.Text = Format$(iv)
End Sub


Private Sub txtRight_GotFocus()
    Call Seltxt(txtRight)
End Sub

Private Sub txtRight_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then txtBottom.SetFocus
End Sub

Private Sub txtRight_LostFocus()
    Dim dx As Long, sx As Single
    If optUnit(0).Value Then
        dx = CLng(Val(txtRight.Text))
        If dx < 0& Then dx = 0&
        If dx > CLng(frmLoad.picView.ScaleWidth) Then _
                dx = CLng(frmLoad.picView.ScaleWidth)
        txtRight.Text = Format$(dx)
    Else
        sx = CSng(Val(txtRight.Text))
        If sx < 0! Then sx = 0!
        If sx > 100! Then sx = 100!
        txtRight.Text = Format$(sx, "0.00")
    End If
End Sub

Private Sub txtRyoshika_GotFocus()
    Call Seltxt(txtRyoshika)
End Sub

Private Sub txtRyoshika_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then cmdRyoshika.SetFocus
End Sub


Private Sub txtRyoshika_LostFocus()
    Dim iv As Integer
    iv = CInt(Val(txtRyoshika.Text))
    If iv < 0 Then iv = 0
    If iv > &H100 Then iv = &H100
    txtRyoshika.Text = Format$(iv)
End Sub


Private Sub txtSignal_GotFocus(Index As Integer)
    Call Seltxt(txtSignal(Index))
End Sub

Private Sub txtSignal_LostFocus(Index As Integer)
    Dim dv As Double
    dv = Val(txtSignal(Index).Text)
    txtSignal(Index).Text = Format$(dv, "0.000")
End Sub

Private Sub txtTop_GotFocus()
    Call Seltxt(txtTop)
End Sub

Private Sub txtTop_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then txtRight.SetFocus
End Sub

Private Sub txtTop_LostFocus()
    Dim dy As Long, sy As Single
    
    If optUnit(0).Value Then
        dy = CLng(Val(txtTop.Text))
        If dy < 0& Then dy = 0&
        If dy > CLng(frmLoad.picView.ScaleHeight) Then _
                dy = CLng(frmLoad.picView.ScaleHeight)
        txtTop.Text = Format$(dy)
    Else
        sy = CSng(Val(txtTop.Text))
        If sy < 0! Then sy = 0!
        If sy > 100! Then sy = 100!
        txtTop.Text = Format$(sy, "0.00")
    End If
End Sub

Private Sub txtWidth_Change()
    Dim i As Integer, l As Integer
    Dim s As String, ss As String
    
    l = Len(txtWidth.Text)
    s = ""
    For i = 1 To l
        ss = Mid$(txtWidth.Text, i, 1)
        If ss = Format$(Val(ss)) Then
            s = s & ss
        End If
    Next i
    txtWidth.Text = s
End Sub

Private Sub txtWidth_GotFocus()
    Call Seltxt(txtWidth)
End Sub

Private Sub txtWidth_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then cmdChngSize.SetFocus
End Sub
