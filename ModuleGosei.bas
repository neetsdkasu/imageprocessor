Attribute VB_Name = "ModuleGosei"
Option Explicit

Public Const GOSEI_SEISA As Integer = 0
Public Const GOSEI_FUSA As Integer = 1
Public Const GOSEI_ZENSA As Integer = 2
Public Const GOSEI_JOSAN As Integer = 3
Public Const GOSEI_SAIDAI As Integer = 4
Public Const GOSEI_SAISHO As Integer = 5
Public Const GOSEI_HEIKIN As Integer = 6
Public Const GOSEI_ONAJI As Integer = 7
Public Const GOSEI_OR As Integer = 8
Public Const GOSEI_AND As Integer = 9
Public Const GOSEI_XOR As Integer = 10

Public Sub Gosei(gFlag As Integer)
    
    Dim i As Integer, j As Integer
    Dim dw As Integer, dh As Integer
    Dim c1 As Long, c2 As Long, c As Long
    
    Screen.MousePointer = vbHourglass
    frmMain.prgShinko.Value = 0!
    
    frmResult.picView.Cls
    frmResult.picView.Width = frmLoad.picView.Width
    frmResult.picView.Height = frmLoad.picView.Height
    
    dw = CInt(frmLoad.picView.ScaleWidth)
    dh = CInt(frmLoad.picView.ScaleHeight)
    
    If (Abs(frmLoad.picView.Width - frmMain.picLoad.Width) > 0!) _
            Or (Abs(frmLoad.picView.Height - frmMain.picLoad.Height) > 0!) Then
        Screen.MousePointer = vbArrow
        Exit Sub
    End If
            
    For i = 0 To dh - 1
        For j = 0 To dw - 1
            c1 = frmLoad.picView.Point(CSng(j), CSng(i))
            c2 = frmMain.picLoad.Point(CSng(j), CSng(i))
            c = GoseiShori(c1, c2, gFlag)
            frmResult.picView.PSet (CSng(j), CSng(i)), c
        Next j
        frmMain.prgShinko.Value = CSng(i + 1) / CSng(dh) * 100!
    Next i
    
    Screen.MousePointer = vbArrow

End Sub


Private Function GoseiShori(c1 As Long, c2 As Long, gFlag As Integer) As Long
    Dim cR As Integer, cG As Integer, cB As Integer
    Dim cR1 As Integer, cG1 As Integer, cB1 As Integer
    Dim cR2 As Integer, cG2 As Integer, cB2 As Integer
    Dim dcR As Single, dcG As Single, dcB As Single

    Select Case gFlag
    Case GOSEI_SEISA
        cR = CInt(c1 Mod &H100&) - CInt(c2 Mod &H100&)
        cG = CInt((c1 \ &H100&) Mod &H100&) _
            - CInt((c2 \ &H100&) Mod &H100&)
        cB = CInt((c1 \ &H100&) \ &H100&) _
            - CInt((c2 \ &H100&) \ &H100&)
        If cR < 0 Then cR = 0
        If cG < 0 Then cG = 0
        If cB < 0 Then cB = 0
    Case GOSEI_FUSA
        cR = CInt(c2 Mod &H100&) - CInt(c1 Mod &H100&)
        cG = CInt((c2 \ &H100&) Mod &H100&) _
            - CInt((c1 \ &H100&) Mod &H100&)
        cB = CInt((c2 \ &H100&) \ &H100&) _
            - CInt((c1 \ &H100&) \ &H100&)
        If cR < 0 Then cR = 0
        If cG < 0 Then cG = 0
        If cB < 0 Then cB = 0
    Case GOSEI_HEIKIN
        cR = CInt(c1 Mod &H100&) + CInt(c2 Mod &H100&)
        cG = CInt((c1 \ &H100&) Mod &H100&) _
            + CInt((c2 \ &H100&) Mod &H100&)
        cB = CInt((c1 \ &H100&) \ &H100&) _
            + CInt((c2 \ &H100&) \ &H100&)
        cR = cR \ 2
        cG = cG \ 2
        cB = cB \ 2
    Case GOSEI_JOSAN
        dcR = CSng((c1 Mod &H100&) + 1&) _
                / CSng((c2 Mod &H100&) + 1&)
        dcG = CSng(((c1 \ &H100&) Mod &H100&) + 1&) _
                / CSng(((c2 \ &H100&) Mod &H100&) + 1&)
        dcB = CSng(((c1 \ &H100&) \ &H100&) + 1&) _
                / CSng(((c2 \ &H100&) \ &H100&) + 1&)
        cR = CInt(dcR)
        cG = CInt(dcG)
        cB = CInt(dcB)
    Case GOSEI_ZENSA
        cR = CInt(c1 Mod &H100&) - CInt(c2 Mod &H100&)
        cG = CInt((c1 \ &H100&) Mod &H100&) _
            - CInt((c2 \ &H100&) Mod &H100&)
        cB = CInt((c1 \ &H100&) \ &H100&) _
            - CInt((c2 \ &H100&) \ &H100&)
        cR = Abs(cR)
        cG = Abs(cG)
        cB = Abs(cB)
    Case GOSEI_SAIDAI
        cR1 = CInt(c1 Mod &H100&)
        cR2 = CInt(c2 Mod &H100&)
        cG1 = CInt((c1 \ &H100&) Mod &H100&)
        cG2 = CInt((c2 \ &H100&) Mod &H100&)
        cB1 = CInt((c1 \ &H100&) \ &H100&)
        cB2 = CInt((c2 \ &H100&) \ &H100&)
        If cR1 > cR2 Then cR = cR1 Else cR = cR2
        If cG1 > cG2 Then cG = cG1 Else cG = cG2
        If cB1 > cB2 Then cB = cB1 Else cB = cB2
    Case GOSEI_SAISHO
        cR1 = CInt(c1 Mod &H100&)
        cR2 = CInt(c2 Mod &H100&)
        cG1 = CInt((c1 \ &H100&) Mod &H100&)
        cG2 = CInt((c2 \ &H100&) Mod &H100&)
        cB1 = CInt((c1 \ &H100&) \ &H100&)
        cB2 = CInt((c2 \ &H100&) \ &H100&)
        If cR1 < cR2 Then cR = cR1 Else cR = cR2
        If cG1 < cG2 Then cG = cG1 Else cG = cG2
        If cB1 < cB2 Then cB = cB1 Else cB = cB2
    Case GOSEI_ONAJI
        If c1 = c2 Then
            cR = CInt(c1 Mod &H100&)
            cG = CInt((c1 \ &H100&) Mod &H100&)
            cB = CInt((c1 \ &H100&) \ &H100&)
        Else
            cR = 0
            cG = 0
            cB = 0
        End If
    Case GOSEI_OR
        cR = CInt(c1 Mod &H100&) Or CInt(c2 Mod &H100&)
        cG = CInt((c1 \ &H100&) Mod &H100&) _
            Or CInt((c2 \ &H100&) Mod &H100&)
        cB = CInt((c1 \ &H100&) \ &H100&) _
            Or CInt((c2 \ &H100&) \ &H100&)
    Case GOSEI_AND
        cR = CInt(c1 Mod &H100&) And CInt(c2 Mod &H100&)
        cG = CInt((c1 \ &H100&) Mod &H100&) _
            And CInt((c2 \ &H100&) Mod &H100&)
        cB = CInt((c1 \ &H100&) \ &H100&) _
            And CInt((c2 \ &H100&) \ &H100&)
    Case GOSEI_XOR
        cR = CInt(c1 Mod &H100&) Xor CInt(c2 Mod &H100&)
        cG = CInt((c1 \ &H100&) Mod &H100&) _
            Xor CInt((c2 \ &H100&) Mod &H100&)
        cB = CInt((c1 \ &H100&) \ &H100&) _
            Xor CInt((c2 \ &H100&) \ &H100&)
    End Select
    
    GoseiShori = RGB(cR, cG, cB)

End Function


